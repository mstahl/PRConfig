## PRConfig

How to make a new release:

```
    cd PRConfig
    ./doc/generate > draft
```
Then,

- Paste the contents of `draft` into `doc/release.notes` and add the usual header.
- Open a MR and merge it.
- Create the tag, adding under the "Release notes" the contents of `draft`.

The deployment happens automatically.
