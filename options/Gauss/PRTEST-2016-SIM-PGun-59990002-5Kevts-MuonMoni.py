###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
importOptions("$GAUSSROOT/options/Gauss-2016.py")
importOptions("$DECFILESROOT/options/59990002.py")
importOptions("$APPCONFIGOPTS/Gauss/NoPacking.py")
importOptions("$LBPGUNSROOT/options/PGuns.py")

# Disable spill-over
from Configurables import Gauss
Gauss().SpilloverPaths = []

# Change PGun output verbosity
from Configurables import ParticleGun, FlatPtRapidity
ParticleGun().OutputLevel = INFO
ParticleGun().FlatPtRapidity.OutputLevel = INFO

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 5000

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'

from Configurables import MuonHitChecker
MuonHitCheckerTest = MuonHitChecker("MuonHitCheckerTest")
MuonHitCheckerTest.HistoDir = "MuonHitChecker/MuonHitCheckerTest"
MuonHitCheckerTest.OutputLevel = INFO

from Configurables import MuonMultipleScatteringChecker
MuonMultipleScatteringTest = MuonMultipleScatteringChecker(
    "MuonMultipleScatteringTest")
MuonMultipleScatteringTest.HistoDir = "MuonMultipleScatteringChecker/MuonMultipleScatteringTest"
MuonMultipleScatteringTest.OutputLevel = INFO

GaudiSequencer("DetectorsMonitor").Members += [
    MuonHitCheckerTest, MuonMultipleScatteringTest
]

HistogramPersistencySvc().OutputFile = 'MuonMoniSim_histos.root'
