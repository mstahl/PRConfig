###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
from Gaussino.Generation import GenPhase

# Beam options as in Beam6500GeV-md100-2016-nu1.6.py
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py")

# Set tags from Gauss
importOptions("$GAUSSROOT/options/DBTags-2016.py")

from Configurables import Generation
Generation().EventType = 30000000

GenPhase().ProductionTool = "Pythia8Production"
GenPhase().SampleGenerationTool = "MinimumBias"
GenPhase().DecayTool = "EvtGenDecay"
GenPhase().CutTool = ""

from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool(EvtGenDecay)
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/minbias.dec"

from Configurables import Gauss
nthreads = 8

# GenPhase().Production = 'P8MB'
GenPhase().GenMonitor = True
Gauss().EvtMax = 1000
Gauss().EnableHive = True

Gauss().ThreadPoolSize = nthreads
Gauss().EventSlots = nthreads
Gauss().DataType = "2016"

from Configurables import GenRndInit
GenRndInit().FirstEventNumber = 10042
GenRndInit().TimingSkipAtStart = 10

from Configurables import GiGaMT
giga = GiGaMT()
giga.NumberOfWorkerThreads = nthreads

# No output file
from Configurables import Gauss
Gauss().OutputType = 'NONE'
