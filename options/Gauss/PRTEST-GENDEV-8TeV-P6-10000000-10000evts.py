###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Config for performance and regression testing
from Gaudi.Configuration import *

importOptions("$GAUSSROOT/options/Gauss-DEV.py")
importOptions(
    "$PRCONFIGOPTS/Gauss/PR-Beam4000GeV-NominalBeamLine-VeloClosed-MagDown-fix1.py"
)
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIAROOT/options/Pythia.py")
importOptions("$APPCONFIGOPTS/Gauss/gen.py")

from Configurables import LHCbApp
LHCbApp().EvtMax = 10000
