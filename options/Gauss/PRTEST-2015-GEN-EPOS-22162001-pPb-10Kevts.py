###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions(
    "$APPCONFIGOPTS/Gauss/pPb-Beam6500GeV-2560GeV-md100-pre2016-fix1.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2015.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
importOptions("$DECFILESROOT/options/22162001.py")
importOptions("$LBCRMCROOT/options/EPOS.py")

# DB tags
from Configurables import LHCbApp
LHCbApp().DDDBtag = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20161124-vc-md100"

# Events to process
LHCbApp().EvtMax = 10000

# Run only the generator part, no full simulation
importOptions('$APPCONFIGOPTS/Gauss/gen.py')

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
