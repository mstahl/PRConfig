###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Config for performance testing with callgrind
#
# To run:
# gaudirun.py --profilerName=valgrindcallgrind --profilerExtraOptions="__instr-atstart=no -v __smc-check=all-non-file __dump-instr=yes __trace-jump=yes" PRTEST-SIM2012-10000000-Callgrind.py

from Gaudi.Configuration import *
from Configurables import CallgrindProfile


def addProfile():
    p = CallgrindProfile()
    p.StartFromEventN = 2
    p.StopAtEventN = 3
    p.DumpAtEventN = 3
    p.DumpName = "calgrinddump"
    p.OutputLevel = DEBUG
    GaudiSequencer("Simulation").Members.insert(0, p)


importOptions("$GAUSSROOT/options/Gauss-2011.py")
importOptions("$DECFILESROOT/options/10000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")

from Configurables import LHCbApp
LHCbApp().EvtMax = 4
appendPostConfigAction(addProfile)
