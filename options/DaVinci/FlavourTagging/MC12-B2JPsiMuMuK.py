###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper

IOHelper().inputFiles([
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2012/ALLSTREAMS.DST/00021062/0000/00021062_00000001_1.allstreams.dst'
])
