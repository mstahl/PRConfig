###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles(
    [
        # LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00021211/0000/00021211_00001955_1.bhadron.mdst
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00021211/0000/00021211_00001955_1.bhadron.mdst'
    ],
    clear=True)
