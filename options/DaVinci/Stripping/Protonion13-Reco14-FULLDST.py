###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles(
    [
        # LFN:/lhcb/LHCb/Protonion13/FULL.DST/00024845/0000/00024845_00007206_1.full.dst
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Protonion13/FULL.DST/00024845/0000/00024845_00007206_1.full.dst'
    ],
    clear=True)
