#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# 2015 Brunel options for Early Measurements:
# ==================================
from Configurables import Brunel
Brunel().DataType = "2015"
Brunel().EvtMax = 2000

from Configurables import LHCbApp
LHCbApp().CondDBtag = "cond-20150602"
LHCbApp().DDDBtag = "dddb-20150526"

from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$APPCONFIGOPTS/Brunel/saveFittedVeloTracks.py")
importOptions("$PRCONFIGOPTS/Brunel/jemalloc/full.py")


def postConfig():
    from Configurables import JemallocProfile
    jp = JemallocProfile()
    jp.StartFromEventN = 499
    jp.StopAtEventN = 1999
    jp.DumpPeriod = 100

    GaudiSequencer("PhysicsSeq").Members += [jp]


from Gaudi.Configuration import *
appendPostConfigAction(postConfig)
