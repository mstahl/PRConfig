###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel
Brunel().EvtMax = 1000
Brunel().InputType = "DIGI"

from Gaudi.Configuration import importOptions
importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")

from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 6

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_DC19_01_Bs2PhiPhiMD'].run(withDB=False)
