###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import subprocess
import os
import sys
from LbNightlyTools import Configuration


def get_project_version(config, project):
    ''' Look up the project version '''
    version = None
    for proj in config[u'projects']:
        if proj['name'].lower() == project.lower():
            version = proj['version']
            break
    return version


def main():
    config = Configuration.load(os.getcwd() + "/../build/slot-config.json")
    version = get_project_version(config, "Brunel")
    return_code = subprocess.call([
        "lb-run", "--user-area=" + os.getcwd() + "/../build",
        "Brunel/" + version, "python",
        "$PRCONFIGOPTS/Brunel/HLT1/runPixelTrackingOnlyThroughput.py",
        "-p10:1,10:2,10:3,10:4,20:1,20:2,22:2,24:2", "-n20000", "-m500",
        "-path", "./output", "/scratch/lhcbpr2/MINBIASTESTSAMPLE_big_GEC.mdf"
    ])
    return return_code


if __name__ == '__main__':
    sys.exit(main())
