###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel
from Configurables import (TrackSys, GaudiSequencer)
from Configurables import FTRawBankDecoder
from Configurables import NTupleSvc
from Gaudi.Configuration import appendPostConfigAction
from Configurables import HistogramPersistencySvc

DDDBtag = "dddb-20171010"
CondDBtag = "sim-20170301-vc-md100"

Evts_to_Run = 1000  # set to -1 to process all

mbrunel = Brunel(
    DataType="Upgrade",
    EvtMax=Evts_to_Run,
    PrintFreq=1,
    WithMC=True,
    Simulation=True,
    OutputType="None",
    DDDBtag=DDDBtag,
    CondDBtag=CondDBtag,
    MainSequence=['ProcessPhase/Reco'],
    RecoSequence=["Decoding", "TrFast"],
    Detectors=["VP", "UT", "FT"],
    InputType="DIGI")

TrackSys().TrackingSequence = ["Decoding", "TrFast"]
TrackSys().TrackTypes = ["Velo", "Upstream", "Forward"]
mbrunel.MainSequence += ['ProcessPhase/MCLinks', 'ProcessPhase/Check']

FTRawBankDecoder("createFTClusters").DecodingVersion = 2


def modifySequences():
    try:
        # empty the calo sequence
        GaudiSequencer("MCLinksCaloSeq").Members = []
        GaudiSequencer("MCLinksCaloSeq").Members = []
        from Configurables import TrackResChecker
        GaudiSequencer("CheckPatSeq").Members.remove(
            TrackResChecker("TrackResCheckerFast"))
        from Configurables import VectorOfTracksFitter
        GaudiSequencer("RecoTrFastSeq").Members.remove(
            VectorOfTracksFitter("ForwardFitterAlgFast"))
        from Configurables import PrTrackAssociator
        GaudiSequencer("ForwardFastFittedExtraChecker").Members.remove(
            PrTrackAssociator("ForwardFastFittedAssociator"))
        from Configurables import MuonRec
        GaudiSequencer("RecoDecodingSeq").Members.append(MuonRec())
    except ValueError:
        None


appendPostConfigAction(modifySequences)

HistogramPersistencySvc().OutputFile = "PrCheckerPlots.root"


def addPrCheckerCutsAndPlots():
    velocuts = {
        "VeloTracks_electrons":
        "isVelo & isElectron",
        "VeloTracks_eta25_electrons":
        "isVelo & isElectron & eta25",
        "LongFromB_eta25_electrons":
        " (isLong) & fromB & isElectron & eta25",
        "LongFromD_eta25_electrons":
        " (isLong) & fromD & isElectron & eta25",
        "LongStrange_eta25_electrons":
        " (isLong) & strange & isElectron & eta25",
        "VeloTracks_notElectrons":
        "isVelo & isNotElectron",
        "VeloTracks_eta25_notElectrons":
        "isVelo & isNotElectron & eta25",
        "LongFromB_eta25_notElectrons":
        " (isLong) & fromB & isNotElectron & eta25",
        "LongFromD_eta25_notElectrons":
        " (isLong) & fromD & isNotElectron & eta25",
        "LongStrange_eta25_notElectrons":
        " (isLong) & strange & isNotElectron & eta25"
    }
    upcuts = {
        "VeloUTTracks_eta25_electrons":
        "isVelo & isUp & isElectron & eta25",
        "LongFromB_eta25_electrons":
        "(isLong) & fromB & isElectron & eta25",
        "LongFromD_eta25_electrons":
        "(isLong) & fromD & isElectron & eta25",
        "LongStrange_eta25_electrons":
        "(isLong) & strange & isElectron & eta25",
        "VeloUTTracks_eta25_notElectrons":
        "isVelo & isUp & isNotElectron & eta25",
        "LongFromB_eta25_notElectrons":
        "(isLong) & fromB & isNotElectron & eta25",
        "LongFromD_eta25_notElectrons":
        "(isLong) & fromD & isNotElectron & eta25",
        "LongStrange_eta25_notElectrons":
        "(isLong) & strange & isNotElectron & eta25"
    }
    forwardcuts = {
        "Long_eta25_electrons":
        "isLong & isElectron & eta25",
        "Long_eta25_triggerNumbers_electrons":
        "isLong & isElectron & eta25 & trigger",
        "LongFromB_eta25_electrons":
        "(isLong) & fromB & isElectron & eta25",
        "LongFromD_eta25_electrons":
        "(isLong) & fromD & isElectron & eta25",
        "LongStrange_eta25_electrons":
        "(isLong) & strange & isElectron & eta25",
        "Long_eta25_notElectrons":
        "isLong & isNotElectron & eta25",
        "Long_eta25_triggerNumbers_notElectrons":
        "isLong & isNotElectron & eta25 & trigger",
        "LongFromB_eta25_notElectrons":
        "(isLong) & fromB & isNotElectron & eta25",
        "LongFromD_eta25_notElectrons":
        "(isLong) & fromD & isNotElectron & eta25",
        "LongStrange_eta25_notElectrons":
        "(isLong) & strange & isNotElectron & eta25"
    }
    from Configurables import PrTrackChecker, PrUTHitChecker
    veloChecker = PrTrackChecker(
        "VeloMCChecker",
        Title="Velo",
        Tracks="Rec/Track/Keyed/Velo",
        Links="Link/Rec/Track/Keyed/Velo",
        HitTypesToCheck=3,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=velocuts)
    forwardChecker = PrTrackChecker(
        "ForwardMCChecker",
        Title="Forward",
        Tracks="Rec/Track/Keyed/ForwardFast",
        Links="Link/Rec/Track/Keyed/ForwardFast",
        HitTypesToCheck=8,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=forwardcuts)
    upChecker = PrTrackChecker(
        "UpMCChecker",
        Title="Upstream",
        Tracks="Rec/Track/Keyed/Upstream",
        Links="Link/Rec/Track/Keyed/Upstream",
        HitTypesToCheck=4,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=upcuts)
    utforwardChecker = PrUTHitChecker(
        "UTForwardMCChecker",
        Title="UTForward",
        Tracks="Rec/Track/Keyed/ForwardFast",
        Links="Link/Rec/Track/Keyed/ForwardFast",
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=forwardcuts)
    # as configurations are not yet uniformized and properly handled, there is an ugly trick here
    # all members are newly defined here as they have different names from the original ones
    # defined in PrUpgradechecking, but the last one that we reuse as it
    GaudiSequencer("CheckPatSeq").Members = [
        veloChecker, forwardChecker, upChecker, utforwardChecker,
        PrTrackChecker("VeloFullMCCheck")
    ]


appendPostConfigAction(addPrCheckerCutsAndPlots)

from PRConfig.TestFileDB import test_file_db
test_file_db['upgrade_2018_BsPhiPhi_FTv2'].run(configurable=mbrunel)
