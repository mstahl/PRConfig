###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from Configurables import Brunel
from Configurables import (TrackSys, GaudiSequencer)
from Configurables import FTRawBankDecoder
from Configurables import NTupleSvc
from Gaudi.Configuration import appendPostConfigAction
from Configurables import HistogramPersistencySvc

Evts_to_Run = 5000  # set to -1 to process all

mbrunel = Brunel(
    DataType="Upgrade",
    EvtMax=Evts_to_Run,
    PrintFreq=1,
    WithMC=True,
    Simulation=True,
    OutputType="None",
    MainSequence=['ProcessPhase/Reco'],
    RecoSequence=["Decoding", "TrFast", "TrBest"],
    Detectors=["VP", "UT", "FT"],
    InputType="DST")

from Configurables import RecombineRawEvent
RecombineRawEvent().Version = 4.2

TrackSys().TrackingSequence = ["Decoding", "TrFast", "TrBest"]
mbrunel.MainSequence += ['ProcessPhase/MCLinks', 'ProcessPhase/Check']

FTRawBankDecoder("createFTClusters").DecodingVersion = 2


def modifySequences():
    try:
        # empty the calo sequence
        GaudiSequencer("MCLinksCaloSeq").Members = []
        GaudiSequencer("MCLinksCaloSeq").Members = []
        from Configurables import TrackResChecker
        GaudiSequencer("CheckPatSeq").Members.remove(
            TrackResChecker("TrackResCheckerFast"))
        from Configurables import MuonRec
        GaudiSequencer("RecoDecodingSeq").Members.append(MuonRec())
    except ValueError:
        None


appendPostConfigAction(modifySequences)

HistogramPersistencySvc().OutputFile = "PrCheckerPlots_HLT2.root"


def enrich_selections(mysel_raw):
    sel_raw = mysel_raw
    if not type(sel_raw) == dict:
        print(
            "ERROR! The function \'enrich_selections\' requires a dict as input."
        )
        exit()
    #Add the 'notElectron' selections
    for sel_label in sel_raw.keys():
        sel_content = sel_raw[sel_label]
        sel_label_notElectron = sel_label.replace("electrons", "notElectrons")
        sel_content_notElectron = sel_content.replace("isElectron",
                                                      "isNotElectron")
        sel_raw[sel_label_notElectron] = sel_content_notElectron
    #Add the "FromB, FromD, Strange" labels before "_electrons" or "_notElectrons".
    for sel_label in sel_raw.keys():
        sel_content = sel_raw[sel_label]
        if "_electrons" in sel_label:
            sel_label_FromB = sel_label.replace("_electrons",
                                                "_FromB_electrons")
            sel_label_FromD = sel_label.replace("_electrons",
                                                "_FromD_electrons")
            sel_label_Strange = sel_label.replace("_electrons",
                                                  "_Strange_electrons")
        elif "_notElectrons" in sel_label:
            sel_label_FromB = sel_label.replace("_notElectrons",
                                                "_FromB_notElectrons")
            sel_label_FromD = sel_label.replace("_notElectrons",
                                                "_FromD_notElectrons")
            sel_label_Strange = sel_label.replace("_notElectrons",
                                                  "_Strange_notElectrons")
        else:
            print(
                "ERROR! The name of histogram should contain electrons or notElectrons. Check the option File!"
            )
            exit()
        sel_content_FromB = sel_content + " & fromB"
        sel_content_FromD = sel_content + " & fromD"
        sel_content_Strange = sel_content + " & strange"
        sel_raw[sel_label_FromB] = sel_content_FromB
        sel_raw[sel_label_FromD] = sel_content_FromD
        sel_raw[sel_label_Strange] = sel_content_Strange
    #Add the "isDecay, PairProd, fromHI" labels before "_electrons" or "_notElectrons"
    for sel_label in sel_raw.keys():
        sel_content = sel_raw[sel_label]
        if "_electrons" in sel_label:
            sel_label_isDecay = sel_label.replace("_electrons",
                                                  "_isDecay_electrons")
            sel_label_PairProd = sel_label.replace("_electrons",
                                                   "_PairProd_electrons")
            sel_label_fromHI = sel_label.replace("_electrons",
                                                 "_fromHI_electrons")
        elif "_notElectrons" in sel_label:
            sel_label_isDecay = sel_label.replace("_notElectrons",
                                                  "_isDecay_notElectrons")
            sel_label_PairProd = sel_label.replace("_notElectrons",
                                                   "_PairProd_notElectrons")
            sel_label_fromHI = sel_label.replace("_notElectrons",
                                                 "_fromHI_notElectrons")
        else:
            print(
                "ERROR! The name of histogram should contain electrons or notElectrons. Check the option File!"
            )
            exit()
        sel_content_isDecay = sel_content + " & isDecay"
        sel_content_PairProd = sel_content + " & PairProd"
        sel_content_fromHI = sel_content + " & fromHI"
        sel_raw[sel_label_isDecay] = sel_content_isDecay
        sel_raw[sel_label_PairProd] = sel_content_PairProd
        sel_raw[sel_label_fromHI] = sel_content_fromHI
    return sel_raw


def addPrCheckerCutsAndPlots():
    #The selection of the tracks
    VeloCuts_raw = {
        #With eta25 cut, No requirement on UT and Scifi hits.
        "Velo_eta25_electrons":
        "isVelo & isElectron & eta25",
        #With eta25 cut, No UT, No Scifi hits. The input is a "real" Velo track sample.
        "Velo_hasnoUTSeed_eta25_electrons":
        "isVelo & isNotUT & isNotSeed & isElectron & eta25",
        #With eta25 cut, isLong. To check the performance of the Velo part reconstruction of the Long track.
        "Velo_isLong_eta25_electrons":
        "isLong & isElectron & eta25",
        #With eta25 cut, has hits in Velo, UT and Scifi. To check the performance of the Velo part reconstruction of the Long track.
        "Velo_hasVeloUTSeed_eta25_electrons":
        "isVelo & isUT & isSeed & isElectron & eta25",
        #With eta25 cut, has hits in Velo and Scifi. To check the performance of the Velo part reconstruction of the Long track without UT hits.
        "Velo_hasVeloSeed_eta25_electrons":
        "isVelo & isNotUT & isSeed & isElectron & eta25",
        #Without eta25 cut. No requirement on UT and Scifi hits.
        "Velo_electrons":
        "isVelo & isElectron",
        #Without eta25 cut. No UT, No Scifi hits. The input is a "real" Velo track sample.
        "Velo_hasnoUTSeed_electrons":
        "isVelo & isNotUT & isNotSeed & isElectron",
        #Without eta25 cut. To check the performance of the Velo part reconstruction of the Long track.
        "Velo_isLong_electrons":
        "isLong & isElectron",
        #Without eta25 cut, has hits in Velo, UT and Scifi. To check the performance of the Velo part reconstruction of the Long track.
        "Velo_hasVeloUTSeed_electrons":
        "isVelo & isUT & isSeed & isElectron",
        #Without eta25 cut, has hits in Velo and Scifi. To check the performance of the Velo part reconstruction of the Long track without UT hits.
        "Velo_hasVeloSeed_electrons":
        "isVelo & isNotUT & isSeed & isElectron",
    }
    VeloCuts = enrich_selections(VeloCuts_raw)

    UpCuts_raw = {
        #isUp. Including the tracks with and without Scifi hits.
        "Upstream_eta25_electrons":
        "isUp & isElectron & eta25",
        #With Scifi hits. Reconstructed as a Upstream track
        "Upstream_hasSeed_eta25_electrons":
        "isUp & isSeed & isElectron & eta25",
        #Without Scifi hits. A "real" Upstream track
        "Upstream_hasnoSeed_eta25_electrons":
        "isUp & isNotSeed & isElectron & eta25",
    }
    UpCuts = enrich_selections(UpCuts_raw)

    ForwardCuts_raw = {
        #Both with and without UT hits
        "Long_eta25_electrons": "isLong & isElectron & eta25",
        #with UT hits
        "Long_hasUT_eta25_electrons": "isLong & isUT & isElectron & eta25",
        #without UT hits
        "Long_hasnoUT_eta25_electrons":
        "isLong & isNotUT & isElectron & eta25",
    }
    ForwardCuts = enrich_selections(ForwardCuts_raw)

    TtrackCuts_raw = {
        #No requirement on Velo and UT hits
        "Seed_eta25_electrons":
        "isSeed & isElectron & eta25",
        #No Velo hits, No UT hits
        "Seed_hasnoVeloUT_eta25_electrons":
        "isSeed & isNotUT & isNotVelo & isElectron & eta25",
        #With Velo hits (Long track)
        "Seed_isLong_eta25_electrons":
        "isLong & isElectron & eta25",
        #With no Velo hits, with UT hits (Downstream track)
        "Seed_isDown_eta25_electrons":
        "isDown & isElectron & eta25",
    }
    TtrackCuts = enrich_selections(TtrackCuts_raw)

    DownCuts_raw = {
        #No requirement on Velo hits
        "Down_eta25_electrons":
        "isDown & isElectron & eta25",
        #Without Velo hits
        "Down_hasnoVelo_eta25_electrons":
        "isDown & isNotVelo & isElectron & eta25",
        #With Velo hits
        "Down_hasVelo_eta25_electrons":
        "isDown & isVelo & isElectron & eta25",
    }
    DownCuts = enrich_selections(DownCuts_raw)

    from Configurables import PrTrackChecker, PrUTHitChecker
    veloChecker = PrTrackChecker(
        "VeloMCChecker",
        Title="Velo",
        Tracks="Rec/Track/Keyed/Velo",
        Links="Link/Rec/Track/Keyed/Velo",
        HitTypesToCheck=3,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=VeloCuts)
    forwardChecker = PrTrackChecker(
        "ForwardMCChecker",
        Title="Forward",
        Tracks="Rec/Track/Keyed/ForwardFast",
        Links="Link/Rec/Track/Keyed/ForwardFast",
        HitTypesToCheck=8,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=ForwardCuts)
    upChecker = PrTrackChecker(
        "UpMCChecker",
        Title="Upstream",
        Tracks="Rec/Track/Keyed/Upstream",
        Links="Link/Rec/Track/Keyed/Upstream",
        HitTypesToCheck=4,
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=UpCuts)
    utforwardChecker = PrUTHitChecker(
        "UTForwardMCChecker",
        Title="UTForward",
        Tracks="Rec/Track/Keyed/ForwardFast",
        Links="Link/Rec/Track/Keyed/ForwardFast",
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=ForwardCuts)
    forwardChecker2 = PrTrackChecker(
        "ForwardBestMCChecker",
        Title="Forward",
        Tracks="Rec/Track/Keyed/ForwardBest",
        Links="Link/Rec/Track/Keyed/ForwardBest",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=8,
        MyCuts=ForwardCuts)
    matchChecker = PrTrackChecker(
        "MatchMCChecker",
        Title="Match",
        Tracks="Rec/Track/Keyed/Match",
        Links="Link/Rec/Track/Keyed/Match",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=8,
        MyCuts=ForwardCuts)
    tChecker = PrTrackChecker(
        "TMCChecker",
        Title="UTrack",
        Tracks="Rec/Track/Keyed/Seed",
        Links="Link/Rec/Track/Keyed/Seed",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=8,
        MyCuts=TtrackCuts)
    downChecker = PrTrackChecker(
        "DownMCChecker",
        Title="Downstream",
        Tracks="Rec/Track/Keyed/Downstream",
        Links="Link/Rec/Track/Keyed/Downstream",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=4,
        MyCuts=DownCuts)
    bestChecker = PrTrackChecker(
        "BestMCChecker",
        Title="Best",
        Tracks="Rec/Track/Best",
        Links="Link/Rec/Track/Best",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=15,
        GhostProbCut=0.9,
        MyCuts=ForwardCuts)
    bestLongChecker = PrTrackChecker(
        "BestLongMCChecker",
        Title="BestLong",
        Tracks="Rec/Track/Best",
        Links="Link/Rec/Track/Best",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=15,
        GhostProbCut=0.9,
        TrackType="Long",
        MyCuts=ForwardCuts)
    bestDownChecker = PrTrackChecker(
        "BestDownMCChecker",
        Title="BestDown",
        Tracks="Rec/Track/Best",
        Links="Link/Rec/Track/Best",
        WriteHistos=2,
        VetoElectrons=False,
        HitTypesToCheck=12,
        GhostProbCut=0.9,
        TrackType="Downstream",
        MyCuts=DownCuts)
    utforwardChecker2 = PrUTHitChecker(
        "UTForwardBestMCChecker",
        Title="UTForward",
        Tracks="Rec/Track/Keyed/ForwardBest",
        Links="Link/Rec/Track/Keyed/ForwardBest",
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=ForwardCuts)
    utmatchChecker = PrUTHitChecker(
        "UTMatchMCChecker",
        Title="UTMatch",
        Tracks="Rec/Track/Keyed/Match",
        Links="Link/Rec/Track/Keyed/Match",
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=ForwardCuts)
    utdownChecker = PrUTHitChecker(
        "UTDownMCChecker",
        Title="UTDownstream",
        Tracks="Rec/Track/Keyed/Downstream",
        Links="Link/Rec/Track/Keyed/Downstream",
        WriteHistos=2,
        VetoElectrons=False,
        MyCuts=DownCuts)

    GaudiSequencer("CheckPatSeq").Members = [
        veloChecker, forwardChecker, upChecker, utforwardChecker,
        forwardChecker2, matchChecker, tChecker, downChecker, bestChecker,
        bestLongChecker, bestDownChecker, utforwardChecker2, utmatchChecker,
        utdownChecker
    ]


appendPostConfigAction(addPrCheckerCutsAndPlots)

from PRConfig.TestFileDB import test_file_db
test_file_db['upgrade_2018_BdKstee_LDST'].run(configurable=mbrunel)
