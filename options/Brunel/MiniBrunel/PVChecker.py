###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import Brunel, GaudiSequencer, FTRawBankDecoder
from GaudiConf import IOHelper

brunel = Brunel()
brunel.DataType = "Upgrade"
brunel.EvtMax = 1000
brunel.WithMC = True
brunel.OutputType = "NONE"
brunel.Simulation = True
brunel.InputType = 'DIGI'
brunel.DDDBtag = "dddb-20170301"
brunel.CondDBtag = "sim-20170301-vc-md100"
brunel.DisableTiming = True
brunel.MainSequence = [
    'ProcessPhase/Reco', 'ProcessPhase/MCLinks', 'ProcessPhase/Check'
]
FTRawBankDecoder("createFTClusters").DecodingVersion = 2


def ConfGaudiSeq():
    GaudiSequencer("RecoRICHFUTURESeq").Members = []
    GaudiSequencer("RecoRICHSeq").Members = []
    GaudiSequencer("RecoCALOSeq").Members = []
    GaudiSequencer("RecoMUONSeq").Members = []
    GaudiSequencer("RecoPROTOSeq").Members = []
    GaudiSequencer("RecoSUMMARYSeq").Members = []
    GaudiSequencer("CheckRICHFUTURESeq").Members = []
    GaudiSequencer("CheckRICHSeq").Members = []
    GaudiSequencer("CheckMUONSeq").Members = []


appendPostConfigAction(ConfGaudiSeq)

files = [
    'root://eoslhcb.cern.ch//eos/lhcb/user/c/chasse/data/upgrade_mc_FT61_1000evts_minbias.xdigi'
]
IOHelper("ROOT").inputFiles(files, clear=True)
