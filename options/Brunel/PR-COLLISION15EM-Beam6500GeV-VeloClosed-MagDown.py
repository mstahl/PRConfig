###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Example 2015 Early Measurements (50ns) collisions options for Brunel

# Syntax is:
#   gaudirun.py COLLISION15EM-Beam6500GeV-VeloClosed-MagDown.py
#
from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import Brunel

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = ["xmlcatalog_file:MyCatalog.xml"]

#-- Use latest 2015 database tags for real data
Brunel().DataType = "2015"

EventSelector().Input = [
    "DATAFILE='mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2015/RAW/TURBO/LHCb/COLLISION15EM/157819/157819_0000000208.raw'  SVC='LHCb::MDFSelector'"
]
