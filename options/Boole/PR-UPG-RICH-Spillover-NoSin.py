###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiConf import IOHelper
InputArea = "root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/Sim10Up08Digi15Up04/13104012/"
myInputFiles = [
    InputArea + "00128262_000000{0:02d}_1.xdigi".format(i)
    for i in range(1, 15)
]
IOHelper('ROOT').inputFiles(myInputFiles, clear=True)

from Configurables import LHCbApp, DDDBConf
LHCbApp().DDDBtag = "upgrade/dddb-20201211"
LHCbApp().CondDBtag = "upgrade/sim-20201218-vc-md100"

from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")

from Configurables import Boole, CondDB
CondDB().Upgrade = True
Boole().DataType = "Upgrade"
Boole().InputDataType = "XDIGI"
Boole().DigiType = "Extended"
myOutputPref = "Boole"
Boole().Outputs = []
HistogramPersistencySvc().OutputFile = myOutputPref + "-histos.root"

Boole().EvtMax = 1000

# SIN
from Configurables import RichDigiSysConf
RichDigiSysConf().Sin = False
