###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import Boole
from GaudiConf import IOHelper

importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

testName = "PR-UPG-Integration-noSpillover"

Boole().EvtMax = 1000
Boole().InputDataType = "XDIGI"
Boole().DigiType = "Extended"
Boole().Histograms = "Expert"
Boole().DatasetName = testName
Boole().DDDBtag = "upgrade/dddb-20201211"
Boole().CondDBtag = "upgrade/sim-20201218-vc-md100"
Boole().Outputs = []

InputArea = "root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/Sim10Up08Digi15Up04/13144011/"
myInputFiles = [
    InputArea + "00128264_000000{0:02d}_1.xdigi".format(i)
    for i in range(1, 16)
]
IOHelper('ROOT').inputFiles(myInputFiles, clear=True)

HistogramPersistencySvc().OutputFile = "Boole-histos.root"
