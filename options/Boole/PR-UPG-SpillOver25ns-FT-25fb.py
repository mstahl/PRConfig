###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""

Runs digitisation on FT only

"""
import sys
from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp, DDDBConf, CondDB

name = "PR-UPG-SpillOver25ns-FT"
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Upgrade-WithTruth.py")
importOptions("$APPCONFIGOPTS/Boole/xdigi.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

Boole().EvtMax = 1000
MessageSvc().OutputLevel = INFO

LHCbApp().Simulation = True
dddbTag = "dddb-20190223"
condDBTag = "sim-20180530-vc-mu100"

Boole().DDDBtag = dddbTag
Boole().CondDBtag = condDBTag

CondDB().Upgrade = True
Boole().DataType = "Upgrade"

Boole().DetectorDigi = ["FT"]
Boole().DetectorLink = ["FT"]
Boole().DetectorMoni = ["FT"]
Boole().DatasetName = name

#Irradiation level
from Configurables import MCFTDepositCreator, SiPMResponse, FTSiPMTool, MCFTG4AttenuationTool
depositCreator = MCFTDepositCreator("MCFTDepositCreator")
depositCreator.addTool(MCFTG4AttenuationTool("MCFTG4AttenuationTool"))
depositCreator.addTool(FTSiPMTool("FTSiPMTool"))
depositCreator.MCFTG4AttenuationTool.Irradiation = 25.
depositCreator.FTSiPMTool.IrradiationLevel = 3.

#Input
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_0.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_1.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_2.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_3.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_4.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190506_147_5.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_6.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_7.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190507_147_8.sim',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Simulation/PRTestData/Boole/FT/Gauss-13104012-100ev-20190505_147_9.sim',
],
                            clear=True)

from Configurables import MCFTDepositCreator, MCFTDigitCreator, FTClusterCreator
from Configurables import MCFTDepositMonitor, MCFTDigitMonitor, FTClusterMonitor
from Configurables import FTMCHitSpillMerger, FTRawBankEncoder, FTRawBankDecoder, FTClusterTuple

NTupleSvc().Output = [
    "FILE1 DATAFILE='" + name + "-tuple.root' TYP='ROOT' OPT='NEW'"
]


def doIt():
    from Configurables import FTLiteClusterMonitor
    GaudiSequencer("DigiFTSeq").Members = [
        FTMCHitSpillMerger(),
        MCFTDepositCreator(),
        MCFTDigitCreator(),
        FTClusterCreator(),
        FTRawBankEncoder(),
        FTRawBankDecoder()
    ]

    GaudiSequencer("MoniFTSeq").Members = [
        MCFTDepositMonitor(),
        MCFTDigitMonitor(),
        FTClusterMonitor(),
        FTLiteClusterMonitor(),
        # FTClusterTuple(),
    ]


appendPostConfigAction(doIt)
