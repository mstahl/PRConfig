#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
fi

python -m MooreTests.run_throughput_jobs -n 2e4 --test-file-db-key=UpgradeHLT1FilteredWithGEC '$MOOREROOT/options/ft_decoding_v2.py' '$RECOCONFROOT/options/hlt2_light_reco_pr_kf.py' "${cache_dirs[@]}"

python -m MooreTests.run_throughput_jobs -n=-1 -j 1 --profile --test-file-db-key=UpgradeHLT1FilteredWithGEC '$MOOREROOT/options/ft_decoding_v2.py' '$RECOCONFROOT/options/hlt2_light_reco_pr_kf.py' "${cache_dirs[@]}"

# force 0 return code so the handler runs even for failed jobs
exit 0
