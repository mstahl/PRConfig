#! /usr/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

rm perf.data
rm ThroughputTest*.log
rm Profile*.log
rm FlameBars.png
rm flamy.svg
rm ht_flamy*
rm heaptrack.python*.gz
rm FlameBars.pdf
rm result.csv
