#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

python -m MooreTests.run_throughput_jobs -n 2e6 '$MOOREROOT/options/force_functor_cache.py' '$HLT1CONFROOT/options/hlt1_pp_default.py'

python -m MooreTests.run_throughput_jobs -n=-1 -j 1 --profile '$MOOREROOT/options/force_functor_cache.py' '$HLT1CONFROOT/options/hlt1_pp_default.py'

# force 0 return code so the handler runs even for failed jobs
exit 0
