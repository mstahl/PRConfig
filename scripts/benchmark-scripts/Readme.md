# LHCb benchmark scripts
A set of scripts to benchmark Moore.
It is used to run [Performance and Regression](https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbPR) (PR) tests on top of nightly builds.

## Work locally to make flamegraphs or determine throughputs
We recommend to work in the [lb-stack-setup](https://gitlab.cern.ch/rmatev/lb-stack-setup) environment.
Once the stack is set up and built, making flamegraphs or throughputs becomes a one-liner.

Scripts in this folder are used for the current PR nightly tests for Moore. These are shell scripts that just call the main `run_throughput_jobs.py` script with appropriate options. Most will run a throughput test first, and then a profiling test.
To test your own code, you may want to (copy and) edit such a script. The script can then be executed in the runtime environment which has been previously set up like so (assuming you are in you lb-stack-setup folder):
```
Moore/run DBASE/PRConfig/scripts/benchmark-scripts/Moore_hlt1_pp_default.sh
```

### Input files
To get reliable throughput numbers, input files are copied to *local* (not mounted) storage. This copy is temporary and will be done every time a test is run (unless the exact same file was already downloaded). To speed up your local tests, we recommend to download the input manually and pass it to the test with the `-f` option.<br>
We ran into cornercases where input files couldn't be downloaded. If this happens, try to download it yourself to a local folder and pass this file to the `-f` option.<br>
In some cases the IO optimizations need to be switched off. Try setting `use_iosvc=False`, `event_store="HiveWhiteBoard"` in your Moore options if you see failures from `LHCb__MDF__IOSvcMM`

### Cleaning up
Running local tests produces some output in the current directory. You can clean up all these files with `./DBASE/PRConfig/scripts/benchmark-scripts/cleanup.sh` or `git clean`.<br> A dry run can be done with `git clean -nxd`. Running `git clean -fxd` will remove the files.


## Add new throughput test to nightlies
Let's say you have come up with a new HLT2 configuration and would like its throughput performance to be monitored as part of the nightly throughput tests.
This can easily be done by following these steps:

1. Create an options file in Moore that configures your specific sequence. The same thing `hlt2_reco_baseline.py` does for the default HLT2 sequence.
   In the merge request to add said options file, please make sure to mention its purpose and that the relevant parties agree that a nightly performance test is necessary.

2. Next, add a script, similar to `Moore_hlt2_reco_baseline.sh`, to this repository.

3. Have a look at [this file](https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf/-/blob/master/test_schedule2.xml) which defines when to run which nightly test.
   You will have to create a MR to this project as well and modify this file.
   Search for `Moore_hlt2_reco_baseline` and recreate the corresponding schedule entry for your test, replacing `Moore_hlt2_reco_baseline` with the name of your new configuration.

4. This last step is unfortunately not possible to be performed by everyone, so make sure to notify @maszyman in the merge request created in step 3 and he/she will happily take care of this final step.
