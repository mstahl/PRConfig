###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TCKUtils.utils import *

settings = {
    'TCKData': 'TCKData/config.cdb',
    'TCK': 0x2AA11609,
    'outputFile': 'hlt2list.py',
}

# Open the cdb
cas = ConfigAccessSvc(File=settings['TCKData'], Mode='ReadOnly')
# Get the HLT2 lines referenced by this TCK
hlt2_lines = getHlt2Lines(settings['TCK'], cas=cas)
# Write the output list to outputFile
with open(settings['outputFile'], 'w') as _file:
    # Store the string rep in a file so we can use it in another script
    _file.write(str(hlt2_lines))
