###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
settings = "Physics_pp_2018"
MooreOptions = dict(
    Simulation=False,
    UseTCK=False,
    ThresholdSettings=settings,
    DataType="2018",
    DDDBtag="dddb-20150724",
    CondDBtag="cond-20170510",
    RemoveInputHltRawBanks=False,
    EnableTimer=True,
    ForceSingleL0Configuration=True,
    TimingTest=True,
    HltLevel='Hlt2',
    Split='Hlt2',
    EnableOutputStreaming=True)


def configure():
    pass


from Configurables import HltConf
HltConf().L0TCK = '0x1706'
from ConfigureJobs import configureOnlineConditions, setHlt1SelectionIDs
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(lambda: setHlt1SelectionIDs(settings))
