###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MooreOptions = dict(
    Simulation=False,
    DataType="2016",
    DDDBtag="dddb-20150724",
    CondDBtag="cond-20160522",
    RemoveInputHltRawBanks=False,
    UseTCK=True,
    InitialTCK="0x11361609",
    HltLevel='Hlt1')


def configure():
    from Moore import Funcs

    lines = ["Hlt1MBNoBias", "Hlt1MBNoBiasRateLimited", "Hlt1Lumi", "Hlt2Lumi"]

    trans = {
        ".*{}PreScaler".format(line): {
            "AcceptFraction": {
                "^.*$": "0"
            }
        }
        for line in lines
    }
    Funcs._mergeTransform(trans)
