###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MooreOptions = dict(
    Simulation=True,
    DataType="2012",
    DDDBtag="dddb-20140729",
    CondDBtag="sim-20140730-vc-md100",
    UseTCK=False,
    RemoveInputHltRawBanks=False,
    EnableTimer=True,
    ThresholdSettings="Physics_December2014_TopoOnly",
    HltLevel='Hlt1Hlt2')


def configure():
    from GaudiKernel.SystemOfUnits import MeV
    from HltTracking.HltRecoConf import HltRecoConf
    recoConf = HltRecoConf()
    recoConf.OfflineRich = True
    recoConf.FitVelo = True
    recoConf.Forward_HPT_MinPt = 500. * MeV
    recoConf.Forward_HPT_MinP = 3000. * MeV
    ## recoConf.Forward_LPT_MinPt = 100. * MeV
    ## recoConf.Forward_LPT_MinP  = 1500. * MeV
    recoConf.Forward_LPT_MinPt = 200. * MeV
    recoConf.Forward_LPT_MinP = 3000. * MeV
