###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
os.system("python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"\
              " --inputdata=\"2016NB_25ns_L0Filt0x160B\""\
              " --settings=\"Physics_pp_LooseDraft2016\"")
