#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run one or more multithreaded jobs and measure throughput.

This is effectively a wrapper around gaudirun.py, which allows running
multi-job and multi-threaded jobs. It uses numactl if available to bind
each job to a numa domain. It also supports profiling.

Examples:
    Moore/run DBASE/PRConfig/scripts/benchmark-scripts/Moore_hlt1_pp_default.sh
    Moore/run DBASE/PRConfig/scripts/benchmark-scripts/Moore_hlt2_pp_thor.sh
    # See scripts under `scripts/benchmark-scripts' for other examples.

Note that for profiling we use a special perf executable to
    1. make `perf script` a bit faster
    2. make perf compatible with DWARF 5, by default emitted by GCC >=11
This executable is available on lbquantaperf01/02 and lbhltperf01 under
`/usr/local/bin/perf_libunwind_lcg101`. The difference is that this is a
perf version that is linked against libunwind and newer binutils from
LCG 101. We take libbfd from LCG 101 binutils but it's a static lib so
we only need libunwind at runtime. Thus to use it one has to make that
libunwind is able to be found, so for example
`Moore/run perf_libunwind_lcg101` will work.

To reproduce that perf executable:
    1. (setup recent compiler e.g. gcc10)
    2. clone linux kernel
    3. checkout v5.15-rc6
    4. make WERROR=0 VF=1 \
    EXTRA_CFLAGS="-I/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/include" \
    LDFLAGS="-L/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/lib" \
    LIBUNWIND_DIR=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/libunwind/1.3.1/x86_64-centos7-gcc11-opt

TODO:
- run each job (and profiler) in a different cwd
- integrate some of this into gaudirun.py?

"""
from __future__ import print_function, division
import atexit
import argparse
import datetime
import logging
import math
import os
import re
import shutil
import socket
import subprocess
import tempfile
from itertools import cycle, islice


def rep(x, length):
    return list(islice(cycle(x), length))


def lscpu():
    output = subprocess.check_output(['lscpu', '-p=NODE'])
    lines = [line for line in output.splitlines() if not line.startswith(b'#')]
    res = {
        'n_logical_cpus': len(lines),
        'n_numa_nodes': len(set(lines)),
    }
    assert res['n_logical_cpus'] % res['n_numa_nodes'] == 0
    return res


CPU_INFO = lscpu()
DEFAULT_CACHE_DIRS = {
    'default': [
        '/run/throughput/numa{}'.format(i)
        for i in range(CPU_INFO['n_numa_nodes'])
    ],
    # if special defaults are needed, add an item with FQDN as the key
}

# prefer XDG_RUNTIME_DIR which should be on tmpfs
FALLBACK_CACHE_DIR = os.getenv('XDG_RUNTIME_DIR', tempfile.gettempdir())


def default_cache_dirs():
    hostname = socket.getfqdn()
    dirs = DEFAULT_CACHE_DIRS.get(hostname, DEFAULT_CACHE_DIRS['default'])
    assert len(dirs) == CPU_INFO['n_numa_nodes']
    return dirs


def is_tmpfs(path):
    try:
        output = subprocess.check_output(['df', '-T', path])
    except subprocess.CalledProcessError:
        return False
    lines = output.splitlines()
    assert len(lines) == 2
    assert lines[0].split()[1] == b'Type'
    return lines[1].split()[1] == b'tmpfs'


def is_remote(url):
    return url.startswith('mdf:root:') or url.startswith('root:')


def has_command(cmd):
    try:
        with open(os.devnull, 'w') as FNULL:
            subprocess.check_call(cmd, stdout=FNULL, stderr=FNULL)
        return True
    except OSError as e:
        logging.debug(str(e))
        return False
    except subprocess.CalledProcessError as e:
        logging.debug(str(e))
        return e.returncode in [0, 1]


def proc_stats(pids):
    def parse_time(s):
        """Return seconds from a "HH:MM:SS" string."""
        return sum(m * int(x) for m, x in zip([3600, 60, 1], s.split(':')))

    now = datetime.datetime.now()
    stats = (now, 0, 0, 0)
    try:
        out = subprocess.check_output([
            "ps", "-o", "rss=", "-o", "time=", "-p", ",".join(
                str(p) for p in pids)
        ],
                                      encoding='utf8')
        data = [x.split() for x in out.splitlines()]
        rss_kB = sum(int(x[0]) for x in data)
        cputime = sum(parse_time(x[1]) for x in data)
        out = subprocess.check_output(["free"], encoding='utf8')
        free_kB = int(out.splitlines()[1].split()[3])

        cpuusage = 0
        if proc_stats.timestamp is not None and cputime > proc_stats.cputime:
            cpuusage = ((cputime - proc_stats.cputime) /
                        (now - proc_stats.timestamp).total_seconds())
        proc_stats.timestamp = now
        proc_stats.cputime = cputime

        stats = (now, cpuusage * 100, rss_kB / (1024 * 1024),
                 free_kB / (1024 * 1024))
    except subprocess.CalledProcessError:
        pass
    return stats


proc_stats.timestamp = None
proc_stats.cputime = 0


def run_gaudi_job(log_filename,
                  input_files,
                  args,
                  n_events,
                  numa_node=None,
                  profiler_numa_node=None,
                  parsed_throughput=0.):

    # build command line
    extra_options = """
from Moore import options
options.n_threads = {n_threads!r}
options.n_event_slots = {evt_slots!r}
options.evt_max = {n_evts!r}
options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.set_conds_from_testfiledb({TFDBkey!r})
options.input_type = 'MDF'
options.input_files = {inputs!r}
""".format(
        n_evts=n_events,
        evt_slots=args.evtSlots,
        n_threads=args.threads,
        inputs=input_files,
        TFDBkey=args.test_file_db_key)

    hlt_label = "HLT1" if "hlt1" in args.options[-1].lower() else "HLT2"

    cmd = ['gaudirun.py', '--option', extra_options
           ] + [os.path.expandvars(x) for x in args.options]

    if hlt_label != "HLT1":
        # only use tcmalloc in HLT2,
        # in HLT1 we see a small slowdown when using it
        cmd.insert(1, '-T')

    heaptrack_cmd = cmd[:]

    # prepend the command line with numactl if needed
    if numa_node is not None:
        cmd = ["numactl", "-N",
               str(numa_node), "-m",
               str(numa_node), "--"] + cmd

    if args.profile:
        # see module docstring for more info about perf
        perf_exe = "perf_libunwind_lcg101" if has_command(
            ["perf_libunwind_lcg101"]) else "perf"
        stack_mode = "fp" if args.use_fp else "dwarf,65528"
        cmd = [
            perf_exe,
            "record",
            "--event=cycles:up",
            "--aio",
            "--call-graph=" + stack_mode,
            "--count=40000000",  # event period to sample
            "--delay=50000",  # delay in msec before measuring
        ] + cmd

    # run the test
    logging.info("Launching job with cmd: {}".format(cmd))
    log_file = open(log_filename, 'w+')
    process = subprocess.Popen(cmd, stdout=log_file, stderr=subprocess.STDOUT)

    if args.profile:
        process.wait()

        has_heaptrack = os.path.isfile(args.heaptrack)
        ht_file = None
        if has_heaptrack:
            # Heaptrack tracks every allocation and is thus deterministic
            # therefore we don't really need to run over many events
            heaptrack_cmd[4] = heaptrack_cmd[4].replace(
                "max = {}".format(n_events), "max = {}".format(100))
            heaptrack_cmd[0] = subprocess.check_output(
                ['which', 'gaudirun.py'])[:-1]
            heaptrack_cmd = [args.heaptrack, 'python'] + heaptrack_cmd

            try:
                logging.info("Running Heaptrack")
                lines = subprocess.check_output(
                    heaptrack_cmd, stderr=subprocess.STDOUT).decode()

                m = re.search(r'heaptrack output will be written to "(.*?)"',
                              lines)
                if m:
                    ht_file = m.group(1)
                else:
                    logging.warning("Could not determine heaptrack file name")
                    raise RuntimeError(
                        "Could not determine heaptrack file name")

            except:
                import traceback
                traceback.print_exc()
                logging.warning("Error occured during heaptrack profile. "
                                "Results may not be produced or reliable.")

        logging.info("Making profile plots")
        from MooreTests import make_profile_plots
        make_profile_plots.main([log_filename], hlt_label, parsed_throughput,
                                ht_file, args.heaptrack, perf_exe)

    return process, log_file


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])
    parser.add_argument('options', nargs='+', help='Gaudi options files.')
    parser.add_argument(
        '-j',
        '--jobs',
        dest='n_jobs',
        type=int,
        default=None,
        help='Number of processes to launch (defaults to # numa nodes)')
    parser.add_argument(
        '-t',
        '--threads',
        type=int,
        default=None,
        help='Number of threads per job (defaults to # logical cpus / # jobs.')
    parser.add_argument(
        '-e',
        '--evtSlots',
        type=int,
        help='Number of event slots per job (defaults to max(1.2 * # threads, '
        '1 + # threads)')
    parser.add_argument(
        '-n',
        '--events',
        default=1000,
        type=lambda x: int(round(float(x))),
        help='nb of events to process per job')
    parser.add_argument(
        '--outfileTag',
        default='',
        help="Output file name tag, name will be 'ThroughputTest_TAG_...'")
    parser.add_argument(
        '--test-file-db-key',
        default='MiniBrunel_2018_MinBias_FTv4_MDF',
        help='TestFileDB key defining input files and tags.')
    parser.add_argument(
        '-f',
        '--inputs',
        nargs='+',
        help='Names of input files, multiple names possible (defaults to '
        'files from TestFileDB entry if not given)')
    parser.add_argument(
        '--avg-event-size',
        default=150 * 1000,
        type=int,
        help='Event size in bytes used to dermine the size of the input '
        'data to download')
    parser.add_argument(
        '--cache-dirs',
        default=None,
        help='Comma separated paths to directories, one per job, where the '
        'input files will be cached (default is hostname dependent or '
        '$XDG_RUNTIME_DIR).')
    parser.add_argument(
        '--nonuma',
        action='store_true',
        help='whether to disable usage of numa domains.')
    parser.add_argument(
        '--profile',
        action='store_true',
        help='Enable profiling (only supported for single job).')
    parser.add_argument(
        '--use-fp',
        action='store_true',
        help='whether to use frame pointer based stack unwinding. '
        '(needs frame pointers in binray, compile with -fno-omit-frame-pointer)'
    )
    parser.add_argument(
        '--debug', action='store_true', help='Debugging output')
    parser.add_argument(
        '--heaptrack',
        type=str,
        default="",
        help='Enable heaptrack profiling by providing path to executable')
    args = parser.parse_args()

    logging.basicConfig(
        format='%(levelname)-7s %(message)s',
        level=(logging.DEBUG if args.debug else logging.INFO))

    #the number of events could change for profiling jobs
    n_events = args.events
    throughput_for_plotlabel = 0

    if CPU_INFO['n_numa_nodes'] == 1:
        args.nonuma = True
    elif not args.nonuma:
        if not has_command(['numactl', '--help']):
            logging.warning(
                'numactl not found, running without binding to nodes')
            args.nonuma = True

    if args.profile:
        if not has_command(['perf']):
            raise RuntimeError(
                "perf command not found, please install the perf linux profiler"
            )
        args.n_jobs = args.n_jobs or 1
        if args.n_jobs > 1:
            parser.error("Profile only supported for single job")
        args.threads = args.threads or (
            CPU_INFO['n_logical_cpus'] // CPU_INFO['n_numa_nodes'])

        #search for latest throughput numbers in the default log file to potentially set the number of events, and label the FlameBars plot
        try:
            parsed_throughput = 0
            throughput_logfiles = [
                f for f in os.listdir(".") if 'ThroughputTest' in f
            ]
            for f in throughput_logfiles:
                lines = open(f, "r").readlines()
                for line in lines[::-1]:
                    m = re.search(r"Evts\/s = ([\d.]+)", line)
                    if m: parsed_throughput += float(m.group(1))
        except:
            logging.warning("Cannot parse throughput numbers.")
            parsed_throughput = 0

        if args.events == -1 and parsed_throughput == 0:
            logging.error(
                "-n=-1 specified but throughput parsing failed. Aborting!")
            exit(1)

        #parse the number of events such that the event loop runs for 90s (assumung there were 2 throughput jobs)
        throughput_for_plotlabel = parsed_throughput
        if args.events == -1:
            n_events = int(parsed_throughput * 45.)
    else:
        args.n_jobs = args.n_jobs or (1 if args.nonuma else
                                      CPU_INFO['n_numa_nodes'])
        if args.n_jobs != CPU_INFO['n_numa_nodes']:
            logging.warning("There are {} available numa nodes but you are "
                            "launching {} jobs".format(
                                CPU_INFO['n_numa_nodes'], args.n_jobs))
        args.threads = args.threads or (
            CPU_INFO['n_logical_cpus'] // args.n_jobs)

    # check total number of threads
    if args.n_jobs * args.threads > CPU_INFO['n_logical_cpus']:
        logging.warning(
            "CPU has {} threads but you specified {} jobs with {} threads "
            "each. This will overcommit the CPU.".format(
                CPU_INFO['n_logical_cpus'], args.n_jobs, args.threads))

    if args.evtSlots is None:
        args.evtSlots = max(int(round(1.2 * args.threads)), 1 + args.threads)

    tag = ''
    if args.outfileTag != '':
        tag = '_' + args.outfileTag + '_'

    # Set up local directories where inputs are cached
    if args.cache_dirs:
        args.cache_dirs = args.cache_dirs.split(',')
    else:
        args.cache_dirs = default_cache_dirs()
        if any(not os.path.isdir(d) for d in args.cache_dirs):
            fallback_dir = tempfile.mkdtemp(
                prefix='throughput-', dir=FALLBACK_CACHE_DIR)
            logging.warning(
                'not all default cache dirs {!r} exist, using {}'.format(
                    args.cache_dirs, fallback_dir))
            args.cache_dirs = [fallback_dir] * args.n_jobs
            # if we use the fallback directory, clean up after ourselves
            atexit.register(shutil.rmtree, fallback_dir)
    if len(args.cache_dirs) < args.n_jobs:
        args.cache_dirs = rep(args.cache_dirs, args.n_jobs)
        logging.warning('Recycling cache directories to {!r}'.format(
            args.cache_dirs))

    if not args.inputs:
        from PRConfig.TestFileDB import test_file_db
        inputs_fns = test_file_db[args.test_file_db_key].filenames
    else:
        inputs_fns = args.inputs

    # Distribute input files between jobs
    # - give a different set of files to each job, if possible
    if len(inputs_fns) < args.n_jobs:
        logging.warning('Too few input files: inputs will be recycled. '
                        'Some events will be processed more than once.')
        # recycle the list of inputs and cut it off at the minimal length
        inputs_fns = (inputs_fns * args.n_jobs)[:args.n_jobs]
    # - distribute the inputs alternating between jobs
    job_inputs = [inputs_fns[i::args.n_jobs] for i in range(args.n_jobs)]
    # - download inputs, get job input size and recycle inputs if needed
    #   based on # events
    for i, inputs in enumerate(job_inputs):
        # download files
        if all(is_remote(url) for url in inputs):
            from Moore.qmtest.context import download_mdf_inputs_locally
            logging.info('Downloading inputs for job {} to {}'.format(
                i, args.cache_dirs[i]))
            inputs[:] = download_mdf_inputs_locally(
                inputs,
                args.cache_dirs[i],
                max_size=n_events * args.avg_event_size)
        elif any(is_remote(url) for url in inputs):
            parser.error('inputs must either be all xrootd or all local')
        # recycle if needed
        sizes = [os.path.getsize(fn) for fn in inputs]
        n_rep = int(math.ceil(n_events / (sum(sizes) / args.avg_event_size)))
        if n_rep > 1:
            logging.warning(
                'Inputs for job {} are likely too few, recycling by a factor '
                'of {}'.format(i, n_rep))
            inputs.extend(inputs * (n_rep - 1))
    # - check and warn if some input files are used more than once
    unique_inputs = set().union(*job_inputs)
    if len(unique_inputs) < sum(len(i) for i in job_inputs):
        logging.warning('Some input files are given more than once. '
                        'Some events might be processed more than once.')

    slow_inputs = [path for path in unique_inputs if not is_tmpfs(path)]
    if slow_inputs:
        logging.warning('Inputs {!r} not on tmpfs, reading might be slower'.
                        format(slow_inputs))

    # Start all jobs
    runningJobs = []
    logfile_prefix = "Profile" if args.profile else "ThroughputTest"
    for i_job in range(args.n_jobs):
        if not args.nonuma:
            numa_node = i_job % CPU_INFO['n_numa_nodes']
            # put profiler on a different numa node
            profiler_numa_node = i_job % CPU_INFO['n_numa_nodes'] + 1
        else:
            numa_node = None
            profiler_numa_node = None

        log_filename = ('{:s}{:s}.{:s}.{:d}t.{:d}j.{:d}e.{:d}.log'.format(
            logfile_prefix, tag, os.environ['BINARY_TAG'], args.threads,
            args.n_jobs, n_events, i_job))
        logging.info('Launching job {}...'.format(i_job))
        runningJobs.append(
            run_gaudi_job(log_filename, job_inputs[i_job], args, n_events,
                          numa_node, profiler_numa_node,
                          throughput_for_plotlabel))

    # Wait for jobs to finish and collect results
    n_failed_jobs = 0
    throughputs = []
    pids = [j[0].pid for j in runningJobs]
    for i_job, (job, ofile) in enumerate(runningJobs):
        logging.info('Waiting for job {} to complete...'.format(i_job))
        while True:
            print('{} {:5.0f}% {:5.1f}G RSS {:5.1f}G free'.format(
                *proc_stats(pids)))
            try:
                retcode = job.wait(timeout=10)
                break
            except subprocess.TimeoutExpired:
                pass
            except KeyboardInterrupt:
                for j in runningJobs:
                    j[0].kill()
                raise

        if retcode != 0:
            logging.warning(
                "non-zero return code from job {} with output file {}".format(
                    i_job, ofile.name))
            n_failed_jobs += 1
            if args.debug and n_failed_jobs == 1:
                ofile.seek(0)
                for line in ofile:
                    print(line, end='')
        if not args.profile:
            # set file index to top of file
            ofile.seek(0)
            # read all lines from file
            lines = ofile.readlines()
            for line in lines[::-1]:
                m = re.search(r"Evts\/s = ([\d.]+)", line)
                if m:
                    throughputs.append(float(m.group(1)))
                    print("Throughput of job {} is {} Evts/s.".format(
                        i_job, throughputs[-1]))
                    break
            ofile.close()

    if not args.profile:
        print(
            "Throughput test is finished. Overall reached Throughput is Evts/s = {}"
            .format(sum(throughputs)))
        if n_failed_jobs:
            exit(
                'Some job(s) failed: check the warnings above for the retcode')
