#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
os.system("python $PRCONFIGROOT/python/MooreTests/RunL0App.py"
          " -n 10000"
          " --replay"
          " --TCK 0x1724"
          " --inputdata pNe2015NB_BE"
          " --outputfile pNe2015NB_L0Filt0x1724_BE.dst")
os.system("python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
          " -n 1000000000"
          " --settings Physics_5tev2017"
          " --L0TCK 0x1724"
          " --input_rate 500e3"
          " --inputdata pNe2015NB_L0Filt0x1724_BE.dst"
          " --TFDBForTags pNe2015NB_BE")
