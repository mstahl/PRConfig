###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
settings = "Physics_pp_Draft2016"
MooreOptions = dict(
    Simulation=False,
    UseTCK=False,
    ThresholdSettings=settings,
    DataType="2016",
    DDDBtag="dddb-20150724",
    CondDBtag="cond-20160522",
    RemoveInputHltRawBanks=True,
    EnableTimer=True,
    ForceSingleL0Configuration=True,
    TimingTest=True,
    HltLevel='Hlt1',
    Split='Hlt1')


def configure():
    from Configurables import HltConf
    HltConf().L0TCK = '0xFF61'
    from ConfigureJobs import configureOnlineConditions, removeHlt1Technical, getHlt1SelectionIDs
    configureOnlineConditions("Hlt1")
    removeHlt1Technical()
    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(lambda: getHlt1SelectionIDs(settings))
