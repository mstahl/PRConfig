###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
settings = "Physics_25ns_October2015"
MooreOptions = dict(
    Simulation=False,
    DataType="2015",
    DDDBtag="dddb-20150724",
    CondDBtag="cond-20150828",
    UseTCK=False,
    RemoveInputHltRawBanks=True,
    EnableTimer=True,
    ForceSingleL0Configuration=False,
    ThresholdSettings=settings,
    HltLevel='Hlt1',
    Split='Hlt1')


def configure():
    from Configurables import UpdateAndReset
    UpdateAndReset().abortRetroEvents = False
    from Configurables import HltConf
    HltConf().L0TCK = '0x0050'
    from Configurables import CondDB
    conddb = CondDB()
    conddb.EnableRunStampCheck = False
    from Gaudi.Configuration import appendPostConfigAction

    def getHlt1SelectionIDs():
        from Configurables import HltANNSvc
        import pickle
        Hlt1SelectionIDs = HltANNSvc().Hlt1SelectionID
        configuration = {settings: Hlt1SelectionIDs}
        pickle.dump(configuration,
                    open("hlt1SelectionIDs_%s.p" % (settings), "wb"))

    appendPostConfigAction(getHlt1SelectionIDs)
