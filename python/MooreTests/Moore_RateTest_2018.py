#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
# os.system(
#     "python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
#     " --settings Physics_pp_2018"
#     " --L0TCK=\"0x1707\""
#     " --inputdata=\"2017NB_L0Filt0x1707\""
# )
os.system("python $PRCONFIGROOT/python/MooreTests/RunL0App.py"
          " -n 30000"
          " --replay"
          " --TCK 0x1801"
          " --inputdata 2017NB_L0Filt0x1707"
          " --outputfile 2017NB_L0Filt0x1707_0x1801.dst")
os.system("python $PRCONFIGROOT/python/MooreTests/Moore_RateTest.py"
          " -n 1000000000"
          " --settings Physics_pp_2018"
          " --L0TCK 0x1801"
          " --inputdata 2017NB_L0Filt0x1707_0x1801.dst"
          " --TFDBForTags 2017NB_L0Filt0x1707")
