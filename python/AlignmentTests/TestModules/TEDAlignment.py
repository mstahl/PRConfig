###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
class Iterator(object):
    ## This module can either return a python string or a function that takes no
    ## arguments and returns the correct string.
    def __init__(self):
        pass

    def __call__(self):
        return "import TrAligIterator; TrAligIterator.doIt('TEDAlignment')"

    def alignmentType(self):
        return 'Velo'

    def gaudi(self):
        return True


class Analyzer(object):
    ## This module can either return a python string or a function that takes a
    ## single integer argument and returns the correct string.
    def __init__(self, input_files):
        self.__input_files = input_files

    def __call__(self, n):
        python_string = "import TrAligAnalyzer; TrAligAnalyzer.doIt('{0}', 'TEDAlignment', {1})"
        return python_string.format(self.__input_files[n], n)

    def gaudi(self):
        return True
