###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import os
import subprocess
from collections import defaultdict


def get_access_urls(bkkpath, evttype, filetypes, max_files=500):
    customEnv = {}

    # set custom grid proxy path if exists
    try:
        customEnv["X509_USER_PROXY"] = os.environ["X509_USER_PROXY"]
        print("Found X509_USER_PROXY set to {}".format(
            customEnv["X509_USER_PROXY"]))
    except:
        print("No X509_USER_PROXY found, continuing with defaults...")

    print("getting list of files from Dirac")
    stdout = subprocess.check_output(
        ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
        "lb-dirac dirac-dms-list-directory -B {} --EventType={} --FileType={}".
        format(bkkpath, evttype, ",".join(x.upper() for x in filetypes)),
        shell=True,
        env=customEnv,
        text=True,
    )

    # remove general directory name and number of files from list
    # e.g. "/lhcb/MC/Upgrade/XDIGI/00128250/0000/: 14 files, 0 sub-directories"
    file_list = [x for x in stdout.splitlines() if ":" not in x]

    # ensure files are always in same order
    file_list.sort()

    try:
        stdout = subprocess.check_output(
            ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
            "lb-dirac dirac-dms-lfn-accessURL --Terminal",
            shell=True,
            env=customEnv,
            text=True,
            input="\n".join(file_list[:max_files]))
    except subprocess.CalledProcessError as e:
        stdout = e.output

    urls = defaultdict(list)
    successful = False
    for line in stdout.splitlines():
        if line[0] != ' ':
            successful = line.startswith('Successful')
        else:
            if successful:
                m = re.match(r' +([^: ]*) *: *(.*)', line)
                if m:
                    urls[m.group(1)].append(m.group(2))

    # Get the first URL (if more than one) for each LFN, while skipping
    # LFNs for which we couldn't find an URL (e.g. a site was down).
    lfns = [urls[lfn][0] for lfn in file_list if lfn in urls]

    # Filter out some failing grid sites/files from the list
    excluded = ['stfc.ac.uk']
    return [lfn for site in excluded for lfn in lfns if site not in lfn]

    # TODO warn if some of the first N files was not resolved to a URL
    # since then one would get numerically different results.


if __name__ == "__main__":
    bkk_path = ("/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8"
                "/Sim10-Up08/Digi15-Up04")
    evt_type = "30000000"
    for url in get_access_urls(bkk_path, evt_type, ['XDIGI']):
        print(url)
