###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
TestFileDB: the database of files used for testing in the nightlies and elsewhere.

Usage: Add into this module a testfiles() object which maps a logical name for a dataset, like '2012_raw_default' to a list of files.

This module creates the dictionary {logicalname : <testfiles>)

Testfile object, minimum information:
   Minimally you must supply->
     1) a logical name
     2) a list of files
     3) a dictionary with { 'Date':<> , 'Format':<>, 'DataType' : <>}
     4) a meaningful comment as to why the file was added to the test DB
     (the database to add to, test_file_db)

So, minimally something like:

testfiles(
    'Dummy!!',
    ['/dev/null'],
    {
    'Author': 'Rob Lambert',
    'Format': 'DIGI',
    'DataType': '2011',
    'Date': '2013.05.31',
    },
    'Dummy test file, for testing the testfiles mechanism...',
    test_file_db
    )

The testfiles object has the concept of a qualifier, for which a small number are additionally known, such as the DDDB tag, CondDB tag, Production, Stripping, Reconstruction, etc.

For the list of possibilites, see PRConfig.TestFileObjects.testfiles.__known_qualifiers__

The testfiles object is smart enough to know how to add it into the EventSelector, e.g.:

from PRConfig.TestFileDB import test_file_db

test_file_db['2012_raw_default'].run() #pass and options to LHCbApp

or

test_file_db['2012_raw_default'].run(configurable=Moore())

---------------------------------

There is now an automated mechanism to append to this database, see:
-> $PRCONFIGROOT/scripts/AddToTestFilesDB.py

To check files in the database can be read, see:
-> $PRCONFIGROOT/scripts/TestReadable.py

To migrate files in this database to EOS/CERN-SWTEST, see:
-> $PRCONFIGROOT/scripts/MigrateToSWTEST.py

"""
#get the testfile object definition
from PRConfig.TestFileObjects import testfiles
from PRConfig.TestFileDBObject import TestFileDBDict

#the database to fill
test_file_db = TestFileDBDict()

###############  TEST FILES START HERE ###################
testfiles(
    'upgrade_DC19_01_Bs2JPsiPhi_MD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000079_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000015_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000026_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000036_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000049_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000061_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000080_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000081_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000003_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000005_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000027_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000052_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000074_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000034_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000044_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000070_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000078_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000020_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000043_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000059_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000076_1.xdigi',
        'root://hake12.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000018_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000029_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000041_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000058_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000077_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000030_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000053_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000082_1.xdigi',
        'root://shark10.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000063_1.xdigi'
    ], {
        'Author': 'Ross Hunter',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.12.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Bs2JpsiPhi (Jpsi -> mu+ mu-, Phi -> K+ K-), MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2JpsiPhiMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000081_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000083_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000037_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000039_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000061_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000073_1.xdigi',
        'root://shark6.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000002_1.xdigi',
        'root://shark6.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000066_1.xdigi'
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000003_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000020_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000041_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000049_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000067_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000004_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000021_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000030_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000042_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000059_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000082_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000016_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000045_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000075_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000084_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000019_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000035_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000044_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000068_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000031_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000054_1.xdigi',
    ], {
        'Author': 'Ross Hunter',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.12.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Bs2JpsiPhi (Jpsi -> mu+ mu-, Phi -> K+ K-), MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Dp2KSPip_MU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000067_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000078_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00093501/0000/00093501_00000002_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00093501/0000/00093501_00000003_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00093501/0000/00093501_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000007_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000010_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000006_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000012_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000004_1.xdigi',
        'root://door08.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000015_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000019_1.xdigi',
        'root://lhcb-sdpd23.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000001_1.xdigi',
        'root://whale2.grid.sara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00093501/0000/00093501_00000008_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.08.01',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Dp2KSPip, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Dp2KSPip_MD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000067_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00093503/0000/00093503_00000081_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000003_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000010_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000013_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000082_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000015_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000005_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000011_1.xdigi',
        'root://lhcb-sdpd19.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000004_1.xdigi',
        'root://lhcb-sdpd21.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00093503/0000/00093503_00000014_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.08.01',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Dp2KSPip, MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Z2mumuMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000067_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000079_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000016_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000020_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000030_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000038_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000044_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000050_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000052_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000054_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000056_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000062_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000063_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000066_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000068_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091373/0000/00091373_00000077_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000007_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000018_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000041_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000051_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000080_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000002_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000031_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000060_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000081_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000028_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000082_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000083_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000003_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000014_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000025_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000035_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000049_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000069_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000084_1.xdigi',
        'root://lhcb-sdpd12.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000004_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000006_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000008_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000023_1.xdigi',
        'root://lhcb-sdpd6.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000042_1.xdigi',
        'root://lhcb-sespd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000057_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000085_1.xdigi',
        'root://guppy16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000001_1.xdigi',
        'root://hake6.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000026_1.xdigi',
        'root://shark11.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091373/0000/00091373_00000086_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Z2mumu, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Z2mumuMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000080_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000082_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000083_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000086_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000016_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000018_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000033_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000045_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000056_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000079_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091833/0000/00091833_00000084_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000002_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000017_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000035_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000044_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000063_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000081_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000027_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000030_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000050_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000064_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000076_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000054_1.xdigi',
        'root://door08.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000066_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000073_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000077_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000003_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000032_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000060_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000075_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000085_1.xdigi',
        'root://lhcb-sdpd5.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000014_1.xdigi',
        'root://lhcb-sdpd15.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000039_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000057_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000067_1.xdigi',
        'root://lobster1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091833/0000/00091833_00000022_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Z2mumu, MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Dst2D0piMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000013_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000025_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000035_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000039_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000052_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000060_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000063_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000066_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091377/0000/00091377_00000081_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000016_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000058_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000064_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000065_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000067_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000070_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000071_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000073_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000074_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000082_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000028_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000042_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000054_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000056_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000083_1.xdigi',
        'root://door08.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000018_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000021_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000029_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000043_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000049_1.xdigi',
        'root://door08.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000084_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000001_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000009_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000022_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000050_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000061_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000085_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000032_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000034_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000068_1.xdigi',
        'root://hake13.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000002_1.xdigi',
        'root://hake16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000041_1.xdigi',
        'root://shark15.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091377/0000/00091377_00000086_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Dst2D0pi, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Dst2D0piMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000080_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000081_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000082_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000083_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000087_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000015_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000016_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000022_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000029_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000053_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000064_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091829/0000/00091829_00000079_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000024_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000039_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000047_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000058_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000062_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000071_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000021_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000031_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000059_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000072_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000084_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000020_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000043_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000068_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000077_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000085_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000001_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000008_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000034_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000052_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000078_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000086_1.xdigi',
        'root://lhcb-sdpd20.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000004_1.xdigi',
        'root://lhcb-sdpd20.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000042_1.xdigi',
        'root://guppy3.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000006_1.xdigi',
        'root://shark5.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000035_1.xdigi',
        'root://shark4.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000045_1.xdigi',
        'root://guppy3.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091829/0000/00091829_00000050_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Dst2D0pi, MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bd2DstmumuMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000080_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000081_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000083_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000086_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000090_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000002_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000018_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000026_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000041_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000055_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000074_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091379/0000/00091379_00000084_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000019_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000042_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000068_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000079_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000085_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000005_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000031_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000034_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000056_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000062_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000067_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000082_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000006_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000007_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000043_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000065_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000087_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000001_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000024_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000036_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000045_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000088_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000008_1.xdigi',
        'root://lhcb-sdpd13.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000044_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000069_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000075_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091379/0000/00091379_00000089_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Bd2Dstmumu, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bd2DstmumuMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000067_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000082_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000085_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000015_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000021_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000036_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000043_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000052_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000061_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000065_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000083_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091827/0000/00091827_00000086_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000004_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000007_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000032_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000040_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000062_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000068_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000069_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000075_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000084_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000033_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000059_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000081_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000087_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000005_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000027_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000051_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000064_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000071_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000088_1.xdigi',
        'root://door08.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000089_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000053_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000066_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000078_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000090_1.xdigi',
        'root://lhcb-sespd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000006_1.xdigi',
        'root://lhcb-sdpd3.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000038_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000058_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000080_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000091_1.xdigi',
        'root://lobster12.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000003_1.xdigi',
        'root://guppy12.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000025_1.xdigi',
        'root://guppy4.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000035_1.xdigi',
        'root://guppy11.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091827/0000/00091827_00000048_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Bd2Dstmumu, MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bd2KsteeMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000080_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000081_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000019_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000034_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000044_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000053_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000066_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000082_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091375/0000/00091375_00000085_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000017_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000040_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000061_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000063_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000065_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000077_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000083_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000005_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000006_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000023_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000038_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000059_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000076_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000084_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000036_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000049_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000086_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000001_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000014_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000021_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000087_1.xdigi',
        'root://lhcb-sdpd5.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000002_1.xdigi',
        'root://lhcb-sdpd23.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000004_1.xdigi',
        'root://lhcb-sespd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000008_1.xdigi',
        'root://lhcb-sdpd17.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000029_1.xdigi',
        'root://lhcb-sdpd5.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000057_1.xdigi',
        'root://lhcb-sdpd23.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000088_1.xdigi',
        'root://shark7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000007_1.xdigi',
        'root://guppy13.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000016_1.xdigi',
        'root://shark7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091375/0000/00091375_00000089_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, Bd2Kstee, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bd2KsteeMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000079_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000016_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000025_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000038_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000049_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000053_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000067_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091835/0000/00091835_00000087_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000001_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000022_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000036_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000060_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000081_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000021_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000027_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000039_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000048_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000058_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000069_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000071_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000074_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000082_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000002_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000004_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000008_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000019_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000023_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000055_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000084_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000007_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000033_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000061_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000068_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000078_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000085_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000042_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000063_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000064_1.xdigi',
        'root://lhcb-sdpd6.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000065_1.xdigi',
        'root://lhcb-sdpd6.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000083_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000086_1.xdigi',
        'root://mouse3.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000045_1.xdigi',
        'root://hake13.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091835/0000/00091835_00000075_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, Bd2KsteeMD, MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_B2JpsiK_ee_MU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00099854/0000/00099854_000000{0}_1.xdigi'
        .format(i) for i in [12, 16, 20, 24, 29, 32, 36, 38, 41, 43, 46]
    ], {
        'Author': 'Carla Marin',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2021.03.16',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Test sample of B2JpsiK with Jpsi2ee for upgrade studies, MagUp, Sim10-Up03-OldP8Tuning',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_MinBiasMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000073_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000006_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000012_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000042_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000010_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000018_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000040_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000062_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000070_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000004_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000025_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000034_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000043_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000074_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000014_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000039_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000057_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000058_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000075_1.xdigi',
        'root://lhcb-sdpd7.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000027_1.xdigi',
        'root://lhcb-sdpd15.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000037_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000054_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000056_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000072_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000002_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000003_1.xdigi',
        'root://guppy14.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000005_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000007_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000017_1.xdigi',
        'root://guppy14.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000021_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000052_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000066_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for upgrade studies, MinBias, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_MinBiasMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000074_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000005_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000010_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000021_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000040_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000055_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000066_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000073_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000018_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000028_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000051_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000057_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000070_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000017_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000029_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000035_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000043_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000060_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000064_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000002_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000024_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000038_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000050_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000006_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000013_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000020_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000042_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000049_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000059_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000063_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000065_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000067_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000004_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000007_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000032_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000047_1.xdigi',
        'root://lhcb-sdpd5.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000058_1.xdigi',
        'root://hake4.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000045_1.xdigi',
        'root://guppy5.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000061_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for upgrade studies, MinBias, MagDown',
    test_file_db=test_file_db)

testfiles(
    "upgrade_minbias_2019", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/minbias/00091069_00000001_1.digi"
    ], {
        "Author": "Biljana Mitreska",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2019.07.23",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20190223",
    }, "Upgrade minbias sample for testing the VP alignment", test_file_db)

testfiles(
    "upgrade_alignment_Bs2JpsiPhi", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/bs2jpsiphi/00091825_00000016_1.xdigi"
    ], {
        "Author": "Peter Griffith",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.04.05",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20190223",
    }, "Upgrade Bs2JpsiPhi sample for testing the Muon system alignment",
    test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2PhiPhiMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000038_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00087920/0000/00087920_00000039_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00087920/0000/00087920_00000040_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000012_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000021_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000033_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000034_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000004_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000005_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000041_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000042_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000043_1.xdigi',
        'root://shark10.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000002_1.xdigi',
        'root://shark16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000022_1.xdigi',
        'root://guppy16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000029_1.xdigi',
        'root://guppy16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00087920/0000/00087920_00000044_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
    },
    'Data Challenges test for tracking, Bs2phiphi, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2PhiPhiMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000014_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000025_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000041_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000054_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000061_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000074_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000081_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000001_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000007_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000031_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000067_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000077_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000082_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000030_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000050_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000063_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000083_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000017_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000036_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000071_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000085_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000028_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000070_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000078_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000086_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000002_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000043_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000076_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000084_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000087_1.xdigi',
        'root://lobster16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000004_1.xdigi',
        'root://mouse7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000033_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
    },
    'Data Challenges test for tracking, Bs2phiphi, MagDown',
    test_file_db=test_file_db)

testfiles(
    "TestData_stripping_PbNeSMOGcollision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00003427_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083198/0001/00083198_00010614_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00004715_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083198/0000/00083198_00004689_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2019.01.09",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 217951 PbNeSMOG data from 2018 collisions", test_file_db)

testfiles(
    "TestData_stripping_PbPbcollision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017884_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017873_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017858_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00015240_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2019.01.09",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 218177 PbPb data from 2018 collisions", test_file_db)

testfiles(
    "TestData_restripping_collision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119592_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119590_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119588_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119597_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119595_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119593_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 214741 pp data from 2018 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000242_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000245_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000500_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2017",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 195969 pp data from 2017 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision16_reco16", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00051630/0001/00051630_00013257_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00051630/0000/00051630_00000030_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00051630/0000/00051630_00001081_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00051630/0000/00051630_00001163_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2016",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 174858 pp data from 2016 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision15_reco15a", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00000298_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00000228_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00000236_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2015",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 164524 pp data from 2015 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision12_reco14", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000672_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000320_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000315_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000327_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000316_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000323_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000317_1.full.dst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 133399 pp data from 2012 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision11_reco14", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000029_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000010_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000047_1.full.dst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "DST",
        "DataType": "2011",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 89335 pp data from 2011 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "2018_DaVinciTests.stripping34_collision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030781_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030844_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030847_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030865_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030916_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037075_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037092_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037467_1.rdst",
    ], {
        "Author": "Carlos Vazquez Sierra",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2018.07.19",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping34_collision18_reco18"],
    }, "Run 209291 pp data from 2018 for Stripping34", test_file_db)

testfiles(
    "2017_DaVinciTests.stripping32_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000015_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000016_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000017_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000018_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000019_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000020_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000021_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000022_1.rdst",
    ], {
        "Author": "Carlos Vazquez Sierra",
        "Format": "RDST",
        "DataType": "2017",
        "Date": "2018.07.19",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping32_collision17_reco17"],
    }, "Run 202214 5 TeV pp data from 2017 for Stripping32", test_file_db)

testfiles(
    "2017_DaVinciTests.stripping33_collision17_reco17a_smog", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005160_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005161_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005162_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005163_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005164_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005165_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005166_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00073870/0000/00073870_00005167_1.rdst",
    ], {
        "Author": "Carlos Vazquez Sierra",
        "Format": "RDST",
        "DataType": "2017",
        "Date": "2018.07.19",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping33_collision17_reco17a_smog"],
    }, "Run 202253 pNe data from 2017 for Stripping33", test_file_db)

# Moore tests
testfiles(
    "S20r0p2_stripped_test", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/B2CC.S20r0p2.Dimuon.dst"
    ], {
        "Author": "Stefano Perazzini",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20p2", "moore.swim_s20p2"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test stripping v20r0p2 2013 DST including packed flavour tag classes. DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20r1p2_stripped_test", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/B2CC.S20r1p2.Dimuon.dst"
    ], {
        "Author": "Stefano Perazzini",
        "Format": "DST",
        "DataType": "2011",
        "Date": "2013.09.30",
        "Processing": "Stripping20r1p2",
        "Reconstruction": 14,
        "Stripping": "20r1p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test stripping v20r1p2 2013 DST including packed flavour tag classes. DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20_stripped_test", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00020567/0000/00020567_00006473_1.dimuon.dst"
    ], {
        "Author":
        "Vava Gligorov",
        "Format":
        "DST",
        "DataType":
        "2012",
        "Date":
        "2013.09.30",
        "Processing":
        "Stripping20",
        "Reconstruction":
        14,
        "Stripping":
        20,
        "Application":
        "DaVinci",
        "Simulation":
        False,
        "QMTests": [
            "moore.rerun_s20", "moore.swim_s20",
            "ioexample.copyreco14stripping20dsttoroot"
        ],
        "DDDB":
        "dddb-20120831",
        "CondDB":
        "cond-20120831"
    },
    "Test stripping v20r0 2013 DST *not* including packed flavour tag classes, for Moore v14r8 (nominal). ",
    test_file_db)

testfiles(
    "S20_v14r11_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00021211/0000/00021211_00009739_1.dimuon.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r11. ",
    test_file_db)

testfiles(
    "S20_v14r9_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00024183/0000/00024183_00009746_1.dimuon.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r9. ",
    test_file_db)

testfiles(
    "S20_v14r6_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00024183/0002/00024183_00020006_1.dimuon.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "head-20120413",
        "CondDB": "cond-20120730"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r6. ",
    test_file_db)

testfiles(
    "S20_v14r2_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00018117/0000/00018117_00002737_1.dimuon.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "head-20120413",
        "CondDB": "head-20120420"
    },
    "Test with v20r0 2013 DST *not* including packed flavour tag classes for Moore v14r2. ",
    test_file_db)

testfiles(
    "S20r0p2_v14r9_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0001/00030613_00018961_1.bhadroncompleteevent.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r9.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20r0p2_v14r8_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0001/00030613_00015694_1.bhadroncompleteevent.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r8.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20r0p2_v14r6_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030613/0000/00030613_00006296_1.bhadroncompleteevent.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "head-20120413",
        "CondDB": "cond-20120730"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r6.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20r0p2_v14r2_swim", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00030264/0000/00030264_00006362_1.bhadroncompleteevent.dst"
    ], {
        "Author": "Vava Gligorov",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20", "moore.swim_s20"],
        "DDDB": "head-20120413",
        "CondDB": "head-20120420"
    },
    "Test with v20r0 2013 DST *including* packed flavour tag classes for Moore v14r2.  DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S21_bhadron_mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00041836/0000/00041836_00000001_1.bhadron.mdst"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDST",
        "DataType": "2012",
        "Date": "2012",
        "Processing": "Stripping21",
        "Reconstruction": 14,
        "Stripping": "21",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["ioexample.copyreco14stripping21mdsttoroot"],
        "DDDB": "dddb-20130929-1",
        "CondDB": "cond-20141107"
    }, "Test with Reco14-Stripping21 BHADRON.MDST from 2012", test_file_db)

testfiles(
    "2018_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2018/RAW/FULL/LHCb/COLLISION18/209291/209291_0000000745.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2018",
        "Date": "2018.05.26",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2018magdown"],
    },
    "Proton-Proton collision raw data FULL stream, run 209291 from 26th May 2018. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2017_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2017/RAW/FULL/LHCb/COLLISION17/198660/198660_0000000211.raw"
    ], {
        "Author":
        "Marco Cattaneo",
        "Format":
        "MDF",
        "DataType":
        "2017",
        "Date":
        "2017.09.06",
        "Application":
        "Moore",
        "Simulation":
        False,
        "QMTests": [
            "brunel.2017magup", "l0app.data_xxx",
            "DaVinciTests.davinci.gaudipython_algs"
        ],
    },
    "Proton-Proton collision raw data FULL stream, run 198660 from 6th September 2017. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2016_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2016/RAW/FULL/LHCb/COLLISION16/179277/179277_0000000601.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2016",
        "Date": "2016.07.05",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2016magup"],
    },
    "Proton-Proton collision raw data FULL stream, run 179277 from 7th July 2016. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2015_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2015/RAW/FULL/LHCb/COLLISION15/164668/164668_0000000744.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2015",
        "Date": "2015.10.02",
        "Application": "Moore",
        "Simulation": False,
        "QMTests":
        ["brunel.2015magdown, brunel.compatibility.2015-latest-*db"],
    },
    "Proton-Proton collision raw data FULL stream, run 164668 from 2nd October 2015. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2009_raw", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63497/063497_0000000001.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2009",
        "Date": "2009.12.06",
        "Simulation": False,
        "QMTests": ["brunel.fixedfile-brunel2009-*"],
    },
    "Proton-Proton collision raw data FULL stream, run 32474 from 12th December 2009. Magnet on, all detectors in readout, Velo open",
    test_file_db)

testfiles(
    "2008_cosmics", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2008/RAW/LHCb/COSMICS/35537/035537_0000088110.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2008",
        "Date": "2008",
        "Simulation": False,
        "QMTests": ["brunel.brunel-cosmics"],
    }, "Cosmics run, with CALO, OT and  RICH2 (35569 events)", test_file_db)

testfiles(
    "2008_TED", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2008/RAW/LHCb/BEAM/32474/032474_0000081642.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2008",
        "Date": "2008.09",
        "Simulation": False,
        "QMTests": ["brunel.brunel-2008"],
    }, "TED data from September 2008 with velo - prev2", test_file_db)

testfiles(
    "2012_raw_default", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_default/1318"
        + str(n) + "_0x0046_NB_L0Phys_00.raw" for n in range(83, 100)
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.physics.2012", "moore.database.2012", "l0app."],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "L0 Phys, L046, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets",
    test_file_db)

testfiles(
    "2012_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2012/RAW/FULL/LHCb/COLLISION12/114753/114753_0000000"
        + n + ".raw" for n in ["015", "016", "017", "298"]
    ], {
        "Author": "Jaap Panman",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["lumialgs"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Direct raw data from the pit taken in 2012. Used for Rec/LumiAlgs tests so that there are lumi events, etc. copied out of the freezer",
    test_file_db)

testfiles(
    "2012_raw_express", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2012/RAW/EXPRESS/LHCb/COLLISION12/124915/124915_0000000"
        + n + ".raw" for n in ["008", "112"]
    ], {
        "Author": "Jaap Panman",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["lumialgs"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Direct raw data from the pit taken in 2012 from the express stream. Used for Rec/LumiAlgs tests so that there are lumi events, etc. copied out of the freezer",
    test_file_db)

testfiles(
    "2012_raw_small", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_small/131883_0x0046_NB_L0Phys_00_3ev.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_small/131884_0x0046_NB_L0Phys_00_3ev.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_small/131886_0x0046_NB_L0Phys_00_1000ev.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_small/131885_0x0046_NB_L0Phys_00_5ev.raw"
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["filememrger.mdf"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "L0 Phys, L046, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets, reduced to be only 4 files and 1011 events for testing mdf merging",
    test_file_db)

testfiles(
    "2012_raw_L041", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_L041/2012_raw_L041.mdf"
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.vdm"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "L0 Phys, L041, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets, originally L046, re-emulated to L041.",
    test_file_db)

testfiles(
    "2012_raw_L03D", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_L03D/1147"
        + str(n) + "_0x003D_NB_L0Phys_00.raw"
        for n in list(range(74, 65, -1)) + list(range(63, 50, -1))
    ] + [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_L03D/1153"
        + str(n) + "_0x003D_NB_L0Phys_00.raw"
        for n in list(range(70, 74)) + list(range(75, 90))
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.physics.2012"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "L0 Phys, L03D, from early 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets",
    test_file_db)

testfiles(
    "2011_raw_default", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/81349_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/80881_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/79647_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/79646_0x002a_MBNB_L0Phys.raw"
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2011",
        "Date": "2011.01.07",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.database.2011"],
        "DDDB": "head-20110302",
        "CondDB": "head-20110622"
    },
    "L0 Phys, L002, from 2011 data, used previously for run_more script in Moore tests. Now no longer used there.",
    test_file_db)

testfiles(
    "2010_raw_default", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_raw_default/075161_0000000282.raw"
    ], {
        "Author": "LHCb",
        "Format": "MDF",
        "DataType": "2010",
        "Date": "2010.07.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["compatibility", "moore.database.2010"],
        "DDDB": "head-20101026",
        "CondDB": "head-20101112"
    },
    "Raw data used for 'fixed file' compatibilty tests of old Moore with latest databases. Found from the book-keeping some time in 2010.",
    test_file_db)

testfiles(
    "2010_digi_default", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_digi_default/00007107_00000001_2.digi"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2010",
        "Date": "2010.12.07",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["compatibility"],
        "DDDB": "head-20100624",
        "CondDB": "sim-20100624-vc-md100"
    },
    "2010 simulation Digi files for data used for 'fixed file' compatibilty tests of old Moore with latest databases. Found from the book-keeping some time in 2010.",
    test_file_db)

testfiles(
    "Sim08_2012_sim_pGun", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_pGun/00042604_1.sim"
    ], {
        "Author": "Gloria Corti",
        "Format": "SIM",
        "DataType": "2012",
        "Date": "2015.02.18",
        "Application": "Gauss",
        "Simulation": True,
        "QMTests": [],
        "DDDB": "dddb-20130929-1",
        "CondDB": "sim-20130522-1-vc-md100"
    },
    "2012 simulation Sim file for signal particle gun used for tests of Boole processing.",
    test_file_db)

testfiles(
    "Sim08_2012_digi_pGun", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_pGun/00042604_2.digi"
    ], {
        "Author": "Gloria Corti",
        "Format": "DIGI",
        "DataType": "2012",
        "Date": "2015.02.18",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["brunel.sim08-pgun"],
        "DDDB": "dddb-20130929-1",
        "CondDB": "sim-20130522-1-vc-md100"
    },
    "2012 simulation Digi files for signal particle gun used for tests of Brunel special configuration.",
    test_file_db)

testfiles(
    "Sim08_2012_digi", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08-2012/Gauss-v45r0-10000000-50ev-20130313-HLT.digi"
    ], {
        "Author":
        "Gloria Corti",
        "Format":
        "DIGI",
        "DataType":
        "2012",
        "Date":
        "2013.03.13",
        "Application":
        "Moore",
        "Simulation":
        True,
        "QMTests": [
            "brunel.sim08", "brunel.sim08-invfield-down",
            "brunel.sim08-invfield-up"
        ],
        "DDDB":
        "dddb-20130312-1",
        "CondDB":
        "sim-20130522-1-vc-md100"
    },
    "2012 simulation minimum bias Digi files (Gauss v45r0, Boole v26r3, Moore v14r8p1 used for tests of Brunel Sim08 settings",
    test_file_db)

testfiles(
    "Sim09_2016_digi", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/debug/2016/DIGI/00073919/0000/00073919_00000001_4.digi"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "DIGI",
        "DataType": "2016",
        "Date": "2018.05.29",
        "Application": "Moore",
        "Simulation": True,
        "QMTests": ["brunel.sim09-2016"],
        "DDDB": "dddb-20170721-3",
        "CondDB": "sim-20170721-2-vc-md100"
    },
    "2016 Sim09 Digi, event type 49000041, /MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/ (Boole step 133533, Moore steps 130089, 130090)",
    test_file_db)

testfiles(
    "Charm_Strip20_Test", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Charm_Strip20_Test/00022726_00111043_1.CharmToBeSwum.dst'
    ], {
        "Author": "Nick Torr",
        "Format": "DST",
        "DataType": "2012",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Date": "2013.02.15",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.swim_strip20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20121025"
    },
    "DST of offline-selected charm physics events for testing the swimming. See task #40021",
    test_file_db)

testfiles(
    "BsMuMu_Strip20_Test", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/BSMUMUUNBLIND.DST/00012992/0000/00012992_00000010_1.bsmumuunblind.dst'
    ], {
        "Author": "Xabier Cid Vidal",
        "Format": "DST",
        "DataType": "2011",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Date": "2013.01.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.swim_strip20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20121025"
    },
    "DST of offline-selected Bs->mumu physics events for testing the swimming/re-running the trigger, copy of an original from production. See task #42208",
    test_file_db)

testfiles(
    "Sim08_2012_ForMoore", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCYes-10000000-100ev-20130225.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCNo-10000000-100ev-20130304.digi"
    ], {
        "Author": "Gloria Corti",
        "Format": "DIGI",
        "DataType": "2012",
        "Processing": "Sim08",
        "Date": "2013.02.26",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.mc2012", "l0app"],
        "DDDB": "dddb-20120831",
        "CondDB": "sim-20121025-vc-md100"
    },
    "Gauss/Boole output for testing Moore v14r8p6 in preparation for large scale 2012 simulation, Sim08, with Reco14 Stripping20. see task #39714",
    test_file_db)

testfiles(
    "Sim08_2012_L045", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_L045/HepMCYes-10000000-100ev-20130225-L045.digi"
    ], {
        "Author": "Gloria Corti, Rob Lambert",
        "Format": "DIGI",
        "DataType": "2012",
        "Processing": "Sim08",
        "Date": "2013.02.26",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.mc2012"],
        "DDDB": "dddb-20120831",
        "CondDB": "sim-20121025-vc-md100"
    },
    "Generated from Sim08_2012_ForMoore with L0 0x0045 added by L0App, for testing of new running of Moore MC after L0App.",
    test_file_db)

testfiles(
    "Sim08_2012_L044", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_L044/HepMCYes-10000000-100ev-20130225-L044.digi"
    ], {
        "Author": "Gloria Corti, Rob Lambert",
        "Format": "DIGI",
        "DataType": "2012",
        "Processing": "Sim08",
        "Date": "2013.02.26",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.mc2012"],
        "DDDB": "dddb-20120831",
        "CondDB": "sim-20121025-vc-md100"
    },
    "Generated from Sim08_2012_ForMoore with L0 0x0044 added by L0App, for testing of new running of Moore MC after L0App.",
    test_file_db)

testfiles(
    "Sim08_2011_ForMoore", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2011_ForMoore/Sim08-2011-Boole-10000000-20130508-100ev.digi"
    ], {
        "Author": "Gloria Corti",
        "Format": "DIGI",
        "DataType": "2011",
        "Processing": "Sim08",
        "Date": "2013.05.08",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.run_moore_mc2011"],
        "DDDB": "dddb-20130503",
        "CondDB": "sim-20130503-vc-md100"
    },
    "Gauss/Boole output for testing Moore v12r8g3 in preparation for large scale 2011 re-simulation with, Sim08, Reco14, Stripping20. see task #40023",
    test_file_db)

testfiles(
    "BsPhiPhi_TISTOSTest", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2011/ALLSTREAMS.DST/00026133/0000/00026133_00000013_1.allstreams.dst"
    ], {
        "Author": "Sean Benson",
        "Format": "DST",
        "DataType": "2011",
        "Processing": "Sim08",
        "Date": "2014.08.23",
        "Application": "DaVinci",
        "Simulation": True,
        "QMTests": ["moore.tistos"],
        "DDDB": "dddb-20130503",
        "CondDB": "sim-20130503-vc-md100"
    },
    "Bs->phi phi MC DST, to check sensible and consistent trigger efficiency is found from the Hlt",
    test_file_db)

testfiles(
    "2015NB_25ns_L0Filt0x0050", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0x0050/2015NB_25ns_0x0050_'
        + str(n) + '.mdf' for n in range(0, 9)
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2015.12.10",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828"
    }, "2015NB data that has been filtered through 0x0050", test_file_db)

testfiles(
    "2016_Hlt1_0x11291605",
    [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11291605/Run_0176198_20160605-%s.hltf1001_%s.mdf'
        % (f[0], f[1])
        for f in [['124747', '000'], ['124859', '001'], ['125010', '002'],
                  ['125121', '003'], ['125232', '004'], ['125345', '005'],
                  ['125457', '006'], ['125609', '007'], ['125722', '008'],
                  ['125834', '009'], ['125946', '010'], ['130058', '011'],
                  ['130211', '012'], ['130325', '013'], ['130437', '014'],
                  ['130550', '015'], ['130819', '016'], ['130932', '017'],
                  ['131046', '018'], ['131201', '019'], ['131316', '020'],
                  ['131433', '021'], ['131550', '022'], ['131707', '023'],
                  ['131823', '024'], ['131956', '025'], ['132114', '026'],
                  ['132231', '027'], ['132348', '028'], ['132623', '029'],
                  ['132741', '030'], ['132858', '031'], ['133017', '032'],
                  ['133134', '033'], ['133252', '034'], ['133409', '035'],
                  ['133525', '036'], ['133642', '037'], ['133800', '038'],
                  ['133918', '039'], ['134036', '040'], ['134150', '041'],
                  ['134414', '042'], ['134524', '043']]  #,
        #'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11291605/Run_0176263_20160606-%s.hltf1001_%s.mdf' %(f[0],f[1]) for f in [['053944','044'],['054206','045'],['054321','046'],['054435','047'],['054551','048'],['054705','049'],['054820','050'],['054935','051'],['055050','052'],['055205','053'],['055322','054'],['055437','055'],['055552','056'],['055706','057'],['055821','058'],['055935','059'],['060012','060'],['060049','061'],['060204','062'],['060319','063'],['060436','064'],['060552','065'],['060708','066'],['060824','067'],['060940','068'],['061054','069'],['061206','070'],['061319','071'],['061433','072'],['061546','073'],['061701','074'],['061931','075'],['062047','076'],['062203','077'],['062314','078'],['062425','079'],['062538','080'],['062651','081'],['062803','082'],['062917','083'],['063029','084'],['063144','085'],['063256','086'],['063409','087'],['063521','088'],['063634','089'],['063711','090'],['063749','091'],['063902','092']]
    ],
    {
        "Author": "Mark Williams",
        "Format": "MDF",
        "DataType": "2016",
        "Processing": "Moore",
        "Date": "2016.06.06",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517",
        "InitialTime": "2016-06-05 11:46:00 UTC",
    },
    "2016 data accepted by Hlt1 TCK 0x11291605",
    test_file_db)

testfiles(
    "2016_Hlt1_0x11361609", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11361609/Run_0179276_20160705-%s.hlta0101_%s.mdf'
        % (f[0], f[1])
        for f in [['162951', '000'], ['163211', '001'], ['163432', '002'],
                  ['163656', '003'], ['163917', '004'], ['164139', '005'],
                  ['164401', '006'], ['164622', '007'], ['164843', '008'],
                  ['165107', '009'], ['165330', '010'], ['165556', '011']]
    ], {
        "Author": "Jessica Prisciandaro",
        "Format": "MDF",
        "DataType": "2016",
        "Processing": "Moore",
        "Date": "2016.07.05",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160522",
        "InitialTime": "2016-07-05 15:29:00 UTC",
    }, "2016 data accepted by Hlt1 TCK 0x11361609", test_file_db)

testfiles(
    "2015NB_25ns_L0Filt0xFF60", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xFF60/2015NB_25ns_0xFF60_%s_%s.mdf'
        % (f[0], f[1]) for f in
        [[162530, '001'], [162531, '002'], [162533, '003'], [162535, '004'],
         [162538, '005'], [162540, '006'], [162544, '007'], [162546, '008'],
         [162549, '009'], [162553, '010'], [162559, '011'], [162600, '012'],
         [162623, '013'], [162871, '014'], [162873, '000'], [162873, '015'],
         [162874, '016'], [162877, '017'], [162880, '018'], [162881, '019'],
         [162883, '020'], [162884, '021'], [162949, '022']]
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.03.30",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828"
    }, "2015NB data that has been filtered through 0xFF60", test_file_db)

testfiles(
    "2015NB_25ns_L0Filt0x1606", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Run2_L0Processed_0x1606/25ns_NoBias/2015NB_25ns_0x1606_%03d.mdf'
        % (i, ) for i in range(0, 475)
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.04.16",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517"
    }, "2015NB data that has been filtered through 0x1606 using the EFF",
    test_file_db)

testfiles(
    "2015NB_25ns_L0Filt0x1600", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0x1600/2015NB_25ns_0x1600_0.mdf'
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.04.13",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828"
    }, "2015NB data that has been filtered through 0x1600", test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x1609",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609/2016NB_0x1609_%s.mdf'
        % f for f in [
            '174822_01', '174823_01', '174824_01', '175266_01', '175266_02',
            '175269_02', '175363_01', '175364_01', '175364_02', '175431_02',
            '175492_02', '175585_01', '175585_02', '175589_01'
        ]
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517",
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-18',
        'Application': 'Moore'
    },
    comment='2015NB data that has been filtered through 0x1609 with L0App',
    test_file_db=test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x160B",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160B/2016NB_0x160B_%s.mdf'
        % i for i in [
            '174819_01', '174822_01', '174823_01', '174824_01', '175266_01',
            '175266_02', '175269_01', '175269_02', '175359_01', '175363_01',
            '175364_01', '175364_02', '175385_01', '175431_01', '175431_02',
            '175434_01', '175492_01', '175492_02', '175497_01', '175499_01',
            '175580_01', '175584_01', '175585_01', '175585_02', '175589_01'
        ]
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick, Mika Vesterinen',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517",
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-07-25',
        'Application': 'Moore'
    },
    comment='2016NB data that has been filtered through 0x160B with L0App',
    test_file_db=test_file_db)

testfiles(
    "2015_pbpb_raw_full", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2015/RAW/FULL/LHCb/LEAD15/169329/169329_0000000062.raw'
    ], {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Date": "2016.02.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2015magdown"],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828",
    },
    "Lead-Lead collision raw data FULL stream, run 169329 from 8th December 2015",
    test_file_db)

testfiles(
    "2015NB_25ns_L0Filt0xFF61", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2015NB_25ns_L0Filt0xFF61/2015NB_25ns_0xFF61_'
        + str(n) + '.mdf' for n in range(0, 15)
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.02.17",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828"
    }, "2015NB data that has been filtered through 0xFF61", test_file_db)

testfiles(
    "pNe2015NB_L0Filt0x1608_small",
    [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BB_small.mdf',  # from 500 nobias events, run 161119
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BE_small.mdf',  # from 2500 nobias events
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_EB_small.mdf',  # from 2500 nobias events
    ],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.06.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150805",
        "InitialTime": "2015-08-25 03:00:00 UTC",  # during run 161119
    },
    "2015 pNe nobias data that has been filtered through 0x1608 (few hundred events)",
    test_file_db)

testfiles(
    "pNe2015NB_BE",
    [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe_NoBias_BEXing/{0}/output/protonNeon_NoBias.raw'
        .format(i) for i in set(range(161, 235)).difference(
            [168, 171, 185, 188, 189, 194, 195, 196, 198, 201, 210, 211, 215])
    ],
    {
        "Author": "Yanxi Zhang",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2017.11.02",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150805",
        "InitialTime": "2015-08-25 03:00:00 UTC",  # during run 161119
    },
    "2015 pNe nobias data that has been filtered through 0x1608 (few hundred events)",
    test_file_db)

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_8.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagUp_L0Processed_0x160F/L0Processed_8.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagDown_L0Processed_0x160F/L0Processed_6.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2Psi2SPhi_MagUp_L0Processed_0x160F/L0Processed_4.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname=
    '2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_21.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_23.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08g',
        'Simulation': True,
        'CondDB': 'sim-20150119-3-vc-md100',
        'DDDB': 'dddb-20150119-3',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname=
    '2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_22.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08g',
        'Simulation': True,
        'CondDB': 'sim-20150119-3-vc-mu100',
        'DDDB': 'dddb-20150119-3',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

###############  DUMMY FILES ###################

testfiles(
    "Dummy!!", ["/dev/null"], {
        "Author": "Rob Lambert",
        "Format": "DIGI",
        "DataType": "2011",
        "Date": "2013.05.31",
    }, "Dummy test file, for testing the testfiles mechanism...", test_file_db)

###############  Auto-additions are appended to this file ###################
#
# There is now an automated mechanism to append to this database.
# See: $PRCONFIGROOT/scripts/AddToTestFilesDB.py

### auto created ###
testfiles(
    myname='2011_25ns_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2011/RAW/FULL/LHCb/COLLISION11_25/103053/103053_0000000035.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 15:03:01.559482',
        'DDDB': 'dddb-20130503',
        'CondDB': 'cond-20130522',
        'QMTests': 'brunel.brunel_25ns'
    },
    comment=
    'RAW data file with events from run 103053, fill 2186 on 2011-10-07, 25ns bunch spacing. DB tags are default 2011 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2010_MagUp_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69623/069623_0000000003.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 15:56:50.465614',
        'DDDB': 'head-20110721',
        'CondDB': 'head-20110614',
        'QMTests': 'brunel.brunel2010magup'
    },
    comment=
    'Default RAW file for 2010 MagUp tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2010_MagOff_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69947/069947_0000000004.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 16:01:05.210397',
        'QMTests': 'brunel.brunel2010magoff'
    },
    comment=
    'Default RAW file for 2010 MagOff tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2009_dst_read_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2009/DST/00006290/0000/00006290_00000001_1.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20101206',
        'Date': '2014-01-21 10:37:23.196590',
        'QMTests': 'ioexample.copy2009dsttoroot'
    },
    comment='Official POOL DST from 2009',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Reco10-sdst-10events',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Reco10-sdst-10events/00011652_00000001_1-evt-18641to18650.sdst'
    ],
    qualifiers={
        'Format': 'SDST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'Reco10',
        'Simulation': False,
        'CondDB': 'head-20110622',
        'DDDB': 'head-20110302',
        'Reconstruction': 10,
        'Date': '2014-01-21 15:14:33.219187',
        'Application': 'Brunel',
        'QMTests': 'ioexample.testunpacktrack'
    },
    comment=
    '10 events from Reco10 SDST, including one testing UnpackTrack bug fix in EventPacker v2r10',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-dst/00001820_00000001.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20090402-vc-mu100',
        'DDDB': 'head-20090330',
        'Date': '2014-01-21 17:20:23.041861',
        'Application': 'Brunel',
        'QMTests': 'ioexample.copypooldsttoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v18r0, Brunel v34r5',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-digi/30000000-100ev-20090407-MC09.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20090508-vc-mu100',
        'DDDB': 'head-20090508',
        'Date': '2014-01-21 17:47:29.732855',
        'Application': 'Boole',
        'QMTests': 'ioexample.copypooldigitoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v19r2',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-sim/30000000-100ev-20090407-MC09.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20090402-vc-mu100',
        'DDDB': 'head-20090330',
        'Date': '2014-01-21 17:51:25.261926',
        'Application': 'Gauss',
        'QMTests': 'ioexample.copypoolsimtoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2012_raw_nu6',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2012/RAW/NOBIAS/LHCb/COLLISION12/125745/125745_0000000007.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2012/RAW/NOBIAS/LHCb/COLLISION12/125745/125745_0000000018.raw'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120730',
        'DDDB': 'head-20120413',
        'Date': '2014-02-05 16:46:08.788714',
        'Application': 'Moore'
    },
    comment=
    'Run 125745, nu around 6, high-occupancy data for stress-testing Moore and timing tests of tracking.',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2011_smallfiles_EW',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000049_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010654_00000049_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000040_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000168_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000163_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000014_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000002_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000003_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00011760_00000130_1.dimuon.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00008385_00000552_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2011',
        'Processing': 'Stripping13b',
        'Simulation': False,
        'CondDB': 'head-20111102',
        'DDDB': 'head-20110302',
        'Stripping': 'Stripping12',
        'Reconstruction': 'Reco08',
        'Date': '2014-02-12 11:43:35.705405',
        'Application': 'DaVinci',
        'QMTests': 'lhcbalgs.fsr-small-files-root'
    },
    comment=
    'Very small files generated in S13b, which have a few events only, but a lot of FSRs potentially. Used to test if FSRs really work properly. Copied from the grid by rlambert in 2011.',
    test_file_db=test_file_db)

testfiles(
    myname='2010_justFSR_EW',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_justFSR_EW/JustFSR.2010.EW.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2010',
        'Processing': 'Stripping12',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101026',
        'Stripping': 'Stripping12',
        'Reconstruction': 'Reco08',
        'Date': '2014-02-12 11:43:35.705405',
        'Application': 'DaVinci',
        'QMTests':
        ['lhcbalgs.fsr-small-files-root', 'lhcbalgs.fsr-only-file.root']
    },
    comment=
    'A file which contains nothing but FSRs. Early on in FSR development, files with only FSRs were not readable. This file is kept to test against future re-occurance of that.',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='boole.boole-mc11',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.boole-mc11/Gauss-10000000-100ev-20111014.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20110723-vc-md100',
        'DDDB': 'head-20110914',
        'Date': '2014-02-13 09:02:22.212231',
        'Application': 'Gauss',
        'QMTests': 'boole.boole-mc11'
    },
    comment=
    'Generic b events with spillover from Gauss v40r4, settings as in $APPCONFIGOPTS/Gauss/beam35000GeV-md100-MC11-nu2-50ns.py',
    test_file_db=test_file_db)

testfiles(
    myname='MC2012.dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2012/ALLSTREAMS.DST/00024784/0000/00024784_00000001_1.allstreams.dst'
    ],
    qualifiers={
        'Author': 'jonrob',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'Sim08-20130326-1-vc-md100',
        'DDDB': 'Sim08-20130326-1',
        'Date': '2014-07-23 09:02:22.212231',
        'Application': 'Brunel',
        'QMTests': ['mc2012-testread']
    },
    comment='Example MC2012 DST (JpsiK)',
    test_file_db=test_file_db)

testfiles(
    myname='mc11a-xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2011/XDST/00012935/0000/00012935_00000827_3.xdst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'XDST',
        'Simulation': True,
        'CondDB': 'sim-20111020-vc-md100',
        'DDDB': 'head-20110914',
        'Date': '2014-02-13 09:02:22.212231',
        'Application': 'Brunel',
        'QMTests': ['boole.boole-mc11a-xdst', 'brunel.brunel-mc11a-xdst']
    },
    comment='Inclusive b events Beam3500GeV-2011-MagDown-Nu2-50ns-EmNoCuts',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='boole.fixedfile-gauss-v39r4-mc10-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.fixedfile-gauss-v39r4-mc10-sim/Gauss-v39r4-30000000-100ev-20110902-MC10.sim'
    ],
    qualifiers={
        'Author':
        'cattanem',
        'DataType':
        '2010',
        'Format':
        'SIM',
        'Simulation':
        True,
        'CondDB':
        'sim-20101210-vc-md100',
        'DDDB':
        'head-20101206',
        'Date':
        '2014-02-13 10:04:53.289232',
        'Application':
        'Gauss',
        'QMTests': [
            'boole.fixedfile-gauss-v39r4-mc10-sim', 'boole.boole-mc10',
            'boole.boole-mc10-notruth', 'boole.boole-fest',
            'boole.boole-2010-latestdb'
        ]
    },
    comment='MC10 file produced with Gauss v39r4',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel-mc10',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-mc10/Boole-v21r8p1-MC2010Tuning.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20101026-vc-mu100',
        'DDDB': 'head-20101026',
        'Date': '2014-02-13 16:13:31.976938',
        'Application': 'Boole',
        'QMTests': 'brunel.brunel-mc10-withtruth'
    },
    comment='Boole v21r8p1 digi file for 2010 MC tuning',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009/Boole-v19r8-30000000-100ev-20091112-vc-md100.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:21:20.645501',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi'
    },
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-sim2010',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-sim2010/Boole-v21r9-00007107_00000001_2.digi'
    ],
    qualifiers={
        'Author':
        'cattanem',
        'DataType':
        '2010',
        'Format':
        'DIGI',
        'Simulation':
        True,
        'CondDB':
        'sim-20100624-vc-md100',
        'DDDB':
        'head-20100624',
        'Date':
        '2014-02-13 17:43:36.579037',
        'Application':
        'Boole',
        'QMTests': [
            'brunel.fixedfile-sim2010', 'brunel.fixedfile-sim2010-globaldb',
            'brunel.fixedfile-sim2010-localdb'
        ]
    },
    comment='Simulation of 2010 geometry with Boole v21r9',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-mdf/Boole-v19r8-30000000-100ev-20091112-vc-md100.mdf'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:56:16.931913',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-mdf'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, mdf format - no MC truth',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:59:26.133915',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, xdigi format',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended-spillover',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended-spillover/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended-spillover.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 18:01:51.972516',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended-spillover'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, with spillover, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='R08S14_smallfiles',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_5.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2010',
        'Processing': 'R08S14',
        'Simulation': False,
        'CondDB': 'head-20100531',
        'DDDB': 'head-20100531',
        'Stripping': 14,
        'Reconstruction': 8,
        'Date': '2014-02-14 11:08:29.713073',
        'Application': 'DaVinci',
        'QMTests': 'lhcbalgs.merge-small-files'
    },
    comment='Small files extracted from the grid for running FSR merging tests',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009/Brunel-v35r8-30000000-100ev-20091112-vc-md100.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:29:37.350134',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Brunel output in 2009 checking that DaVinci is still able to read it',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-wtruth',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile dst from 2009 with MCTruth',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth-wspill',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth-wspill/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth-spillover.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth, with spillover',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v37r3-fsrs-md',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v37r3-fsrs-md/Brunel-v37r3-20ev-FSR1.dst'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20110721',
        'Date': '2014-02-17 14:46:53.777561',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fsrs'
    },
    comment=
    'Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Down.',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v37r3-fsrs-mu',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v37r3-fsrs-mu/Brunel-v37r3-20ev-FSR2.dst'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20110721',
        'Date': '2014-02-17 14:46:53.777561',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fsrs'
    },
    comment=
    'Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Up.',
    test_file_db=test_file_db)
testfiles(
    myname='brunel-v37r1-sdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v37r1-sdst/Brunel-v37r1-069857_0000000006-1000ev.sdst'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2010',
        'Format': 'SDST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20110721',
        'Date': '2014-02-17 14:46:53.777561',
        'Application': 'DaVinci',
        'QMTests': 'davinci.io'
    },
    comment=
    'Test SDST from Brunel v37r1 in DaVinci, also links to a raw file, see the catalog in DaVinciTests/tests.options/TestSDSTCatalog.xml.',
    test_file_db=test_file_db)

testfiles(
    myname='MC2010_BdJPsiKs',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC2010_BdJPsiKs/MC2010_BdJPsiKs_00008414_00000106_1.dst'
    ],
    qualifiers={
        'Author': 'jonrob',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20100715-vc-md100',
        'DDDB': 'head-20100624',
        'Date': '2014-03-07 17:16:03.678324',
        'Application': 'Brunel'
    },
    comment='DaVinci MC2010 decaytreeTuple test',
    test_file_db=test_file_db)

testfiles(
    myname='Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst/Brunel_Beam6500GeV-md100-nu4.8.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'dhcroft',
        'DataType': 'XDST',
        'Processing': 'SIM08',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Stripping': None,
        'Reconstruction': 'Brunelv44r7',
        'Date': '2014-03-21 16:28:57.998963',
        'Application': 'Brunel',
        'QMTests': 'boole-sim08-xdst.qmt'
    },
    comment=
    '1000 MinBias XDST events used to test reprocessing in Boole of XDSTs',
    test_file_db=test_file_db)

testfiles(
    myname='2010-Raw-data-testVetraErrorHandling',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010-Raw-data-testVetraErrorHandling/2010_RAW_FULL_LHCb_COLLISION10_69984_069984_0000000001.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-03 18:36:32.282645',
        'QMTests': 'vetra-moni-error-banks-2010'
    },
    comment='Normal raw data from 2010, used to test VETRA error handling',
    test_file_db=test_file_db)

testfiles(
    myname='2009-Raw-data-Vetra_NZS_NoTestPulse',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2009-Raw-data-Vetra_NZS_NoTestPulse/2009_RAW_FULL_VELOA_PHYSICSNTP_45445_045445_0000000001.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_NZS_NoTestPulse'
    },
    comment='Non-zero suppressed VELO data from 2009, no test pulses',
    test_file_db=test_file_db)

testfiles(
    myname='2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw/2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_NZS_RoundRobin_100'
    },
    comment=
    'Non-zero suppressed VELO data from 2010: taken in round robin mode',
    test_file_db=test_file_db)

testfiles(
    myname='2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw/2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_NZS_RoundRobin_1'
    },
    comment=
    'Non-zero suppressed VELO testpulse data from 2010: taken in round robin mode',
    test_file_db=test_file_db)

testfiles(
    myname='2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw/2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_NZS_Stream_201105'
    },
    comment='Non-zero suppressed VELO data from 2011',
    test_file_db=test_file_db)

testfiles(
    myname='2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw/2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_ZS_2010'
    },
    comment='Non-zero suppressed VELO data from 2011',
    test_file_db=test_file_db)

testfiles(
    myname='2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw/2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw'
    ],
    qualifiers={
        'Author': 'dhcroft',
        'DataType': 'MDF',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'head-20110524',
        'DDDB': 'head-20110303',
        'Date': '2014-04-04',
        'QMTests': 'VetraMoni_ZS_201105'
    },
    comment='LHCb raw data from May 2011',
    test_file_db=test_file_db)

testfiles(
    myname='MC10-MinBias',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/MC10/ALLSTREAMS.DST/00008898/0000/00008898_00000002_1.allstreams.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'MC10',
        'Simulation': True,
        'CondDB': 'sim-20101210-vc-md100',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copymc10dsttoroot'
    },
    comment='MC10 minimum bias DST',
    test_file_db=test_file_db)

testfiles(
    myname='Reco08-charm.mdst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/CHARM.MDST/00008397/0000/00008397_00000939_1.charm.mdst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'Reco08',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Stripping': '12',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco08mdsttoroot'
    },
    comment='2010 Reco08 CHARM.MDST',
    test_file_db=test_file_db)

testfiles(
    myname='Reco08-bhadron.dst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/BHADRON.DST/00008399/0000/00008399_00001052_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'Reco08',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Stripping': '12',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco08dsttoroot'
    },
    comment='2010 Reco08 BHADRON.DST',
    test_file_db=test_file_db)

testfiles(
    myname='R12S17-bhadron.dst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/BHADRON.DST/00012545/0000/00012545_00000003_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'R21S17',
        'Simulation': False,
        'CondDB': 'head-20110914',
        'DDDB': 'head-20110914',
        'Reconstruction': '12',
        'Stripping': '17',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco12stripping17dsttoroot'
    },
    comment='2011 R12S17 BHADRON.DST',
    test_file_db=test_file_db)

testfiles(
    myname='R12S17-charm.mdst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/CHARM.MDST/00012547/0000/00012547_00000013_1.charm.mdst'
    ],
    qualifiers={
        'Format': 'MDST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'R21S17',
        'Simulation': False,
        'CondDB': 'head-20110914',
        'DDDB': 'head-20110914',
        'Reconstruction': '12',
        'Stripping': '17',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco12stripping17mdsttoroot'
    },
    comment='2011 R12S17 CHARM.MDST',
    test_file_db=test_file_db)

testfiles(
    "R14S20-bhadron.mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00020198/0000/00020198_00000758_1.bhadron.mdst"
    ], {
        "Author": "cattanem",
        "Format": "MDST",
        "DataType": "2012",
        "Date": "2014.04.11",
        "Processing": "R14S20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["ioexample.copyreco14stripping20mdsttoroot"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    }, "2012 R14S20 BHADRON.MDST", test_file_db)

testfiles(
    myname='upgrade-reprocessing-xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDST/00032467/0000/00032467_00000002_1.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'yamhis',
        'DataType': 'Upgrade',
        'Processing': 'Step-125740',
        'Simulation': True,
        'CondDB': 'sim-20130830-vc-md100',
        'DDDB': 'dddb-20131025',
        'Reconstruction': '14',
        'Date': '2014-06-18 17:56:23.229933',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdst.qmt'
    },
    comment='xdst for reprocessing of upgrade MC sample',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-25ns-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-25ns-sim/Baseline-25ns.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20131108-vc-md100',
        'DDDB': 'dddb-20131108',
        'Date': '2014-06-23 11:02:41.522819',
        'QMTests': 'boole-upgrade-baseline-25ns.qmt'
    },
    comment='Simulation of 2014 baseline for upgrade detectors, with spillover',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-25ns-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-25ns-xdigi/Baseline-25ns.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20131108-vc-md100',
        'DDDB': 'dddb-20131108',
        'Date': '2014-06-23 11:02:41.522819',
        'QMTests': 'brunel-upgrade-baseline-25ns.qmt'
    },
    comment=
    'Digitisation of 2014 baseline for upgrade detectors, with spillover',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-sim/Baseline.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20131108-vc-md100',
        'DDDB': 'dddb-20131108',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment='Simulation of 2014 baseline for upgrade detectors',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-xdigi/Baseline.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20131108-vc-md100',
        'DDDB': 'dddb-20131108',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-baseline.qmt'
    },
    comment='Digitisation of 2014 baseline for upgrade detectors',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-ut-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-ut-sim/Baseline-UT.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130830-vc-md100',
        'DDDB': 'dddb-20131025',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-baseline-ut.qmt'
    },
    comment='Simulation of 2014 baseline for upgrade detectors plus UT',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-ut-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-baseline-ut-xdigi/Baseline-UT.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130830-vc-md100',
        'DDDB': 'dddb-20131025',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-baseline-ut.qmt'
    },
    comment='Digitisation of 2014 baseline for upgrade detectors plus UT',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minimal-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-minimal-sim/Minimal.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-minimal.qmt'
    },
    comment='Simulation for studies of minimal upgrade detector',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minimal-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-minimal-xdigi/Minimal.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Application': 'Brunel',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-minimal.qmt'
    },
    comment='Digitisation for studies of minimal upgrade detector',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-calo_nospdprs-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-calo_nospdprs-sim/Calo_NoSpdPrs.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-calo_nospdprs.qmt'
    },
    comment=
    'Simulation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-calo_nospdprs-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-calo_nospdprs-xdigi/Calo_NoSpdPrs.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-calo_nospdprs.qmt'
    },
    comment=
    'Digitisation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-muon_nom1-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-muon_nom1-sim/Muon_NoM1.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-muon_nom1.qmt'
    },
    comment='Simulation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-muon_nom1-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-muon_nom1-xdigi/Muon_NoM1.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-muon_nom1.qmt'
    },
    comment='Digitisation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-reprocessing-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00027904/0000/00027904_00000001_2.xdigi'
    ],
    qualifiers={
        'Author': 'yamhis',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130806',
        'Date': '2014-07-02 10:43:43.592234',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdigi.qmt'
    },
    comment='files to test reprocessing of xdigi files in Boole',
    test_file_db=test_file_db)

testfiles(
    myname='Sim09-2016-xdigi-LHCBGAUSS-1008',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2016/XDIGI/00064502/0000/00064502_00000001_1.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2015',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Date': '2018-04-30 12:18:45',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdigi.qmt'
    },
    comment=
    'file to test reprocessing of xdigi file in Boole/digi14-patches, see LHCBGAUSS-1008',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_RecKstee',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/MCFILTER.DST/00033103/0000/00033103_00000006_1.mcfilter.dst'
    ],
    qualifiers={
        'Author': 'sneubert',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-08-05 17:14:15.919588',
        'Application': 'Brunel'
    },
    comment='Standard signal MC for 2015 tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_digi_nu2.6_B2Kstmumu',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034730/0000/00034730_00000003_1.xdigi'
    ],
    qualifiers={
        'Author': 'sneubert',
        'DataType': '2012',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-08-28 11:09:54.975311'
    },
    comment='MC2015 signal for tracking studies',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_digi_nu2.6_B2Kstmumu_L0',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/digi/MC2015_digi_nu2.6_B2Kstmumu_L0.dst'
    ],
    qualifiers={
        'Author': 'sneubert',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-08-28 15:36:15.365588'
    },
    comment='L0 processed digi file for 2015 tracking studies',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='upgrade-rotnm_foil_gbt-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319117/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319127/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319139/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319157/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319176/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319198/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319211/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319224/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319234/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319236/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319237/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319254/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319263/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319275/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319284/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319293/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319303/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319314/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319320/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319335/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319341/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319359/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319382/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319392/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319409/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319413/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319419/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319422/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319440/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319444/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319447/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319450/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319495/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319502/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319506/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319516/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319527/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319534/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319557/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319569/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319585/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319592/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319597/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319603/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319622/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319632/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319639/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319651/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319664/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319675/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319684/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319692/OctTest-Extended.digi'
    ],
    qualifiers={
        'Author': 'lben',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130806',
        'Date': '2014-08-21 10:04:11.651755',
        'Application': 'Brunel'
    },
    comment='Test files for the Velo Pixel created by Tim Head',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_digi_nu1.6_minbias',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000018_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000016_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000006_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00034696/0000/00034696_00000001_1.xdigi",
    ],
    qualifiers={
        'Author': 'sneubert',
        'DataType': '2012',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-09-13 14:49:04.554731'
    },
    comment='2015 minbias monte carlo. xdigi file for trigger studies',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2012_CaloFemtoDST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000001_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000002_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000003_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000004_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000005_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000006_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000007_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000008_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000009_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000010_1.fmdst'
    ],
    qualifiers={
        'Author': 'jonesc',
        'DataType': '2012',
        'Format': 'FMDST',
        'Simulation': False,
        'CondDB': 'cond-20140604',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-09-29 12:59:33.800587'
    },
    comment='CALO FemtoDST tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bd2Kpi_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_17.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_20.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_21.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_22.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_23.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_24.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_25.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_26.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_27.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_28.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_29.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_30.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagUp/MC2015_Bd2Kpi_MagUp_L0Processed_9.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_17.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_20.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_21.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_22.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_23.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_24.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_25.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_26.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_27.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_28.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_29.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/MagDown/MC2015_Bd2Kpi_MagDown_L0Processed_9.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-05 17:11:42.330871',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bd2Kpi, processed for Run II trigger studies, MagUp and MagDown, for the BnoC group',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_9.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_17.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_9.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-06 15:02:16.232429',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bs2PhiPhi, processed for Run II trigger studies, MagUp and MagDown, for the BnoC group',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_2015_D2KKPi_PrompyOnly_MagDown.dst',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.21263002_MagDown.dst'
    ],
    qualifiers={
        'Author': 'sreicher',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-10 10:21:12.705113',
        'Application': 'Moore_v23r1'
    },
    comment=
    'Prompt only D->KKPi candidates (event tupe 21263002), pT > 1 GeV, MagDown for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_2015_D2KKPi_PrompyOnly_MagUp.dst',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.21263002_MagUp.dst'
    ],
    qualifiers={
        'Author': 'sreicher',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-10 10:23:23.084335',
        'Application': 'Moore_v23r1'
    },
    comment=
    'Prompt only D->KKPi candidates (event tupe 21263002), pT > 1 GeV, MagUp for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_Dst2DPi2KsKK_PromptOnly_MagDown.dst',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.27265101_MagDown.dst'
    ],
    qualifiers={
        'Author': 'sreicher',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-10 10:25:05.229285',
        'Application': 'Moore_v23r1'
    },
    comment=
    'Prompt only D*->D(->KsKK)pi candidates (event type 27265101) , pT > 1 GeV, MagDown for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_Dst2DPi2KsKK_PromptOnly_MagUp.dst',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/PromptOnly.27265101_MagUp.dst'
    ],
    qualifiers={
        'Author': 'sreicher',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-10 10:26:27.155885',
        'Application': 'Moore_v23r1'
    },
    comment=
    'Prompt only D*->D(->KsKK)pi candidates (event type 27265101) , pT > 1 GeV, MagUp for Run 2 L0 bandwidth division (Charm WG).',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KpiGamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-10 23:41:36.785427'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KpiGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KpiGamma/MagDown/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-10 23:43:18.187332'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KstGamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-10 23:46:16.965375'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2KstGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2KstGamma/MagDown/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-10 23:47:24.451381'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Dst2D0Pi_D02mumu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 09:54:50.836637'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Dst2D0Pi_D02mumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Dst2D0Pi_D02mumu/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 09:55:45.142074'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2JpsiK_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 10:32:30.677116'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2JpsiK_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 10:33:04.978984'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagUp/L0processed_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:27:59.918947'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstee/MagDown/L0processed_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:28:48.020421'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2Lst1670Gamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:31:34.385704'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2Lst1670Gamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2Lst1670Gamma/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:32:13.313359'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagUp/L0processed_1.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:36:01.576431'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2mumu/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:37:22.269945'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagUp/L0processed_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:00.591309'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:56.208205'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_19.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_20.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_21.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_22.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_23.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_24.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_25.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_26.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_27.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_28.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_29.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_30.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_31.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_32.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagUp/L0processed_33.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:07:16.370383'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_19.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_20.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_21.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_22.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_23.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_24.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_25.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_26.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_27.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_28.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_29.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_30.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_31.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_32.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_33.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_34.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu/MagDown/L0processed_35.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:07:53.465299'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Ksmumu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagUp/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:10:35.480375'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Ksmumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:11:11.895297'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2K1Gamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagUp/L0processed_6.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2012',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:13:07.964964'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2K1Gamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2K1Gamma/MagDown/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:13:39.124451'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2KstGamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagUp/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:17:49.449453'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bu2KstGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bu2KstGamma/MagDown/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:18:24.882589'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiPhiGamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagUp/L0processed_6.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:20:39.519342'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiPhiGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiPhiGamma/MagDown/L0processed_6.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:21:09.569733'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_tau23mu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:24:16.798859'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_tau23mu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/tau23mu/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:25:20.157185'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Sigma2pmumu_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:26:36.125797'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Sigma2pmumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Sigma2pmumu/MagDown/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:27:07.548354'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bd2Kspipi_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagDown/MC2015_Bd2Kspipi_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kspipi/MagUp/MC2015_Bd2Kspipi_MagUp_L0Processed_5.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-14 16:39:05.628041',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bd2Kspipi, processed for RUn II trigger studies, MagUp and MagDown, for the BnoC WG',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_Mc2015_Bu2KKpi_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagDown/MC2015_Bu2KKpi_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KKpi/MagUp/MC2015_Bu2KKpi_MagUp_L0Processed_6.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2012',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-14 16:47:56.221845',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bu2KKpi, processed for Run II trigger studies, MagUp and MagDown, for BnoC WG',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bu2KsK_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagDown/MC2015_Bu2KsK_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bu2KsK/MagUp/MC2015_Bu2KsK_MagUp_L0Processed_6.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-14 16:55:22.711013',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bu2KsK, processed for Run II trigger studies, MagUp and MagDown, for the BnoC WG',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2KKpi0_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagDown/MC2015_Bs2KKpi0_MagDown_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2KKpi0/MagUp/MC2015_Bs2KKpi0_MagUp_L0Processed_6.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'SIm08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-14 16:58:40.428013',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bs2KKpi0, processed for Run II trigger studies, MagUp and MagDown, BnoC WG',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_11.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_12.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_13.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_14.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_15.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_16.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_17.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_18.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_19.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_20.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_21.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_22.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_23.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_24.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_25.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_26.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_27.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_28.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_29.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_30.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_31.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_32.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/L0Processed_lowq2_33.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:04:06.766959'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_11.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_12.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_13.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_14.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_15.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_16.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_17.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_18.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_19.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_20.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_21.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_22.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_23.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_24.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_25.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_26.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_27.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_28.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_29.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_30.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_31.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_32.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_33.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_34.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/L0Processed_lowq2_35.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:05:02.109845'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2OC_MC2015_DD_L0Processed_Strip21',
    filenames=[
        '/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Down/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DD_Up/6/output/L0BW.BhadronCompleteEvent.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jasmith',
        'DataType': 'LDST',
        'Processing': 'juggling_L0App_B2OCStripping21Lines',
        'Simulation': True,
        'CondDB': 'cond-20141007',
        'DDDB': 'dddb-20130929-1',
        'Stripping': 21,
        'Date': '2014-11-18 11:24:18.858900',
        'Application': 'Moore_v23r1,2'
    },
    comment='L0BWdivision',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2OC_MC2015_DsPi_L0Processed_Strip21',
    filenames=[
        '/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Down/7/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/0/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/1/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/2/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/3/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/4/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/5/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/6/output/L0BW.BhadronCompleteEvent.dst,/afs/cern.ch/user/j/jasmith/eos/lhcb/user/w/wbqian/NewL0BW/DsPi_Up/7/output/L0BW.BhadronCompleteEvent.dst'
    ],
    qualifiers={
        'Author': 'jasmith',
        'DataType': 'LDST',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'cond-20141007',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-11-18 11:30:58.072365',
        'Application': 'Moore_v23r1,2'
    },
    comment='L0BWdivision',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2L0Gamma_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagUp/L0processed_7.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-25 09:59:30.534281'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Lb2L0Gamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Lb2L0Gamma/MagDown/L0processed_7.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-25 10:00:35.357301'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kspipi_StripFiltered_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/StripFiltered/MC2015_Bd2Kspipi_StripFiltered_L0Processed_9.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-12-15 13:58:03.930293',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bd2Kspipi, filtered on stripping candidates, for Run II trigger studies, BnoC WG',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_Bs2JpsiPhi_L0Processed_Stripped_Selected',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_11.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_12.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_13.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_14.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_15.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_16.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_17.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_18.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_19.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_20.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_21.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_22.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_23.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/L0processed_24.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Stripping': 20,
        'Date': '2014-12-29 19:46:08.054146',
        'Application': 'Moore_v23r2/DaVinci_v36r2'
    },
    comment='bw samples for run2 trigger optimisation',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_L0Processed_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/SelDimuon_7.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-12-29 19:51:15.860076',
        'Application': 'Moore_v23r2/DaVinci_v36r2'
    },
    comment='bw sample for run2 trigger optimisation',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_L0Processed_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/SelDimuon_6.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-12-29 19:58:21.484033',
        'Application': 'Moorev23r2/DaVinci_v36r2'
    },
    comment='bw division sample for run 2 trigger optimisation',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kpi_StripFiltered_TCK_FF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_21.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF66/MC2015_Bd2Kpi_0xFF66_9.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-22 12:49:11.533034',
        'Application': 'Moore'
    },
    comment='MC2015 Bd2Kpi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bd2Kpi_StripFiltered_TCK_FF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bd2Kpi/TCK_FF67/MC2015_Bd2Kpi_0xFF67_9.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Revo15DEV',
        'Date': '2015-01-22 12:51:42.153368',
        'Application': 'Moore'
    },
    comment='MC2015 Bd2Kpi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2PhiPhi_StripFiltered_TCK_FF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_21.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_22.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_23.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_25.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_26.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_27.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_28.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_29.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_30.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_31.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_32.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF66/MC2015_Bs2PhiPhi_0xFF66_9.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Revo15DEV',
        'Date': '2015-01-22 12:53:54.596883',
        'Application': 'Moore'
    },
    comment='MC2015 Bs2PhiPhi sample processed with L0 TCK 0xFF66',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='BnoC_MC2015_Bs2PhiPhi_StripFiltered_TCK_FF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_21.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_22.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_23.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_24.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_25.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_26.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_27.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_28.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_29.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_30.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_31.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_32.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/TCK_FF67/MC2015_Bs2PhiPhi_0xFF67_9.ldst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-22 12:55:22.005800',
        'Application': 'Moore'
    },
    comment='MC2015 sample of Bs2PhiPhi processed with L0 TCK 0xFF67',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bu2Kpi0_MagUp',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_21.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_22.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_23.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_24.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_25.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_26.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_27.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_28.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_29.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_30.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_31.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_32.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_33.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_34.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_35.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_36.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_37.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_38.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagUp/BnoC_MC2015_Bu2Kpi0_MagUp_9.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'amerli',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2017-02-17 12:50',
        'Application': 'Moore'
    },
    comment='MC2015 sample of Bu2Kpi0 MagUp no L0 Processed',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bu2Kpi0_MagDown',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_17.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_18.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_19.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_20.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_21.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_22.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BnoC/Bu2Kpi0/MagDown/BnoC_MC2015_Bu2Kpi0_MagDown_9.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'amerli',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2017-02-17 12:50',
        'Application': 'Moore'
    },
    comment='MC2015 sample of Bu2Kpi0 MagDown no L0 Processed',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2016_B2pppp_MagDown',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_10.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_11.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_12.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_13.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_14.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_15.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_16.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_17.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_18.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_19.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_20.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_9.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'amerli',
        'DataType': '2016',
        'Processing': 'Sim09b',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Reconstruction': '',
        'Date': '2017-02-17 12:50',
        'Application': 'Moore'
    },
    comment='MC2016 sample of B2ppbarppbar MagDown digitalized no L0 processed',
    test_file_db=test_file_db)

testfiles(
    myname='2017HLTCommissioning_BnoC_MC2016_B2pppp_MagUp',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_10.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_11.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_12.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_13.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_14.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_15.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_16.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_17.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_18.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_19.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_20.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_21.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_22.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_23.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_24.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_25.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_26.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagUp/BnoC_MC2016_B2pppp_MagUp_9.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'amerli',
        'DataType': '2016',
        'Processing': 'Sim09b',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-mu100',
        'DDDB': 'dddb-20150724',
        'Reconstruction': '',
        'Date': '2017-02-17 12:50',
        'Application': 'Moore'
    },
    comment='MC2016 sample of B2ppbarppbar MagUp digitalized no L0 Processed',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='SLWG_MC2015_Bs2Dsmunu_L0Processed_TCK_FF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_9.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Bs2Dsmunu/RedoL0_0xFF67_17.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'powen',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-29 13:12:19.003247',
        'Application': 'Moore'
    },
    comment='MC2015 samples of Bs2Dsmunu for HLT tuning (TCK = 0xFF67)',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='SLWG_MC2015_Bs2Dsmunu_L0Processed_TCK_FF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_3.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_4.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_5.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_6.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_7.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_8.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_9.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_10.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_11.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_12.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_13.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_14.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_15.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_16.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Bs2Dsmunu/RedoL0_0xFF66_17.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'powen',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-29 13:22:13.114037',
        'Application': 'Moore'
    },
    comment='MC2015 samples of Bs2Dsmunu for HLT tuning (TCK = 0xFF66)',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='SLWG_MC2015_Lb2pmunu_L0Processed_TCK_FF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD300_0xFF66/Lb2pmunu/RedoL0_0xFF66_3.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'powen',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-29 13:26:48.116227',
        'Application': 'Moore'
    },
    comment='MC2015 samples of Lb2pmunu for HLT tuning (TCK = 0xFF66)',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='SLWG_MC2015_Lb2pmunu_L0Processed_TCK_FF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/SLWG/SPD600_0xFF67/Lb2pmunu/RedoL0_0xFF67_3.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'powen',
        'DataType': '2015',
        'Processing': 'Sim08f',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2015-01-29 13:29:22.650237',
        'Application': 'Moore'
    },
    comment='MC2015 samples of Lb2pmunu for HLT tuning (TCK = 0xFF67)',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed_TCK66_Stripped_Selected',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_11.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagUp/SelDimuon_12.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Stripping': 20,
        'Date': '2015-01-30 16:06:00.380193',
        'Application': 'Moore_v23r3/DaVinci_v36r2'
    },
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagUp_L0Processed_TCK67_Stripped_Selected',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_11.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagUp/SelDimuon_12.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Stripping': 20,
        'Date': '2015-01-30 16:11:14.974191',
        'Application': 'Moore_v23r3/DaVinci_v36r2'
    },
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed_TCK66_Stripped_Selected',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF66/MagDown/SelDimuon_11.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Stripping': 20,
        'Date': '2015-01-30 16:13:18.417172',
        'Application': 'Moore_v23r3/DaVinci_v36r2'
    },
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed_TCK67_Stripped_Selected',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_0.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_1.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_2.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_3.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_4.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_5.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_6.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_7.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_8.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_9.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_10.dst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiPhiMagUpDown/TCK_0xFF67/MagDown/SelDimuon_11.dst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Stripping': 20,
        'Date': '2015-01-30 16:16:29.454194',
        'Application': 'Moore_v23r3/DaVinci_v36r2'
    },
    comment='Hlt1 optmisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagUp_L0Processed_TCK66_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF66/RedoL0_0xFF66_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 20:46:44.385086',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagUp_L0Processed_TCK67_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagUp/TCK_0xFF67/RedoL0_0xFF67_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 20:52:05.416118',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagDown_L0Processed_TCK66_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF66/RedoL0_0xFF66_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 20:55:32.773393',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2JpsiKSKK_MagDown_L0Processed_TCK67_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2JpsiKSKKMagDown/TCK_0xFF67/RedoL0_0xFF67_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 20:56:29.387313',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimisation for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed_TCK66_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF66/RedoL0_0xFF66_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 21:03:02.494157',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagUp_L0Processed_TCK67_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagUp/TCK_0xFF67/RedoL0_0xFF67_6.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 21:04:57.433179',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed_TCK66_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_6.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF66/RedoL0_0xFF66_7.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 21:06:29.221969',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='B2CC_MC2015_Bs2Psi2SPhi_MagDown_L0Processed_TCK67_Stripped',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_0.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_1.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_2.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_3.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_4.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_5.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_6.ldst',
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/B2CC/Bs2Psi2SPhiMagDown/TCK_0xFF67/RedoL0_0xFF67_7.ldst'
    ],
    qualifiers={
        'Author': 'vsyropou',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-01-31 21:07:49.450345',
        'Application': 'Moore_v23r3'
    },
    comment='Hlt optimization for RunII',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagDown_0xFF66',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.21263002_MagDown.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 10:53:15.485129',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagUp_0xFF66',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.21263002_MagUp.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 11:06:34.347142',
        'Application': 'Moorev23r2'
    },
    comment='filtred for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagDown_0xFF66',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.27265101_MagDown.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 11:10:22.670068',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagUp_0xFF66',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF66/RedoL0_PromptOnly.27265101_MagUp.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 11:12:26.388181',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagDown_0xFF67',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.21263002_MagDown.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 11:57:11.521643',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_D2KKP_MagUp_0xFF67',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.21263002_MagUp.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 12:12:09.734123',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagDown_0xFF67',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.27265101_MagDown.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 12:14:49.386141',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Charm_ForBWDivision_Dst2D0Pi_D02KsKK_MagUp_0xFF67',
    filenames=[
        '/eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/Charm/TCK_0xFF67/RedoL0_PromptOnly.27265101_MagUp.ldst'
    ],
    qualifiers={
        'Author': 'msokolof',
        'DataType': 'LDST',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'reco15-dev',
        'Date': '2015-02-06 12:16:40.744190',
        'Application': 'Moorev23r2'
    },
    comment='filtered for prompt ccbar',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:00.591309'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:56.208205'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagUp/juggled_1.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:36:01.576431'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bs2mumu/MagDown/juggled_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:37:22.269945'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagUp/juggled_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:27:59.918947'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstee/MagDown/juggled_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:28:48.020421'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_33.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:04:06.766959'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed_0xFF66',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_9.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_17.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_20.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_21.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_22.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_23.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_24.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_25.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_26.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_27.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_28.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_29.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_30.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_31.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_32.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_33.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_34.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF66/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_35.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:05:02.109845'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagUp_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_9.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagUp/juggled_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:00.591309'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2PhiGamma/MagDown/juggled_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:56.208205'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagUp_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagUp/juggled_1.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:36:01.576431'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2mumu_MagDown_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bs2mumu/MagDown/juggled_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:37:22.269945'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagUp_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagUp/juggled_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:27:59.918947'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstee_MagDown_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstee/MagDown/juggled_14.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:28:48.020421'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagUp_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagUp/juggled_33.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:04:06.766959'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bd2Kstmumu_lowq2_MagDown_L0Processed_0xFF67',
    filenames=[
        'root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_0.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_1.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_2.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_3.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_4.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_5.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_6.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_7.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_8.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_9.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_10.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_11.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_12.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_13.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_14.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_15.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_16.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_17.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_18.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_19.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_20.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_21.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_22.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_23.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_24.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_25.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_26.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_27.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_28.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_29.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_30.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_31.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_32.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_33.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_34.dst,root://eoslhcb/cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed_0xFF67/filtered/RDWG/Bd2Kstmumu_lowq2/MagDown/juggled_35.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-17 17:05:02.109845'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_MinBias_SPD_lt_420_md_4xKee_L0Filtered',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_000.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_001.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_002.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_003.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_004.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_005.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_006.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_007.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_008.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_009.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_010.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_011.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_012.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_013.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_014.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_015.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_016.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_017.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_018.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_019.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_020.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_021.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_022.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_023.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_024.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_025.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_026.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_027.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_028.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_029.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagDown/MC2015_MinBias_0xFF63_030.ldst'
    ],
    qualifiers={
        'Author': 'raaij',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-03-26 23:04:28.631412'
    },
    comment='MC2015 MinBias samples for 2015 HLT commissioning rate tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_MinBias_SPD_lt_420_mu_4xKee_L0Filtered',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_000.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_001.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_002.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_003.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_004.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_005.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_006.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_007.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_008.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_009.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_010.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_011.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_012.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_013.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_014.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_015.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_016.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_017.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_018.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_019.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_020.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_021.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_022.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_023.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_024.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_025.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_026.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_027.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_028.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_029.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/ldst/SPD_lt_420/MagUp/MC2015_MinBias_0xFF63_030.ldst'
    ],
    qualifiers={
        'Author': 'raaij',
        'DataType': '2015',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2015-03-26 23:11:09.961390'
    },
    comment='MC2015 MinBias samples for 2015 HLT commissioning rate tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Tesla_Bsphiphi_MC12wTurbo',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/RemadeReports_HEAD_14-1-15_Bsphiphi_1k.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'sbenson',
        'DataType': '2012',
        'Processing': 'Sim08plusTurbo',
        'Simulation': True,
        'CondDB': 'sim-20130503-vc-md100',
        'DDDB': 'dddb-20130503',
        'Stripping': 20,
        'Reconstruction': 14,
        'Date': '2015-03-27 23:28:13.682936',
        'Application': 'DaVinci',
        'QMTests': 'tesla.containers,tesla.default'
    },
    comment='check resurrection of decay from raw bank',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2gammagamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BsGammaGamma_Stripped_Truth_0xFF66/MC15Dev_0_Truth.SeqBs2GG_none.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'sbenson',
        'DataType': '2015',
        'Processing': 65382,
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Stripping': 'Custom',
        'Reconstruction': 'Reco15Dev',
        'Date': '2015-05-24 12:41:17.316214',
        'Application': 'Moore',
        'QMTests': 'Moore_Neutral'
    },
    comment='LHCbIntegrationTests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2015HLTValidationData_L0filtered',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_0.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_1.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_2.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_3.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_4.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_5.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_7.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_8.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0xEE63/2015NB_L0filtered_0xEE63_9.mdf'
    ],
    qualifiers={
        'Author': 'ashires',
        'DataType': '2015',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20150617',
        'DDDB': 'dddb-20150526',
        'Stripping': None,
        'Reconstruction': None,
        'Date': '2015-07-22 11:43:23.139081',
        'Application': 'Moore',
        'QMTests': None
    },
    comment=
    '2015 no bias data that has passed the L0 for tuning the HLT (0xEE63)',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2015HLTValidationData_L0filtered_0x0050',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0x0050/2015NB_L0filtered_0x0050.mdf'
    ],
    qualifiers={
        'Author': 'raaij',
        'DataType': '2015',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20150617',
        'DDDB': 'dddb-20150526',
        'Date': '2015-07-22 18:06:43.143827',
        'Application': 'Moore'
    },
    comment='Validation of Moore for 25 ns running in August of 2015',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='TeslaTest_TCK_0x022600a2',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TeslaTest_TCK_0x022600a2.dst'
    ],
    qualifiers={
        'Author': 'sbenson',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'cond-20150828',
        'DDDB': 'dddb-20150724',
        'Date': '2016-03-29 18:05:33.296452',
        'Application': 'Moore'
    },
    comment='For Tesla output container checks',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2015raw_0xFF66',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TurboRaw_L0_0xFF66.mdf'
    ],
    qualifiers={
        'Author': 'sbenson',
        'DataType': '2015',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20150828',
        'DDDB': 'dddb-20150724',
        'Date': '2016-04-07 23:41:33.296452',
        'Application': 'Moore'
    },
    comment='For integration persist reco. tests',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2016raw_0x11361609_0x21361609',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2016raw_0x11361609_0x21361609/179348_0000000227.raw'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20160522',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2016-07-07 03:54:16 UTC',
        'Date': '2016-07-21',
        'Application': 'Moore'
    },
    comment='A turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    myname="Juggled_MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/Juggled_MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/juggled.dst'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-05-18',
        'Application': 'Moore'
    },
    comment=
    'Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db)

testfiles(
    myname="MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/00046271_00000135_2.dst'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-05-18',
        'Application': 'Moore'
    },
    comment=
    'Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x160D",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174819_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174822_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174823_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_174824_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175266_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175266_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175269_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175269_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175359_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175363_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175364_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175364_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175385_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175431_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175431_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175434_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175492_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175492_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175497_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175499_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175580_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175585_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175585_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160D/2016NB_0x160D_175589_01.mdf'
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-21',
        'Application': 'Moore'
    },
    comment='2016NB data that has been filtered through 0x160D with L0App',
    test_file_db=test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x1609_large",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174819_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174822_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174823_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174824_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175359_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175363_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175385_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175434_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175497_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175499_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175580_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175589_01.mdf'
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-21',
        'Application': 'Moore'
    },
    comment=
    'Larger sample of 2016NB data that has been filtered through 0x1609 with L0App',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_nobias",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_nobias/174909_0000000095_nobias.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_nobias',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_full",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_full/174375_0000000619_full.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-14 08:30:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_full',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_beamgas",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_beamgas/174909_0000000705_beamgas.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_beamgas',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_turcal",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_turcal/174892_0000000258_turcal.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-20 20:33:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_turcal',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_turbo",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_turbo/174909_0000000760_turbo.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_turbo',
    test_file_db=test_file_db)

testfiles(
    myname="HltServices-close_cdb_file",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291600.raw',
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291603.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160420',
        'InitialTime': '2016-05-17 05:26:16 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test files for test HltServices.close_cdb_file',
    test_file_db=test_file_db)

testfiles(
    myname="upgrade-Brunel-Baseline-DIGI",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/UFT5x_13104012_MagDown_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-10ev-Extended.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'tnikodem',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20150716-vc-md100',
        'DDDB': 'dddb-20160304',
        'Date': '2016-06-30',
        'Application': 'Brunel',
        'QMTests': 'Brunel'
    },
    comment='Test file for upgrade Brunel qmtests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_Pbp_MagUp_NoBias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/0/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/1/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/2/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/3/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/4/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/5/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/6/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/7/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/8/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/9/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/10/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/11/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/12/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/13/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/14/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/15/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/16/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/17/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/18/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MU/19/output/pPb_NoBias.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-07-05 14:00:52.674188'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_pPb_MagDown_NoBias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/0/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/1/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/2/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/3/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/4/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/5/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/6/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/7/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/8/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/9/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/10/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/11/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/12/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/13/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/14/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/15/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/16/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/17/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/18/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MD/19/output/pPb_NoBias.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-07-05 14:02:12.210359'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_pPb_MagUp_NoBias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/0/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/1/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/2/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/3/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/4/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/5/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/6/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/7/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/8/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/9/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/10/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/11/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/12/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/13/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/14/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/15/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/16/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/17/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/18/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/pPb_MU/19/output/pPb_NoBias.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-07-05 14:02:57.614386'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_Pbp_MagDown_NoBias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/0/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/1/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/2/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/3/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/4/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/5/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/6/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/7/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/8/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/9/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/10/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/11/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/12/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/13/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/14/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/15/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/16/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/17/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/18/output/pPb_NoBias.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/user/z/zhangy/pPb2013_NoBias/Pbp_MD/19/output/pPb_NoBias.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-07-05 14:25:53.199139'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

testfiles(
    myname='2016NB_25ns_L0Filt0x1715',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1715/RedoL0.mdf'
    ],
    qualifiers={
        'Author': 'sely',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20160517',
        'DDDB': 'dddb-20150724',
        "InitialTime": "2016-05-31 10:40:00 UTC",
        'Date': '2016-08-03',
        'Application': 'L0App'
    },
    comment=
    'Sample of 2016 No Bias data filtered with 0x1715 for Nightly test of Calibration microbias configuration',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_pPb_NoBias_L0Filter0x161B',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/pPb_L0TCK_0x161D_MagDown.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-08-18 10:33:38.493052'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_Pbp_NoBias_L0Filter0x161B',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagDown.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagUp.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-08-18 10:35:19.150018'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test2.digi'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-01'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test2.dst'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-01'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test2.sim'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'SIM',
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-25'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT61-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SciFi-v61/Gauss-13104012-100ev-20170301.sim'
    ],
    qualifiers={
        'Format': 'SIM',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2017-03-01',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment='sim for testing upgrade Boole',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT61-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SciFi-v61/Boole-13104012-100ev-20170301.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2017-03-01',
        'QMTests': 'brunel-upgrade-baseline.qmt'
    },
    comment='digi for testing upgrade Brunel',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT61-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SciFi-v61/Boole-13104012-100ev-20170301-Extended.digi'
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2017-03-01',
        'QMTests': 'boole-reprocess-xdigi.qmt'
    },
    comment='xdigi for testing upgrade Boole',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT61-xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SciFi-v61/Brunel-13104012-100ev-20170301.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2017-03-01',
        'QMTests': 'boole-reprocess-xdst.qmt'
    },
    comment='xdst for testing upgrade Boole',
    test_file_db=test_file_db)

testfiles(
    myname='11142401_geoSep2019_1.sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.upgrade/11142401_geoSep2019_1.sim'
    ],
    qualifiers={
        'Format': 'SIM',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20190912-vc-md100',
        'DDDB': 'dddb-20190912',
        'Date': '2019-09-12',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment='sim for testing upgrade Boole',
    test_file_db=test_file_db)

testfiles(
    myname='11142401_geoSep2019_1.mdf',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.upgrade/11142401-geoSep2019-10ev.mdf'
    ],
    qualifiers={
        'Format': 'MDF',
        'Author': 'Sebastien Ponce',
        'DataType': 'Upgrade',
        'Simulation': False,
        'CondDB': 'sim-20190912-vc-md100',
        'DDDB': 'dddb-20190912',
        'Date': '2019-09-12',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment='mdf file resulting of running Boole on 11142401_geoSep2019_1.sim',
    test_file_db=test_file_db)

testfiles(
    myname='11142401_geoSep2019_1.digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.upgrade/11142401-geoSep2019-10ev.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'Sebastien Ponce',
        'DataType': 'Upgrade',
        'Simulation': False,
        'CondDB': 'sim-20190912-vc-md100',
        'DDDB': 'dddb-20190912',
        'Date': '2019-09-12',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment='digi file resulting of running Boole on 11142401_geoSep2019_1.sim',
    test_file_db=test_file_db)

testfiles(
    myname='11142401_geoSep2019_1.xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.upgrade/11142401-geoSep2019-10ev.xdigi'
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'Sebastien Ponce',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20190912-vc-md100',
        'DDDB': 'dddb-20190912',
        'Date': '2019-09-12',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment=
    'xdigi file resulting of running Boole on 11142401_geoSep2019_1.sim',
    test_file_db=test_file_db)

testfiles(
    myname='11142401_geoSep2019_1.xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.upgrade/11142401-geoSep2019-10ev.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'Sebastien Ponce',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20190912-vc-md100',
        'DDDB': 'dddb-20190912',
        'Date': '2019-09-12',
        'QMTests': 'boole-upgrade-baseline.qmt'
    },
    comment=
    'xdst file resulting of running Brunel on 11142401_geoSep2019_1.xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='2015_Leptonic.MDST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/LEPTONIC.MDST/00048279/0004/00048279_00041826_1.leptonic.mdst'
    ],
    qualifiers={
        'Format': 'MDST',
        'Author': 'pseyfert',
        'DataType': '2015',
        'Processing': 'Stripping24',
        'Simulation': False,
        'CondDB': 'cond-20150828',
        'DDDB': 'dddb-20150724',
        'Stripping': '24',
        'Reconstruction': 'Reco15a',
        'Date': '2017-03-13 17:39:29.697135',
        'Application': 'DaVinci_v38r1p1',
        'QMTests': 'DaVinciTests.trackrefitting.read_2015_mdst'
    },
    comment=
    'MDST output of Stripping24 (test if DV can read data from Stripping24 MDST files)',
    test_file_db=test_file_db)

testfiles(
    myname='2016_turboraw_run178631',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5048_run178631_0000001751.raw'
    ],
    qualifiers={
        'Author': 'Olli Lupton',
        'Format': 'MDF',
        'DataType': '2016',
        'Simulation': False,
        'CondDB': 'cond-20160522',
        'DDDB': 'dddb-20150724',
        'Date': '2016-06-28'
    },
    comment=
    'Proton-Proton collisition data raw data TURBORAW stream, run 178631 from 28th June 2016. HLT1 TCK 0x11341609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/178631/178631_0000001751.raw',
    test_file_db=test_file_db)

testfiles(
    myname='2016_turboraw_run179597',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5085_run179597_0000001921.raw'
    ],
    qualifiers={
        'Author': 'Olli Lupton',
        'Format': 'MDF',
        'DataType': '2016',
        'Simulation': False,
        'CondDB': 'cond-20160522',
        'DDDB': 'dddb-20150724',
        'Date': '2016-07-11'
    },
    comment=
    'Proton-Proton collisition data raw data TURBORAW stream, run 179597 from 11th July 2016. HLT1 TCK 0x11361609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/179597/179597_0000001921.raw',
    test_file_db=test_file_db)

testfiles(
    myname='2016_turboraw_run180546',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016TurboRaw/fill5111_run180546_0000001837.raw'
    ],
    qualifiers={
        'Author': 'Olli Lupton',
        'Format': 'MDF',
        'DataType': '2016',
        'Simulation': False,
        'CondDB': 'cond-20160522',
        'DDDB': 'dddb-20150724',
        'Date': '2016-07-24'
    },
    comment=
    'Proton-Proton collisition data raw data TURBORAW stream, run 180546 from 24th July 2016. HLT1 TCK 0x11361609. HLT2 TCK 0x21361609. LFN /lhcb/data/2016/RAW/TURBORAW/LHCb/COLLISION16/180546/180546_0000001837.raw',
    test_file_db=test_file_db)
testfiles(
    myname='2016NB_L0Filt0x1701',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184247_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184249_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184251_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184253_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184254_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184255_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184256_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184257_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184258_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184261_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184263_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184264_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184266_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184267_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184268_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184269_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184270_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184271_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184272_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184273_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184274_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184275_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184276_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184285_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184289_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184290_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184291_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184292_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184293_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184294_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184296_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184299_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184300_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184301_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184302_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184303_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_184304_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185312_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185313_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185318_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185319_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185320_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185321_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185322_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185323_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185324_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185325_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185326_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185327_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185328_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185329_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185330_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185331_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185332_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185333_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185335_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2016NB_0x1701/2016NB_0x1701_185336_01.mdf'
    ],
    qualifiers={
        'Author': 'cofitzpa',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170325',
        'DDDB': 'dddb-20150724',
        'Date': '2017-04-10',
        'Application': 'L0App',
        "InitialTime": "2016-10-22 12:00:00 UTC",
    },
    comment=
    'Sample of 2016 No Bias data filtered with 0x1701 for rate tests and 2017 commissioning',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB_L0Filt0x1706',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_03.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_04.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_05.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_06.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_07.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_08.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_09.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_100.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_101.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_102.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_103.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_104.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_105.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_106.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_107.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_108.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_109.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_10.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_110.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_111.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_112.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_113.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_114.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_115.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_116.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_117.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_118.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_119.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_11.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_120.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_12.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_13.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_14.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_15.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_16.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_17.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_18.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_19.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_20.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_21.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_22.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_23.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_24.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_25.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_26.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_27.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_28.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_29.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_30.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_31.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_32.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_33.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_34.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_35.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_36.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_37.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_38.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_39.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_40.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_41.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_42.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_43.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_44.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_45.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_46.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_47.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_48.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_49.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_50.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_51.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_52.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_53.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_54.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_55.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_56.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_57.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_58.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_59.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_60.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_61.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_62.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_63.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_64.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_65.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_66.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_67.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_68.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_69.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_70.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_71.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_72.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_73.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_74.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_75.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_76.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_77.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_78.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_79.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_80.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_81.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_82.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_83.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_84.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_85.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_86.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_87.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_88.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_89.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_90.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_91.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_92.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_93.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_94.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_95.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_96.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_97.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_98.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_99.mdf'
    ],
    qualifiers={
        'Author': 'cofitzpa',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170510',
        'DDDB': 'dddb-20150724',
        'Date': '2017-06-20',
        'Application': 'L0App',
    },
    comment=
    'Sample of 2017 No Bias data filtered with 0x1706 for rate tests and 2017 commissioning',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB_L0Filt0x1707',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1707/2017NB_0x1707_194920_{0:02}.mdf'
        .format(i) for i in range(1, 29)
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-01-24',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170510',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-07-14 09:41:00 UTC',  # run 194920
    },
    comment=
    'Sample of 2017 No Bias data filtered with 0x1707 for rate tests and 2017 commissioning',
    test_file_db=test_file_db)

testfiles(
    myname='2017_Hlt1_0x11611709',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017HLT1/Run_0201852_20171106-{0}.hltf0111.mdf'
        .format(i) for i in [
            222031, 222252, 222426, 222513, 222644, 222905, 223039, 223259,
            223431, 223517, 223649, 223906, 224040, 224125, 224256, 224339,
            224511, 224729, 224901, 225119, 225251, 225337, 225507, 225725,
            225857, 225942, 230116, 230203, 230335, 230554, 230724, 230811,
            230942, 231114, 231158, 231331, 231549, 231807, 231938
        ]
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-02-27',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170724',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-11-06 21:20:00 UTC',  # run 201852
    },
    comment='Sample of 2017 HLT1-accepted data with 0x11611709',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB/{0}'
        .format(i) for i in [
            'Run_0197458_20170818-150239.hlte0112.mdf'
            'Run_0197458_20170818-150240.hlta0105.mdf'
            'Run_0197458_20170818-150240.hlte0109.mdf'
            'Run_0197458_20170818-150240.hlte0110.mdf'
            'Run_0197458_20170818-150241.hlte0105.mdf'
            'Run_0197458_20170818-150241.hlte0106.mdf'
            'Run_0197458_20170818-150241.hlte0107.mdf'
            'Run_0197458_20170818-150241.hlte0108.mdf'
            'Run_0197458_20170818-150242.hlte0111.mdf'
            'Run_0197458_20170818-150249.hlte0117.mdf'
            'Run_0197458_20170818-150249.hlte0118.mdf'
            'Run_0197458_20170818-150249.hltf0106.mdf'
            'Run_0197458_20170818-150249.hltf0108.mdf'
            'Run_0197458_20170818-150251.hltf0105.mdf'
            'Run_0197458_20170818-150251.hltf0109.mdf'
            'Run_0197458_20170818-150252.hlte0119.mdf'
            'Run_0197458_20170818-150252.hltf0107.mdf'
            'Run_0197458_20170818-150252.hltf0111.mdf'
            'Run_0197458_20170818-150252.hltf0112.mdf'
            'Run_0197458_20170818-150253.hltf0110.mdf'
            'Run_0197458_20170818-150254.hlte0116.mdf'
            'Run_0197458_20170818-150257.hlte0115.mdf'
        ]
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-03-21',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170724',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-08-18 13:03:00 UTC',  # run 197458
    },
    comment='Sample of 2017 NoBias events',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB_L0Filt0x18A1',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2018CommissioningDatasets/2017NB_0x18A1/2017NB_0x18A1_197458_{0:02}.mdf'
        .format(i) for i in range(1, 22)
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-04-05',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170724',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-08-18 13:03:00 UTC',  # run 197458
    },
    comment=
    'Sample of 2017 No Bias data filtered with 0x18A1 for rate tests and 2018 commissioning',
    test_file_db=test_file_db)

testfiles(
    myname='2018_Hlt1_0x11751801',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2018CommissioningDatasets/2018HLT1/2018_Hlt1_0x11751801_209933_{0:02}.mdf'
        .format(i) for i in range(68)
    ],
    qualifiers={
        'Author': 'mramospe',
        'Date': '2018-06-07',
        'DataType': '2018',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20180202',
        'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-06 15:10:00 UTC',  # run 209933
    },
    comment='Sample of 2018 HLT1-accepted data with 0x11751801',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='upgrade_integration_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/LHCbIntegrationTest/Gauss-30000000-10ev-20171212.sim'
    ],
    qualifiers={
        'Author': 'sstahl',
        'DataType': 'SIM',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-01-11 13:46:49.273953'
    },
    comment=
    'A sample of 10 events, privately produced, to test the integration of applications from Boole to DaVinci in Upgrade conditions',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-throughput-minbias-nogec-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big.mdf'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-06'
    },
    comment=
    'A sample of upgrade events in MDF format with no GEC applied for throughput tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-throughput-minbias-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big_GEC.mdf'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-06'
    },
    comment=
    'A sample of upgrade events in MDF format with a GEC at 11000 FT hits applied for throughput tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-prchecker-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/BBARTESTSAMPLE_1.digi'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-02'
    },
    comment=
    'A sample of upgrade events in DIGI format applied for PrChecker tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-minbias-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbiasDigi/00070647_{0:08}_1.digi'
        .format(i) for i in range(1, 31)
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-11-06'
    },
    comment=
    'Upgrade minbias MC in DIGI format, used to test reconstruction algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-minbias-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbiasXDigi/00067189_{0:08}_1.xdigi'
        .format(i) for i in range(1, 31)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20171010',
        'Date': '2019-04-01'
    },
    comment=
    'Upgrade minbias MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-reco-up01-minbias-ldst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbias/00069155_00000021_2.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'olupton',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Reconstruction': 'Reco-Up01',
        'Date': '2018-04-25'
    },
    comment=
    'Upgrade minbias MC in LDST format, used for TCK creation for upgrade MC filtering for selection studies',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-34112100-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalDigi/K0S2mu2/K0S2mu2_MCUpgrade_34112100_MagDown_{0:02}.digi'
        .format(i) for i in range(0, 37)
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2018-11-28'
    },
    comment=
    'Upgrade KS0 -> mu+ mu- MC in DIGI format, used to test reconstruction algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up01-34112106-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalXDigi/34112106/K0S2mu2_MCUpgrade_34112106_MagUp_{0:02}.xdigi'
        .format(i) for i in range(0, 45)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim10-Up01',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-mu100',
        'DDDB': 'dddb-20190223',
        'Date': '2019-04-03'
    },
    comment=
    'Upgrade KS0 -> mu+ mu- MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up01-24142001-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalXDigi/24142001/Jpsi2mu2_MCUpgrade_24142001_MagDown_{0:02}.xdigi'
        .format(i) for i in range(0, 91)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim10-Up01',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20190223',
        'Date': '2019-08-01'
    },
    comment=
    'Upgrade inclusive Jpsi -> mu+ mu- MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    "2015_DaVinciTests.davinci.gaudipython_algs", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008700_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008701_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2015",
        "Date": "2018.05.30",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.davinci.gaudipython_algs"],
    }, "Proton-Proton collision data, Reco15a, run 164668", test_file_db)

testfiles(
    "2016_DaVinciTests.stripping28X_collision16_reco16", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00053196/0010/00053196_00100516_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00053196/0009/00053196_00099798_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2016",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [""],
    }, "Proton-Proton collision data from 2016", test_file_db)

testfiles(
    "2017_DaVinciTests.stripping29r2_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065694_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065751_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2017",
        "Date": "2018.06.04",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping29r2_collision17_reco17"],
    }, "Proton-Proton collision data from 2017", test_file_db)

testfiles(
    "2012_DaVinciTests.stripping.test_express_appconfig", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00047243_1.full.dst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping.test_express_appconfig"],
    }, "Proton-Proton collision data from 2012", test_file_db)

testfiles(
    "2015_DaVinciTests.trackrefitting.read_2015_mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/LEPTONIC.MDST/00048279/0000/00048279_00001457_1.leptonic.mdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "MDST",
        "DataType": "2015",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.trackrefitting.read_2015_mdst"],
    }, "Stripping output from 2015 pp collision data", test_file_db)

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURBO',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000252.raw'
    ],
    qualifiers={
        'Author': 'rmatev',
        'DataType': '2018',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20180202',
        'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-12 04:47:00 UTC',
        'Date': '2018-06-18',
        'Application': 'Moore'
    },
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURCAL',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000247.raw'
    ],
    qualifiers={
        'Author': 'rmatev',
        'DataType': '2018',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20180202',
        'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-12 04:47:00 UTC',
        'Date': '2018-06-18',
        'Application': 'Moore'
    },
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    "MiniBrunel_2018_MinBias_FTv2_MDF", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MiniBrunel/MC_Upg_Down_201710_43k.mdf"
    ] * 20, {
        "Author": "Sebastien Ponce",
        "Format": "MDF",
        "DataType": "Upgrade",
        "Date": "2018.08.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20170301-vc-md100",
        "DDDB": "dddb-20171010",
    },
    "Upgrade minimum biais data from simulation. 43k events using FT v2 rawbank format",
    test_file_db)

testfiles(
    "MiniBrunel_2018_MinBias_FTv4_DIGI", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_1.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_2.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_3.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_4.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_5.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_6.digi.digi"
    ], {
        "Author": "Sebastien Ponce",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2018.08.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade minimum biais data from simulation. 100k events using FT v4/5 rawbank format",
    test_file_db)

testfiles(
    "MiniBrunel_2018_MinBias_FTv4_MDF", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MiniBrunel/00067189.mdf"
    ] * 10, {
        "Author": "Sebastien Ponce",
        "Format": "MDF",
        "DataType": "Upgrade",
        "Date": "2018.08.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade minimum biais data from simulation. 100k events using FT v4/5 rawbank format",
    test_file_db)

testfiles(
    "MiniBrunel_2019_MinBias_FTv6_MDF", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Beam7000GeV/Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up02/uncompressed_large.mdf'
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "Upgrade",
        "Date": "2019.13.06",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20190223",
    },
    "Upgrade minimum biais data from simulation. Using FT v6 rawbank format. Magnet up",
    test_file_db)

testfiles(
    "MiniBrunel_2019_MinBias_FTv6_MDF_small", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Beam7000GeV/Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up02/uncompressed_small.mdf'
    ], {
        "Author": "Mika Vesterinen",
        "Format": "MDF",
        "DataType": "Upgrade",
        "Date": "2019.13.06",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20190223",
    },
    "Small file with a subset of events from MiniBrunel_2019_MinBias_FTv6_MDF",
    test_file_db)

testfiles(
    "2017_raw_full_lowmu",
    [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2017/RAW/FULL/LHCb/CALIBRATION17/202694/202694_0000000810.raw"
    ],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2017",
        "Date": "2018.08.31",
        "Application": "",  # passthrough data
        "Simulation": False,
        "QMTests": ["brunel.2017-passthrough"],
    },
    "Low-mu passthrough Proton-Proton collision raw data FULL stream, run 202694 from 21th November 2017.",
    test_file_db)

testfiles(
    myname='upgrade-baseline-FT64-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/minbias-64-1068-MagDown-Extended.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'Simon Stemmle',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20180815',
        'Date': '2018-10-26',
        'QMTests': 'brunel-upgrade-paramKalman.qmt'
    },
    comment='Mini bias digi for testing upgrade Brunel with param Kalman',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='upgrade-big-events',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00067189/0000/00067189_00000098_1.xdigi'
    ],
    qualifiers={
        'Author': 'sstahl',
        'DataType': 'XDIGI',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20171010',
        'Date': '2018-09-08 10:05:37.051919',
        'QMTests': 'Brunel.brunel-upgrade-bigevents'
    },
    comment=
    'This file is intended to gather big events to test the upgrade reconstruction',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_BdKstgamma_busy_event_XDIGI", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000026_1.xdigi"
    ], {
        "Author": "Andre Guenther",
        "Format": "XDIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.14",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20190223",
        "QMTests": 'brunel-upgrade-bigevents2.qmt'
    },
    "This file contains events with particularly many hits in the SciFi stations, same idea as 'upgrade-big-events'.",
    test_file_db)

testfiles(
    myname='2016-lb2l0gamma.strip.dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2016/LB2L0GAMMA.STRIP.DST/00057531/0000/00057531_00000001_1.lb2l0gamma.strip.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2016',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Date': '2018-09-13',
        'QMTests': 'L0Calo.testL0CaloFix2016'
    },
    comment='MC/2016 lb2l0gamma stripped DST',
    test_file_db=test_file_db)

testfiles(
    "upgrade_2018_BsPhiPhi_FTv2", [
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00067195/0000/00067195_00000032_1.xdigi"
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "XDIGI",
        "DataType": "Upgrade",
        "Date": "2018.10.25",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20170301-vc-md100",
        "DDDB": "dddb-20171010",
    }, "Upgrade BsPhiPhi simulation, FT v2 rawbank format, 1k events",
    test_file_db)

testfiles(
    "upgrade_2018_BdKstee_LDST", [
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000019_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000045_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000066_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000075_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000078_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000084_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000090_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000093_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000101_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000106_2.ldst"
    ], {
        "Author": "Carla Marin Benito",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2019.04.02",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "DDDB": "dddb-20171126",
    }, "Upgrade BdKstee simulation, LDST format, 10k events", test_file_db)

testfiles(
    myname='UpgradeHLT1FilteredMinbias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP2/Hlt2Throughput/minbias_filtered_{0}.mdf'
        .format(i) for i in [
            1, 12, 13, 14, 17, 2, 20, 21, 22, 23, 24, 25, 27, 29, 3, 32, 36,
            37, 39, 4, 40, 42, 43, 46, 47, 49, 52, 53, 54, 55, 58, 60, 61, 62,
            66, 68, 70, 73, 74, 76, 78, 79, 80, 82, 83, 86, 87, 9, 91, 94, 96,
            97, 98
        ]
    ],
    qualifiers={
        'Author': 'Marian Stahl',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171010',
        'CondDB': 'sim-20170301-vc-md100',
        'Date': '2019-05-28'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/3000000 . There was an additional filtering on (|Two)TrackTight | DiMuon(Low|HighMass)Tight.',
    test_file_db=test_file_db)

testfiles(
    myname='UpgradeHLT1FilteredWithGEC',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP2/Hlt2Throughput/minbias_filtered_gec11000_{0}.mdf'
        .format(i) for i in [1, 2, 3, 4, 5]
    ],
    qualifiers={
        'Author': 'Sascha Stahl',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171010',
        'CondDB': 'sim-20170301-vc-md100',
        'Date': '2019-05-28'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/3000000 . There was an additional filtering on (|Two)TrackTight | DiMuon(Low|HighMass)Tight. A GEC of 11000 on the sum of FT and UT clusters is applied.',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_MinBias_LDST", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001005_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000988_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000940_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001010_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000789_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001379_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001442_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001435_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000531_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000538_2.ldst"
    ], {
        "Author": "Alex Pearce",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2019.09.20",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "DDDB": "dddb-20171126",
    }, "Upgrade MinBias simulation, LDST format, 10k events", test_file_db)

testfiles(
    myname='KS0MuMu_Upgrade_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_0.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_100.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_101.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_102.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_103.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_104.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_105.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_106.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_107.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_108.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_109.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_10.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_110.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_111.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_112.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_113.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_114.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_115.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_116.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_117.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_118.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_119.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_11.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_120.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_121.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_122.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_123.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_124.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_125.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_126.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_127.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_128.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_129.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_12.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_130.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_131.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_132.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_133.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_134.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_135.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_136.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_137.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_138.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_139.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_13.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_140.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_141.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_142.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_144.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_145.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_146.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_147.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_148.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_149.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_14.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_151.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_152.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_154.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_155.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_156.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_158.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_159.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_160.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_161.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_162.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_163.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_164.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_165.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_166.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_167.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_169.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_16.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_171.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_172.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_173.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_174.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_175.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_176.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_177.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_178.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_179.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_17.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_180.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_181.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_182.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_183.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_184.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_185.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_187.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_188.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_189.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_18.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_190.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_191.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_192.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_193.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_194.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_195.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_196.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_197.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_198.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_19.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_20.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_21.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_24.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_25.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_26.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_27.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_28.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_29.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_2.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_30.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_31.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_32.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_33.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_34.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_36.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_38.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_39.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_3.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_40.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_41.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_42.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_43.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_44.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_45.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_46.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_47.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_48.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_49.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_4.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_50.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_51.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_52.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_53.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_54.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_55.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_56.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_57.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_59.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_5.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_60.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_61.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_62.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_63.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_64.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_65.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_66.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_67.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_68.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_69.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_6.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_70.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_72.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_73.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_74.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_75.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_78.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_79.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_7.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_80.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_82.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_83.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_85.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_86.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_87.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_88.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_8.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_90.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_91.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_92.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_93.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_94.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_95.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_96.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_97.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_98.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_99.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Ks2MuMu/Boole_34112106_md_Upgrade_9.xdigi'
    ],
    qualifiers={
        'Author': 'Adrian Casais',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20180815',
        'Date': '2020-02-21'
    },
    comment='Privately produced KS0-> mu mu xdigi files.',
    test_file_db=test_file_db)
### auto created ###
testfiles(
    myname='genFSR_upgrade_spillover_digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr-upgrade-spillover.digi'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2019-08-28 13:41:32.321467',
        'QMTests': 'test-genfsr-spillover.qmt'
    },
    comment=
    'digi for testing the genFSR merging in samples with spillover in Brunel',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_BsPhiPhi_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.01.23",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade BsPhiPhi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_BsPhiPhi_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.01.23",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade BsPhiPhi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_Ds2KKPi_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p0-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p1-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p2-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p3-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p4-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p5-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p6-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p7-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p8-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p9-Extended.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade Ds2KKPi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_Ds2KKPi_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p0-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p1-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p2-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p3-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p4-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p5-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p6-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p7-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p8-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p9-Extended.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade Ds2KKPi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_JPsiMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-10.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-11.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-12.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-13.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-14.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-15.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-16.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-17.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-18.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-19.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-20.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-21.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-22.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-23.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-24.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-25.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-26.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-27.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-28.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-29.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-30.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-31.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-32.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-33.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-35.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-36.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-37.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-38.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-39.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-40.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-41.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-42.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-43.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-44.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-45.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-46.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-47.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-48.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-49.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-50.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-51.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-52.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-53.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-54.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-55.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-56.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-57.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-58.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-59.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-60.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-61.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-62.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-63.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-64.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-65.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-66.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-67.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-68.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-69.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-70.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-71.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-72.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-73.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-74.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-75.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-76.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-77.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-78.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-79.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-80.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-81.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-82.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-83.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-84.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-85.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-86.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-87.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-88.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-89.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-90.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-91.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-92.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-93.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-94.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-95.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-96.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-97.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-98.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-99.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade JPsiMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstEE_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstEE data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstEE_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstEE data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstMuMu_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_ZMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade ZMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_ZMuMu_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade ZMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

### auto created ###
testfiles(
    myname='Upgrade_Bd_pi+pi-pi0_LDST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000153_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000078_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000048_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000026_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000171_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000084_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000019_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000012_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000219_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000102_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000077_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000096_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000133_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000197_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000072_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000005_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000151_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000158_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000027_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000148_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000212_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000069_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000145_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000042_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000017_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000099_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000170_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000098_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000215_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000192_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000136_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000041_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000103_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000144_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000002_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000167_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000021_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000120_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000176_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000180_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000007_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000061_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000022_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000201_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000082_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000143_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000075_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000168_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000051_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000193_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000175_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000113_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000163_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000214_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000196_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000149_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000008_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000125_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000052_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000074_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000128_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000183_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000152_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000169_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000010_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000165_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000203_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000119_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000020_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000177_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000009_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000080_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000160_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000079_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000091_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000058_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000184_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000071_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000166_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000094_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000023_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000053_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000181_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000086_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000206_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000116_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000015_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000129_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000001_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000130_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000115_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000062_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000059_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000146_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000055_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000198_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000135_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000047_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000117_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000093_2.ldst',
    ],
    qualifiers={
        'Author': 'lzenaiev',
        'DataType': 'Upgrade',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2020-03-04 15:56:08.815188',
        'QMTests': 'RecoConf.hlt2_reco_calo_resolution_pi0'
    },
    comment=
    'MC Upgrade Bd_pi+pi-pi0_LDST sample (about 100K events), event type 11102404, needed for calo resolution studies in Moore tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Upgrade_BdKstgamma_XDIGI',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000012_1.xdigi',
    ],
    qualifiers={
        'Author': 'lzenaiev',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20190223',
        'Date': '2020-03-04 16:57:09.341749',
        'QMTests': 'RecoConf.hlt2_reco_calo_resolution_gamma'
    },
    comment=
    'MC Upgrade BdKstgamma_XDIGI sample (about 40K events), event type 11102201, needed for calo resolution studies in Moore tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2011',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2011.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2011',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20160614-1-vc-md100',
        'DDDB': 'dddb-20170721-1',
        'Date': '2020-04-17 19:24:42.879832',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2012',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2012.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2012',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-md100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-17 19:26:35.055644',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2015',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2015.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2015',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20161124-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:27:47.632598',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2016',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2016.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2016',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20170721-2-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:29:35.877782',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2017',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2017.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2017',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20190430-1-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:47:11.052450',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2018',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2018.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2018',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20190430-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:48:15.875389',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2011',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2011.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2011',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20160614-1-vc-md100',
        'DDDB': 'dddb-20170721-1',
        'Date': '2020-04-17 19:54:16.091713',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2012',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2012.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-md100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-17 19:55:52.573428',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2015',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2015.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2015',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20161124-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:57:05.767479',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2016',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2016.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2016',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20170721-2-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:02:04.821129',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2017',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2017.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2017',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20190430-1-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:03:08.176329',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2018',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2018.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2018',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20190430-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:04:06.217418',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='genFSR_2012_mergesmallfiles_digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test5.digi',
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-mu100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-20 20:20:15.45723',
        'QMTests': 'mergesmallfiles-genFSR.qmt'
    },
    comment=
    'Files .digi for testing the virtual memory and the memory usage during the genFSR merging',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_Bs2phiphi_MU_FTv4_SIM", [
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/0/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/1/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/2/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/3/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/4/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/5/Gauss-13104012-100ev-20190506.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/6/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/7/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/8/Gauss-13104012-100ev-20190507.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/9/Gauss-13104012-100ev-20190505.sim'
    ], {
        "Author": "Vladimir Gligorov",
        "Format": "SIM",
        "DataType": "Upgrade",
        "Date": "2019.05.05",
        "Application": "Boole",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
    },
    "Upgrade Bs2PhiPhi data from simulation. #10k events with FT version 6.4 , private production",
    test_file_db)

testfiles(
    myname="upgrade_minbias_hlt1_filtered",
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00004602_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00004625_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005065_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005143_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005582_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005592_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006124_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006137_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006187_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006191_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006211_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006220_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006746_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007043_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007187_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007259_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007332_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007348_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007357_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007389_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007409_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007441_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007457_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007519_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007578_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007641_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007658_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007932_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007952_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00008351_1.ldst",
    ],
    qualifiers={
        "Author": "Alex Pearce",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2020-04-23 12:12:12.12000",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "DDDB": "dddb-20171126",
    },
    comment=
    "Upgrade minimum bias sample filtered by Moore v30r0 HLT1; DIRAC transformation ID 75219.",
    test_file_db=test_file_db)

testfiles(
    myname='CHARMSPECPARKED_for_memory_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Collision16/CHARMSPECPARKED.MDST/00076439/0000/00076439_00000685_1.charmspecparked.mdst'
    ],
    qualifiers={
        'Author': 'Marian Stahl',
        'Format': 'ROOT',
        'DataType': '2016',
        'Simulation': False,
        'Turbo': True,
        'RootInTES': '/Event/Charmspec/Turbo',
        'Date': '2020-04-29'
    },
    comment=
    'Randomly chosen CHARMSPECPARKED mDST which causes large memory consumption in an empty event loop from DaVinci v45r1 (Gaudi v32r1) onwards. Issue described in https://gitlab.cern.ch/lhcb-dpa/project/issues/2',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_Bd2KstarMuMu',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076720/0000/00076720_00000002_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076720/0000/00076720_00000004_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076720/0000/00076720_00000043_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076720/0000/00076720_00000068_1.ldst',
    ],
    qualifiers={
        'Author': 'Patrick Koppenburg',
        'Format': 'LDST',
        'DataType': 'Upgrade',
        'Date': '2020-05-28 11:12:55',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
    },
    comment='K*mumu by Moore Hlt1-filtered with TCK-0x52000000',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202004-PbPb-EPOS-b-6_14fm',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/'
        'upgrade-202004-PbPb-EPOS-b-6_14fm/{0}_Gausstest_MB.xdigi'.format(i)
        for i in [
            361080073, 361080076, 361080079, 361080084, 361080088, 361080109,
            361080120, 361080132, 361080148, 361080176, 361080192, 361080204,
            361080218, 361080231, 361080239, 361080251, 361080262, 361080274,
            361080288, 361080289, 361080290, 361080291, 361080292, 361080293,
            361080294, 361080297, 361080298, 361080300, 361080301, 361080302,
            361080303, 361080304, 361080305, 361080306, 361080307, 361080308,
            361080310, 361080311, 361080312, 361080313, 361080314, 361080316,
            361080317, 361080318, 361080319, 361080320, 361080321, 361080322,
            361080323, 361080324, 361080325, 361080326, 361080327, 361080328,
            361080329, 361080330, 361080331, 361080332, 361080334, 361080335,
            361080336, 361080337, 361080338, 361080339, 361080340, 361080341,
            361080343, 361080344, 361080345, 361080346, 361080347, 361080348,
            361080349, 361080350, 361080351, 361080352, 361080353, 361080354,
            361080355, 361080357, 361080358, 361080359, 361080360, 361080361,
            361080362, 361080363, 361080364, 361080365, 361080366, 361080368,
            361080369, 361080370, 361080371, 361080372
        ]
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20190223',
        'Date': '2020-04-20 00:00:00'
    },
    comment=(
        'Private sample using EPOS: merged J/psi-> mu mu (5k) and B->J/psi '
        '(5k) embedded in MB EPOS events with the impact parameter b ranging '
        'between 6-14 fm (15%-80% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202004-PbPb-EPOS-b-6_14fm-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/'
        'upgrade-202004-PbPb-EPOS-b-6_14fm/all_Gausstest_MB.mdf',
    ],
    qualifiers={
        'Author': 'Carla Marin',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        'DDDB': 'dddb-20190223',
        'Date': '2021-03-03 08:30:00'
    },
    comment=
    ('Private MDF sample created from upgrade-202004-PbPb-EPOS-b-6_14fm for throughput tests.'
     'Around 10k events available.'),
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='genFSR_upgrade_ldst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade3.ldst',
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': 'Upgrade',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        'DDDB': 'dddb-20171126',
        'Date': '2020-06-17 18:34:43.57756',
        'QMTests': 'test-genfsr.qmt'
    },
    comment=
    'Files .ldst with Upgrade condition for testing the propagation and merging of genFSR in DaVinci',
    test_file_db=test_file_db)

testfiles(
    "Sim09j-2015-Digi14c-incl_b",
    filenames=[
        ("root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2015/DIGI/"
         "00115195/0000/00115195_0000000{0}_1.digi").format(i)
        for i in range(1, 10)
    ],
    qualifiers={
        "Author": "Rosen Matev",
        "Format": "DIGI",
        "DataType": "2015",
        "Date": "2020.09.25",
        "Application": "Boole",
        "Simulation": True,
        "DDDB": "dddb-20170721-3",
        "CondDB": "sim-20161124-vc-md100",
    },
    comment=("Inclusive B sample for testing 2015 legacy applications. From "
             "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8"
             "/Sim09j/Digi14c/10000000/DIGI"),
    test_file_db=test_file_db)

### auto created ###
testfiles(
    'upgrade-TELL40-UT-miniBias',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-ut/Boole-Extended.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'xuyuan',
        'DataType': 'Upgrade',
        'Processing': 'Boole',
        'Simulation': True,
        'CondDB': 'upgrade/UTv4r2',
        'DDDB': 'upgrade/dddb-20201105',
        'Date': '2020-12-04 08:01:44.324813',
        'Application': 'Boole',
        'QMTests': 'brunel-upgrade-UTTELL40-decoder.qmt'
    },
    comment='miniBias sample with new UTTELL40 data format',
    test_file_db=test_file_db)

#Up08 samples
testfiles(
    myname='upgrade-magdown-sim10-up08-11102202-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2KstGamma, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (12/12 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstgamma/MagDown/00122684_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstgamma/MagDown/00122684_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000001_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000008_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000009_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000010_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000011_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000003_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122684/0000/00122684_00000012_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-11102202-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2KstGamma, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstgamma/MagUp/00122704_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstgamma/MagUp/00122704_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122704/0000/00122704_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000002_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000004_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000012_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000001_1.xdigi',
        'root://lhcb-sdpd3.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122704/0000/00122704_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-11124001-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2Kstee, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (13/13 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstee/MagDown/00122682_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstee/MagDown/00122682_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000001_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122682/0000/00122682_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000006_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000009_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000002_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000010_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000005_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000011_1.xdigi',
        'root://lhcb-sdpd12.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000003_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000012_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122682/0000/00122682_00000013_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-11124001-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2Kstee, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (15/15 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstee/MagUp/00122702_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bd2Kstee/MagUp/00122702_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000009_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122702/0000/00122702_00000010_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000011_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000012_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000006_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000013_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000003_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000014_1.xdigi',
        'root://lhcb-sdpd13.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000004_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000015_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122702/0000/00122702_00000008_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-11874004-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2Dstmunu, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/B2Dstmunu/MagDown/00122698_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/B2Dstmunu/MagDown/00122698_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122698/0000/00122698_00000002_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122698/0000/00122698_00000009_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000005_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000012_1.xdigi',
        'root://lhcb-sdpd15.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000004_1.xdigi',
        'root://lhcb-sdpd17.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122698/0000/00122698_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-11874004-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bd2Dstmunu, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/B2Dstmunu/MagUp/00122718_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/B2Dstmunu/MagUp/00122718_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122718/0000/00122718_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000012_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000004_1.xdigi',
        'root://lhcb-sespd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000002_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122718/0000/00122718_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-13104012-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bs2phiphi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2phiphi/MagDown/00122688_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2phiphi/MagDown/00122688_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122688/0000/00122688_00000009_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000004_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000010_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000011_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000012_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000013_1.xdigi',
        'root://lhcb-sdpd3.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000005_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000003_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122688/0000/00122688_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-13104012-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bs2phiphi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2phiphi/MagUp/00122708_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2phiphi/MagUp/00122708_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122708/0000/00122708_00000001_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000004_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000012_1.xdigi',
        'root://lhcb-sdpd21.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000007_1.xdigi',
        'root://lhcb-sespd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000006_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122708/0000/00122708_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-13144011-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bs2JpsiPhi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (15/15 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2Jpsiphi/MagDown/00122700_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2Jpsiphi/MagDown/00122700_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122700/0000/00122700_00000001_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122700/0000/00122700_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122700/0000/00122700_00000009_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000004_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000010_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000011_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000005_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000012_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000013_1.xdigi',
        'root://lhcb-sdpd15.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000014_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000015_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-13144011-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Bs2JpsiPhi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (13/13 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2Jpsiphi/MagUp/00122720_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Bs2Jpsiphi/MagUp/00122720_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122720/0000/00122720_00000005_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122720/0000/00122720_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000004_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000011_1.xdigi',
        'root://lhcb-sdpd19.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000012_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122720/0000/00122720_00000013_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-21103100-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of D2KsPi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dp2Kspi/MagDown/00122690_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dp2Kspi/MagDown/00122690_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122690/0000/00122690_00000001_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122690/0000/00122690_00000004_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122690/0000/00122690_00000009_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000010_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000007_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000005_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000012_1.xdigi',
        'root://lhcb-sdpd13.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122690/0000/00122690_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-21103100-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of D2KsPi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (13/13 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dp2Kspi/MagUp/00122710_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dp2Kspi/MagUp/00122710_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122710/0000/00122710_00000001_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122710/0000/00122710_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000006_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000010_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000011_1.xdigi',
        'root://lhcb-sdpd24.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000012_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122710/0000/00122710_00000013_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-27163003-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Dst2D0pi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (16/16 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dst2D0pi/MagDown/00122694_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dst2D0pi/MagDown/00122694_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000010_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122694/0000/00122694_00000011_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000003_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000012_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000005_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000013_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000014_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000007_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000015_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000006_1.xdigi',
        'root://lhcb-sdpd22.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000016_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000001_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122694/0000/00122694_00000009_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-27163003-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Dst2D0pi, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dst2D0pi/MagUp/00122714_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Dst2D0pi/MagUp/00122714_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122714/0000/00122714_00000003_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122714/0000/00122714_00000009_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000010_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000011_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000012_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000013_1.xdigi',
        'root://lhcb-sdpd3.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000002_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122714/0000/00122714_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-42112000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Z2mumu, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (16/16 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2mumu/MagDown/00122692_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2mumu/MagDown/00122692_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000010_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122692/0000/00122692_00000011_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000002_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000012_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000004_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000009_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000013_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000014_1.xdigi',
        'root://lhcb-sdpd14.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000001_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000015_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122692/0000/00122692_00000016_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-42112000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Z2mumu, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (16/16 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2mumu/MagUp/00122712_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2mumu/MagUp/00122712_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000009_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122712/0000/00122712_00000010_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000011_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000012_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000002_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000013_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000014_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000015_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000001_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122712/0000/00122712_00000016_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-42122000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Z2ee, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2ee/MagDown/00122686_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2ee/MagDown/00122686_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122686/0000/00122686_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000002_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000001_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000012_1.xdigi',
        'root://lhcb-sdpd21.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000005_1.xdigi',
        'root://lhcb-sdpd21.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122686/0000/00122686_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-42122000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of Z2ee, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2ee/MagUp/00122706_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/Z2ee/MagUp/00122706_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122706/0000/00122706_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000003_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000002_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000006_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000012_1.xdigi',
        'root://lhcb-sdpd7.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000005_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122706/0000/00122706_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-30000000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of MinBias, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/ (14/14 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/MinBias/MagDown/00122696_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/MinBias/MagDown/00122696_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122696/0000/00122696_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000009_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000010_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000005_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000011_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000012_1.xdigi',
        'root://lhcb-sdpd22.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000013_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122696/0000/00122696_00000014_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-30000000-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of MinBias, December 2020. From /MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/ (13/13 files)'
     ),
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/MinBias/MagUp/00122716_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-MC-2020-Dec/MinBias/MagUp/00122716_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122716/0000/00122716_00000005_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122716/0000/00122716_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000009_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000003_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000010_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000011_1.xdigi',
        'root://lhcb-sdpd24.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000001_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000012_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122716/0000/00122716_00000013_1.xdigi',
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'Processing': 'Sim-Up08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-digi15-up04-30000000-velo_open-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of MinBias, May 2021. From /MC/Upgrade/Beam7000GeV-Upgrade-VPOpen-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/Digi15-Up04'
     ),
    filenames=[
        'root://x509up_u24378@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000001_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000002_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000003_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000004_1.xdigi',
        'https://lhcbwebdav-kit.gridka.de:2880/pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000005_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000006_1.xdigi',
        'root://x509up_u24378@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000007_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135245/0000/00135245_00000008_1.xdigi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2022-03-03',
        'Processing': 'Sim-Up08-Digi15-Up04',
        'DDDB': 'velo-open',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-digi15-up04-30000000-velo_open-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of MinBias, May 2021. From /MC/Upgrade/Beam7000GeV-Upgrade-VPOpen-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/Digi15-Up04'
     ),
    filenames=[
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000001_1.xdigi',
        'root://x509up_u24378@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000002_1.xdigi',
        'root://x509up_u24378@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000003_1.xdigi',
        'root://x509up_u24378@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000004_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000005_1.xdigi',
        'root://x509up_u24378@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000006_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000007_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000008_1.xdigi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2022-03-03',
        'Processing': 'Sim-Up08-Digi15-Up04',
        'DDDB': 'velo-open',
        'CondDB': 'sim-20201218-vc-mu100',
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='Moore-Output-DST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/DaVinci-Run3/hlt2_test_persistreco_fromfile.dst'
    ],
    qualifiers={
        'Author': 'Sevda Esen',
        'Format': 'DST',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'Date': '2021-03-26'
    },
    comment=
    'Uploaded by Patrick Koppenburg. Output of Moore test hlt2_test_persistreco_fromfile.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-digi15-33102102',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00129674/0000/00129674_00000{0:03d}_1.xdigi"
        .format(i) for i in [
            53, 209, 219, 107, 87, 202, 25, 203, 31, 17, 222, 29, 121, 126, 91,
            199, 228, 235, 150, 191, 237, 117, 61, 19, 127, 226, 224, 207, 190,
            156, 36, 90, 103, 86, 163, 51, 16, 83, 154, 72, 134, 143, 57, 128,
            80, 26, 122, 152, 73, 71, 13, 205, 63, 43, 233, 28, 68, 97, 38,
            217, 44, 176, 155, 22, 167, 148, 105, 10, 136, 158, 49, 231, 172,
            170, 42, 93, 32, 27, 50, 130, 184, 69, 174, 110, 112, 193, 46, 166,
            216, 227, 55, 129, 149, 102, 34, 204, 159, 229, 95, 81, 4, 195, 60,
            187, 180, 76, 56, 179, 113, 11, 45, 118, 109, 182, 236, 137, 48,
            210, 186, 160, 214, 100, 139, 64, 24, 23, 192, 74, 189, 157, 88,
            33, 215, 59, 200, 223, 197, 35, 141, 165, 234, 66, 115, 131, 232,
            54, 85, 77, 123, 37, 14, 221
        ]
    ],
    qualifiers={
        'Author': 'Marian Stahl',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20210218',
        'CondDB': 'sim-20201218-vc-md100',
        'Date': '2021-07-01'
    },
    comment=
    'Prompt Lambda -> p pi xdigi with GenLvl cuts for LL Lambdas. Request ID 79730,  Gauss v54r5, Boole v41r3. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up08/33102102/XDIGI. Only files at CERN (not the full 250k events).',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pHe',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_11-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_19-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_5-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_18-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_0-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_23-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_4-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_12-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_27-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_33-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_34-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_38-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_42-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_32-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_48-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_47-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_50-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_64-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_58-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_68-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_73-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_65-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_75-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_74-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_77-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_57-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_72-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_69-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_80-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_83-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_89-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_100-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_93-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_94-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_95-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_98-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_106-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_108-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_114-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_110-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_112-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_118-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_120-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_121-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_122-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_123-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_124-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_127-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_135-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_141-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_131-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_139-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_137-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_143-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_147-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_149-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_151-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_152-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_161-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_157-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_162-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_167-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_169-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_168-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_173-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_174-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_177-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_180-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_193-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_194-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_190-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_202-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_197-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_201-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_203-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_200-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_199-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_29-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_215-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_225-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_223-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_226-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_232-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_230-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_231-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_237-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_238-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_244-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_246-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_249-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_243-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_248-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_255-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_252-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_258-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_259-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_260-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_261-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_264-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_271-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_279-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_278-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_283-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_286-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_287-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_288-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_291-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_290-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_293-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_292-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_295-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_302-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_303-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_301-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_305-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_304-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_307-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_313-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_312-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_315-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_310-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_319-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_318-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_321-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_322-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_323-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_324-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_325-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_327-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_326-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_331-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_329-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_330-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_344-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_349-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_350-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_352-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_353-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_356-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_355-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_359-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_251-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_363-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_367-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_370-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_374-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_379-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_384-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_382-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_383-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_387-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_390-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_391-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_393-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_394-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_395-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_400-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_408-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_402-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_407-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_396-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_418-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_413-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_417-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_420-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_357-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_427-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_428-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_426-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_422-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_432-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_431-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_430-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_433-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_435-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_444-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_448-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_451-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_455-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_456-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_459-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_460-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_462-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_463-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_464-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_466-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_467-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_470-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_476-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_474-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_480-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_482-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_481-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_487-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_486-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_495-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_496-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_500-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_503-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_505-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_511-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_515-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_516-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_522-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_523-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_526-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_530-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_534-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_536-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_392-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_537-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_539-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_540-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_543-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_545-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_547-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_554-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_560-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_558-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_561-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_562-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_571-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_570-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_569-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_532-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_580-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_581-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_583-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_584-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_585-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_594-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_592-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_596-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_597-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_600-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_602-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_606-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_608-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_612-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_619-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_623-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_624-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_625-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_628-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_635-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_627-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_633-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_636-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_639-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_632-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_490-Extended.digi",
    ],
    qualifiers={
        'Author': 'Giacomo Graziani, Saverio Mariani',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment=
    'Private simulation with one fixed per-event pHe collision within the SMOG2 cell (z in [-500, -300] mm). SciFi v6 is required. Required for PR jobs.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pp_pHe',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_0-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_9-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_38-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_37-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_15-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_45-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_44-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_56-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_26-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_24-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_14-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_12-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_53-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_8-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_28-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_29-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_42-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_43-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_17-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_2-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_46-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_13-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_50-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_16-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_27-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_49-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_57-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_7-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_51-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_11-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_40-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_62-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_25-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_54-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_39-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_3-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_52-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_47-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_34-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_4-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_18-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_21-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_31-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_36-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_55-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_35-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_5-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_64-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_68-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_69-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_70-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_78-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_84-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_76-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_86-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_105-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_73-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_96-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_89-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_91-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_94-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_81-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_93-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_100-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_71-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_75-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_85-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_77-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_95-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_90-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_103-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_87-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_80-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_120-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_92-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_88-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_119-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_83-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_114-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_108-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_104-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_122-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_128-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_109-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_129-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_98-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_124-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_112-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_132-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_131-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_106-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_99-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_110-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_130-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_115-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_123-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_118-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_127-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_133-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_134-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_137-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_147-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_135-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_139-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_141-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_148-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_138-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_160-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_144-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_149-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_146-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_157-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_185-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_183-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_165-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_167-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_176-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_169-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_150-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_175-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_159-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_164-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_151-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_170-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_181-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_174-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_171-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_143-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_192-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_166-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_195-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_186-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_182-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_173-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_140-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_162-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_163-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_177-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_172-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_179-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_197-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_178-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_198-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_189-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_193-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_191-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_190-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_199-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_200-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_204-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_202-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_201-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_213-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_203-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_212-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_219-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_230-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_225-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_205-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_222-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_211-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_228-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_220-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_218-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_241-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_207-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_235-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_217-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_237-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_244-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_224-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_238-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_239-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_231-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_232-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_233-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_259-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_250-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_246-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_251-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_216-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_256-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_258-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_226-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_261-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_234-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_229-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_240-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_260-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_252-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_245-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_264-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_268-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_255-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_248-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_257-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_266-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_249-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_269-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_272-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_270-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_271-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_273-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_280-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_274-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_279-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_287-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_286-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_275-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_298-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_282-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_305-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_307-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_291-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_296-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_276-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_288-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_277-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_312-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_290-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_295-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_294-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_299-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_302-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_321-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_315-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_289-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_281-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_308-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_306-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_319-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_293-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_320-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_309-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_323-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_311-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_328-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_324-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_331-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_322-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_325-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_318-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_338-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_335-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_330-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_340-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_339-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_337-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_333-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_344-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_343-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_345-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_347-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_346-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_348-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_350-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_353-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_351-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_342-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_355-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_358-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_362-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_354-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_366-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_352-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_359-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_367-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_373-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_360-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_357-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_364-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_368-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_370-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_374-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_356-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_365-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_369-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_372-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_380-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_387-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_375-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_388-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_371-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_363-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_384-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_385-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_379-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_392-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_399-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_396-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_393-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_397-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_402-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_395-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_390-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_391-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_394-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_401-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_400-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_404-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_403-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_408-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_413-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_411-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_414-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_429-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_407-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_418-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_436-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_441-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_427-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_410-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_423-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_424-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_438-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_431-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_415-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_426-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_416-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_432-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_428-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_433-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_440-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_435-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_419-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_420-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_430-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_445-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_443-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_421-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_452-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_439-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_449-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_447-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_453-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_434-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_451-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_450-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_448-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_442-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_457-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_446-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_461-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_459-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_455-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_454-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_460-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_458-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_456-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_463-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_462-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_464-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_465-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_466-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_467-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_475-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_474-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_470-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_472-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_483-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_478-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_469-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_487-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_473-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_488-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_496-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_479-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_477-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_484-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_482-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_471-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_491-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_481-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_492-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_495-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_493-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_490-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_476-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_489-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_494-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_504-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_500-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_499-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_505-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_498-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_510-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_502-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_501-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_512-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_514-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_509-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_511-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_513-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_515-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_516-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_507-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_517-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_518-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_519-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_521-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_522-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_524-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_526-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_531-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_525-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_527-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_530-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_537-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_544-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_553-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_540-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_532-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_538-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_536-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_551-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_539-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_541-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_545-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_546-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_535-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_534-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_533-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_542-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_556-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_552-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_564-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_561-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_554-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_559-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_548-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_581-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_568-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_558-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_567-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_565-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_576-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_575-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_562-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_573-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_589-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_574-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_592-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_579-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_583-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_588-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_582-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_584-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_594-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_591-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_595-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_597-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_603-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_598-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_605-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_604-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_608-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_599-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_619-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_612-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_634-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_622-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_609-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_632-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_618-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_614-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_627-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_616-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_625-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_628-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_641-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_630-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_655-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_635-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_621-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_617-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_623-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_650-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_654-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_629-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_640-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_633-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_648-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_649-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_638-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_636-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_646-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_659-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_658-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_645-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_665-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_660-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_661-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_662-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_656-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_667-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_653-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_666-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_669-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_668-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_670-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_671-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_673-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_678-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_677-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_676-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_683-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_684-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_681-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_680-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_682-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_679-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_685-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_686-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_693-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_698-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_691-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_696-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_702-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_701-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_692-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_695-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_690-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_708-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_706-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_697-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_687-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_714-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_710-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_707-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_719-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_720-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_721-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_717-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_727-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_715-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_724-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_713-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_705-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_716-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_728-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_718-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_725-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_722-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_723-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_734-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_732-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_736-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_730-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_737-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_740-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_738-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_739-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_741-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_743-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_742-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_750-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_749-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_744-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_746-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_745-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_758-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_760-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_748-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_751-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_757-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_762-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_756-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_761-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_752-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_754-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_763-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_765-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_755-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_747-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_768-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_772-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_777-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_767-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_759-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_764-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_769-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_773-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_766-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_771-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_770-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_780-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_774-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_775-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_779-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_776-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_784-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_781-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_778-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_787-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_793-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_790-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_794-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_799-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_797-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_801-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_800-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_805-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_806-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_809-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_808-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_817-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_815-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_823-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_811-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_824-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_812-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_834-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_816-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_810-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_821-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_807-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_814-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_825-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_818-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_822-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_830-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_813-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_828-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_836-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_831-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_839-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_842-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_844-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_838-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_835-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_829-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_832-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_852-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_827-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_850-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_845-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_841-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_853-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_848-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_843-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_856-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_847-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_851-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_858-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_859-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_857-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_867-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_865-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_866-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_868-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_869-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_871-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_870-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_872-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_873-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_876-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_874-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_875-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_882-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_877-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_881-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_883-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_878-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_896-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_884-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_900-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_899-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_897-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_885-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_880-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_893-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_902-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_916-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_901-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_898-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_915-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_904-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_895-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_903-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_905-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_909-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_914-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_919-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_921-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_918-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_910-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_908-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_913-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_917-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_928-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_920-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_922-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_912-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_924-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_930-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_929-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_932-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_935-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_933-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_937-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_939-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_938-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_936-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_944-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_941-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_950-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_942-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_948-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_946-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_945-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_949-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_957-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_953-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_956-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_947-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_960-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_954-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_955-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_951-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_966-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_958-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_977-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_959-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_976-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_968-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_974-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_978-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_965-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_970-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_990-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_989-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_979-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_972-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_986-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_985-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_994-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_987-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_993-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_996-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_988-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_992-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_995-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_982-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_999-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_60-Extended.digi",
    ],
    qualifiers={
        'Author': 'Giacomo Graziani, Saverio Mariani',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment=
    'Private simulation with overlapped pp collisions in Run3 and one fixed per-event pHe collision within the SMOG2 cell (z in [-500, -300] mm). The gas presence is overestimated, being the expected mu in SMOG2, still not fixed and depending from the gas species and pressure, ~0.2. SciFi v6 is required. Required for PR jobs.',
    test_file_db=test_file_db)

testfiles(
    myname='MiniBrunel_2018_MinBias_FTv4_DIGI_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/MiniBrunel_2018_MinBias_FTv4_DIGI_retinacluster_digi/minbias_2018_retinacluster_1.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2018.08.15',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the MiniBrunel_2018_MinBias_FTv4_DIGI sample.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pHe_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/SMOG2_pHe_retinacluster_digi/SMOG2_pHe_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment='Private simulation adding RetinaClusters to the SMOG2_pHe sample.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOGHepp8MB_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/SMOGHepp8MB_retinacluster_digi/smog2_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the SMOG2_pp_pHe sample.',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster_digi/Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020.01.23',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the Upgrade_BsPhiPhi_MD_FTv4_DIGI sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_DC19_01_Bs2PhiPhiMD_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_2.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_3.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_4.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_5.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_6.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_7.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_Bs2PhiPhiMD_retinacluster_xdigi/bsphiphi_2019_retinacluster_8.xdigi",
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2019.07.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade_DC19_01_Bs2PhiPhiMD sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_DC19_01_MinBiasMD_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMD_retinacluster_xdigi/upgrade_DC19_01_MinBiasMD_retinacluster_xdigi.xdigi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade_DC19_01_MinBiasMD sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_DC19_01_MinBiasMU_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_2.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_3.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_4.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_5.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_6.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_7.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade_DC19_01_MinBiasMU_retinacluster_xdigi/minbias_2019_retinacluster_8.xdigi",
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade_DC19_01_MinBiasMU sample.',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_KstEE_MU_FTv4_DIGI_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/Upgrade_KstEE_MU_FTv4_DIGI_retinacluster_digi/kstee_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020.02.15',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the Upgrade_KstEE_MU_FTv4_DIGI sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up08-30000000_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade-magdown-sim10-up08-30000000_retinacluster-xdigi/upgrade-magdown-sim10-up08-30000000-digi_retinacluster.xdigi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-01-12',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-magdown-sim10-up08-30000000 sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minbias-magdown-scifi-v5_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade-minbias-magdown-scifi-v5_retinacluster_digi/upgrade-minbias-magdown-scifi-v5_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2018.08.15',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-minbias-magdown-scifi-v5 sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT64-digi_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade-baseline-FT64-digi_retinacluster/minbias-64-1068-MagDown-Extended_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2018-10-26',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-baseline-FT64-digi sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-TELL40-UT-miniBias_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples/upgrade-TELL40-UT-miniBias_retinacluster/Boole-Extended_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020-12-04 08:01:44.324813',
        'DDDB': 'upgrade/dddb-20201105',
        'CondDB': 'upgrade/UTv4r2',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-TELL40-UT-miniBias sample.',
    test_file_db=test_file_db)

testfiles(
    myname='plume-raw-data-v1',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/plume-raw-data-v1/Run_0000226558_20220311-124351-571_PLEB01.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-03-15',
        'Simulation': False,
    },
    comment='PLUME v1 raw data from DAQ.',
    test_file_db=test_file_db)
